# libsirbyh

A libhybris-like library for loading ARM (aarch32 only for now) Android shared libraries from **unmodified system partitions** into a GNU/Linux process.

## Building and installing

```
cd lib
make
make install
```

If you are developing libsirbyh itself, you can also use `make develop`. In that case the installed library becomes a symlink into the tree and you don't need to reinstall it after each rebuild.

## Using libsirbyh

Public interface:

```
void* android_dlopen(const char* library_name, int flags);
void* android_dlsym(void* handle, const char* symbol_name);
```

These are the only two symbols exported by libsirbyh. When installing, a symlink with the name "libhybris-common.so.1" is also created, for libhybris compatibility.

## Comparison with libhybris

Unlike libhybris which does a high-level emulation of bionic, libsirbyh loads the actual bionic library from the filesystem, and patches it at runtime to be compatible. This eliminates the need for a compile-time bionic patch and allows using stock /system images by phone vendors, but comes with drawbacks:

* malloc(3) and free(3) are not compatible. A pointer allocated with one libc cannot be freed with another.
* Pointers to library objects, such as `FILE*`, are not interchangeable between libcs.
* errno is separate between libcs. You can resolve `int __error(void)` from `"libc.so"` to get the Android-side errno pointer. Do not rely on this behavior, this may be fixed in the future.

I don't know if any of these problems affect real programs using libhybris (apart from mcpelauncher-linux), but technically they could.

## License

Copyright 2023 Sergey Lisov

Unless otherwise noted, all source files in this repository are released under the terms of the GNU LGPL v3. A copy of this license is made available in the repo.
