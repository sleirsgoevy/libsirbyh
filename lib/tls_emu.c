#include "tls_emu.h"

static __thread __attribute__((aligned(16))) char stack_for_exit[2048];

__thread uintptr_t the_emutls[4] = {};

__attribute__((naked))
void emutls_tlsgd(void)
{
    asm volatile(".word the_emutls(tlsgd)");
}

uintptr_t* get_emutls_access_details(void)
{
    uintptr_t tlsgd = *(uintptr_t*)((uintptr_t)emutls_tlsgd & -2);
    tlsgd += (uintptr_t)emutls_tlsgd & -2;
    return (uintptr_t*)tlsgd;
}

uintptr_t get_emutls(void)
{
    return the_emutls[0];
}

void set_emutls(uintptr_t value)
{
    the_emutls[0] = value;
}

uintptr_t get_emu_tid_address(void)
{
    return the_emutls[1];
}

void set_emu_tid_address(uintptr_t value)
{
    the_emutls[1] = value;
}

void set_thread_exit_handler(void(*fn)(int))
{
    the_emutls[2] = (uintptr_t)fn;
    the_emutls[3] = (uintptr_t)(1 + &stack_for_exit);
}
