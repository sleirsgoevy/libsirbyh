#pragma once
#include <stddef.h>

void* alloc_jit(size_t sz);
void lock_jit(void);
