#pragma once
#include <stddef.h>
#include <stdint.h>

int load_elf(const void* elf, size_t elf_size, void** base, void** entry, void** linker_base, void** linker_entry, void** phdr, size_t* phdr_size, void(*patch_code)(uintptr_t start, uintptr_t end));
void run_elf(int argc, const char** argv, const char** envp, void* entry, void* linker_base, void* linker_entry, void* phdr, size_t phdr_size, const stack_t* stack);
