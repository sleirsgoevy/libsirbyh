#include <stdint.h>
#include <inttypes.h>
#include "../archdetect.h"

void __libc_init(void* raw_args, void* onexit, int(*main)(int, const char**), void*[10]);
int printf(const char* fmt, ...);
int sscanf(const char* s, const char* fmt, ...);
void* dlopen(const char* path, int flags);
void* dlsym(void* handle, const char* name);
int clone(int(*fn)(void*), void* stack, int flags, void* arg, ...);
int strcmp(const char*, const char*);

int main(int argc, const char** argv);

static void* structors[10] = {};

__attribute__((naked)) void _start(void)
{
    uintptr_t sp;
#if CURRENT_MACHINE == EM_ARM
    asm volatile("and sp, sp, #-16\nmov %0, sp":"=r"(sp));
#endif
    __libc_init((void*)sp, 0, main, structors);
}

int main(int argc, const char** argv)
{
    uintptr_t fn = 0;
    if(argc == 3 && !strcmp(argv[1], "--yes-i-am-really-libsirbyh"))
        sscanf(argv[2], "%" PRIuPTR, &fn);
    if(!fn)
    {
        printf("This binary is an Android-side stub for libsirbyh. It is not meant to be called directly.\n");
        return 0;
    }
    ((void(*)(void*, void*, void*))fn)(dlopen, dlsym, clone);
}
