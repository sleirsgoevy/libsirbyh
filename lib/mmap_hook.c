#include <stddef.h>
#include <stdint.h>
#include <sys/syscall.h>
#include <sys/mman.h>
#include "patches.h"
#include "mmap_hook.h"

uintptr_t raw_syscall(int nr, ...);

#ifndef __NR_mmap2
#define __NR_mmap2 __NR_mmap
#endif

uintptr_t modified_mmap(uintptr_t addr, size_t size, int prot, int flags, int fd, uintptr_t offset)
{
    if((prot & PROT_EXEC) && !(flags & MAP_ANON))
    {
        uintptr_t mapping = raw_syscall(__NR_mmap2, addr, size, prot | PROT_WRITE, flags, fd, offset);
        if(mapping > (uintptr_t)-4096)
            return mapping;
        patch_dyld(mapping, mapping + size);
        uintptr_t error = raw_syscall(__NR_mprotect, mapping, size, prot);
        if(error)
            return error;
        return mapping;
    }
    return raw_syscall(__NR_mmap2, addr, size, prot, flags, fd, offset);
}
