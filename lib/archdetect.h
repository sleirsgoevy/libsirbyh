#pragma once
#include <elf.h>

#define ERROR(x) (errno = (x), -1)

#if defined(__arm__) && !defined(__aarch64__)
static const char e_ident[] = {0x7f, 'E', 'L', 'F', 1, 1, 1, 0, 0};
typedef Elf32_Ehdr Elf_Ehdr;
typedef Elf32_Phdr Elf_Phdr;
#define CURRENT_MACHINE EM_ARM
#else
#error "unsupported architecture"
static const char e_ident[1] = {0xff};
typedef Elf32_Ehdr Elf_Ehdr;
typedef Elf32_Phdr Elf_Phdr;
#define CURRENT_MACHINE 0xff
#endif


