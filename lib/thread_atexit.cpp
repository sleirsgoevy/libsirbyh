extern "C"
{
#include "thread_atexit.h"
}

static struct AtexitCaller
{
    void(*fn)(void*);
    void* opaque;
    ~AtexitCaller(void)
    {
        fn(opaque);
    }
} thread_local atexit_caller;

extern "C" void set_thread_atexit(void(*fn)(void*), void* opaque)
{
    atexit_caller.fn = fn;
    atexit_caller.opaque = opaque;
}
