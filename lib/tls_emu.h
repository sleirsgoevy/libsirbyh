#pragma once
#include <stdint.h>

uintptr_t* get_emutls_access_details(void);
uintptr_t get_emutls(void);
void set_emutls(uintptr_t);
uintptr_t get_emu_tid_address(void);
void set_emu_tid_address(uintptr_t);
void set_thread_exit_handler(void(*fn)(int));
