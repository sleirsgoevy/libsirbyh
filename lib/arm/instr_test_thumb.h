static int test_thumb_instr_271(uint32_t instr) {
 if((instr & 0xd0u) == 0x10u) {
  return 2;
 } else {
  return 0;
 }
}

static int test_thumb_instr_1930(uint32_t instr) {
 if((instr & (1u << 9))) {
  return test_thumb_instr_271(instr);
 } else if((instr & 0x1d0u) == 0x0u) {
  return 2;
 } else {
  return 0;
 }
}

static int test_thumb_instr_306(uint32_t instr) {
 if((instr & (1u << 4))) {
  return 0;
 } else {
  return 2;
 }
}

static int test_thumb_instr_670(uint32_t instr) {
 if((instr & 0x11u) == 0x11u) {
  return 0;
 } else {
  return 2;
 }
}

static int test_thumb_instr_704(uint32_t instr) {
 if((instr & (1u << 7))) {
  return test_thumb_instr_306(instr);
 } else {
  return test_thumb_instr_670(instr);
 }
}

static int test_thumb_instr_2010(uint32_t instr) {
 if((instr & (1u << 8))) {
  if((instr & (1u << 7))) {
   return 0;
  } else {
   return test_thumb_instr_670(instr);
  }
 } else {
  return test_thumb_instr_704(instr);
 }
}

static int test_thumb_instr_270(uint32_t instr) {
 if((instr & 0x50u) == 0x10u) {
  return 2;
 } else {
  return 0;
 }
}

static int test_thumb_instr_572(uint32_t instr) {
 if((instr & 0x11u) == 0x10u) {
  return 2;
 } else {
  return 0;
 }
}

static int test_thumb_instr_266(uint32_t instr) {
 if((instr & (1u << 4))) {
  return 2;
 } else {
  return 0;
 }
}

static int test_thumb_instr_573(uint32_t instr) {
 if((instr & (1u << 6))) {
  return test_thumb_instr_572(instr);
 } else {
  return test_thumb_instr_266(instr);
 }
}

static int test_thumb_instr_745(uint32_t instr) {
 if((instr & (1u << 7))) {
  return 0;
 } else {
  return test_thumb_instr_573(instr);
 }
}

static int test_thumb_instr_1922(uint32_t instr) {
 if((instr & (1u << 9))) {
  return test_thumb_instr_745(instr);
 } else if((instr & 0x190u) == 0x0u) {
  return 2;
 } else {
  return 0;
 }
}

static int test_thumb_instr_708(uint32_t instr) {
 if((instr & 0x1c0u) == 0x0u) {
  return 2;
 } else {
  return test_thumb_instr_306(instr);
 }
}

static int test_thumb_instr_1913(uint32_t instr) {
 if((instr & 0x280u) == 0x0u) {
  return test_thumb_instr_670(instr);
 } else {
  return test_thumb_instr_306(instr);
 }
}

static int test_thumb_instr_1931(uint32_t instr) {
 if((instr & (1u << 10))) {
  return test_thumb_instr_1930(instr);
 } else {
  return test_thumb_instr_1913(instr);
 }
}

static int test_thumb_instr_744(uint32_t instr) {
 if((instr & (1u << 9))) {
  return test_thumb_instr_708(instr);
 } else {
  return test_thumb_instr_704(instr);
 }
}

static int test_thumb_instr_1923(uint32_t instr) {
 if((instr & (1u << 10))) {
  return test_thumb_instr_1922(instr);
 } else {
  return test_thumb_instr_744(instr);
 }
}

static int test_thumb_instr_1949(uint32_t instr) {
 if((instr & (1u << 12))) {
  if((instr & (1u << 11))) {
   return test_thumb_instr_1931(instr);
  } else {
   return test_thumb_instr_270(instr);
  }
 } else if((instr & (1u << 11))) {
  return test_thumb_instr_1923(instr);
 } else {
  return test_thumb_instr_573(instr);
 }
}

static int test_thumb_instr_569(uint32_t instr) {
 if((instr & (1u << 6))) {
  return 0;
 } else {
  return 2;
 }
}

static int test_thumb_instr_568(uint32_t instr) {
 if((instr & 0x41u) == 0x41u) {
  return 0;
 } else {
  return 2;
 }
}

static int test_thumb_instr_575(uint32_t instr) {
 if((instr & 0x11u) == 0x0u) {
  return 2;
 } else {
  return 0;
 }
}

static int test_thumb_instr_632(uint32_t instr) {
 if((instr & 0x11u) == 0x1u) {
  return 0;
 } else {
  return 2;
 }
}

static int test_thumb_instr_1942(uint32_t instr) {
 if((instr & (1u << 8))) {
  return test_thumb_instr_270(instr);
 } else if((instr & (1u << 6))) {
  return test_thumb_instr_575(instr);
 } else {
  return test_thumb_instr_632(instr);
 }
}

static int test_thumb_instr_630(uint32_t instr) {
 if((instr & (1u << 6))) {
  return test_thumb_instr_572(instr);
 } else {
  return 2;
 }
}

static int test_thumb_instr_1936(uint32_t instr) {
 if((instr & (1u << 7))) {
  return test_thumb_instr_573(instr);
 } else {
  return test_thumb_instr_630(instr);
 }
}

static int test_thumb_instr_81(uint32_t instr) {
 if((instr & (1u << 0))) {
  return 0;
 } else {
  return 2;
 }
}

static int test_thumb_instr_1937(uint32_t instr) {
 if((instr & (1u << 8))) {
  return test_thumb_instr_1936(instr);
 } else if((instr & 0x50u) == 0x10u) {
  return 2;
 } else {
  return test_thumb_instr_81(instr);
 }
}

static int test_thumb_instr_774(uint32_t instr) {
 if((instr & (1u << 6))) {
  return test_thumb_instr_81(instr);
 } else {
  return test_thumb_instr_266(instr);
 }
}

static int test_thumb_instr_1926(uint32_t instr) {
 if((instr & 0x380u) == 0x280u) {
  return test_thumb_instr_270(instr);
 } else {
  return test_thumb_instr_569(instr);
 }
}

static int test_thumb_instr_1918(uint32_t instr) {
 if((instr & 0x380u) == 0x280u) {
  return test_thumb_instr_573(instr);
 } else {
  return test_thumb_instr_568(instr);
 }
}

static int test_thumb_instr_780(uint32_t instr) {
 if((instr & 0x2d0u) == 0x210u) {
  return 2;
 } else {
  return 0;
 }
}

static int test_thumb_instr_1914(uint32_t instr) {
 if((instr & (1u << 10))) {
  return test_thumb_instr_780(instr);
 } else {
  return test_thumb_instr_1913(instr);
 }
}

static int test_thumb_instr_570(uint32_t instr) {
 if((instr & (1u << 8))) {
  return test_thumb_instr_270(instr);
 } else {
  return test_thumb_instr_569(instr);
 }
}

static int test_thumb_instr_788(uint32_t instr) {
 if((instr & 0x280u) == 0x200u) {
  return test_thumb_instr_573(instr);
 } else {
  return 0;
 }
}

static int test_thumb_instr_1907(uint32_t instr) {
 if((instr & (1u << 10))) {
  return test_thumb_instr_788(instr);
 } else {
  return test_thumb_instr_744(instr);
 }
}

static int test_thumb_instr_1904(uint32_t instr) {
 if((instr & (1u << 8))) {
  return test_thumb_instr_573(instr);
 } else {
  return test_thumb_instr_568(instr);
 }
}

static int test_thumb_instr_647(uint32_t instr) {
 if((instr & (1u << 6))) {
  return 0;
 } else {
  return test_thumb_instr_632(instr);
 }
}

static int test_thumb_instr_1833(uint32_t instr) {
 if((instr & (1u << 6))) {
  return test_thumb_instr_572(instr);
 } else {
  return test_thumb_instr_632(instr);
 }
}

static int test_thumb_instr_1933(uint32_t instr) {
 if((instr & (1u << 12))) {
  if((instr & (1u << 11))) {
   return test_thumb_instr_1931(instr);
  } else if((instr & (1u << 10))) {
   return test_thumb_instr_270(instr);
  } else {
   return test_thumb_instr_1926(instr);
  }
 } else if((instr & (1u << 11))) {
  return test_thumb_instr_1923(instr);
 } else if((instr & (1u << 10))) {
  return test_thumb_instr_573(instr);
 } else {
  return test_thumb_instr_1918(instr);
 }
}

static int test_thumb_instr_781(uint32_t instr) {
 if((instr & (1u << 10))) {
  return test_thumb_instr_780(instr);
 } else if((instr & 0x291u) == 0x10u) {
  return 2;
 } else {
  return 0;
 }
}

static int test_thumb_instr_621(uint32_t instr) {
 if((instr & 0x150u) == 0x0u) {
  return 2;
 } else {
  return 0;
 }
}

static int test_thumb_instr_1868(uint32_t instr) {
 if((instr & (1u << 8))) {
  return 0;
 } else if((instr & 0xc0u) == 0x0u) {
  return 2;
 } else {
  return test_thumb_instr_306(instr);
 }
}

static int test_thumb_instr_673(uint32_t instr) {
 if((instr & 0x91u) == 0x10u) {
  return 2;
 } else {
  return 0;
 }
}

static int test_thumb_instr_355(uint32_t instr) {
 if((instr & 0x50u) == 0x0u) {
  return 2;
 } else {
  return 0;
 }
}

static int test_thumb_instr_672(uint32_t instr) {
 if((instr & (1u << 7))) {
  return test_thumb_instr_355(instr);
 } else if((instr & (1u << 6))) {
  return test_thumb_instr_572(instr);
 } else {
  return test_thumb_instr_670(instr);
 }
}

static int test_thumb_instr_674(uint32_t instr) {
 if((instr & (1u << 8))) {
  return test_thumb_instr_673(instr);
 } else {
  return test_thumb_instr_672(instr);
 }
}

static int test_thumb_instr_1879(uint32_t instr) {
 if((instr & (1u << 9))) {
  return test_thumb_instr_1868(instr);
 } else {
  return test_thumb_instr_674(instr);
 }
}

static int test_thumb_instr_728(uint32_t instr) {
 if((instr & (1u << 6))) {
  return test_thumb_instr_670(instr);
 } else {
  return test_thumb_instr_266(instr);
 }
}

static int test_thumb_instr_1857(uint32_t instr) {
 if((instr & (1u << 8))) {
  return test_thumb_instr_630(instr);
 } else {
  return test_thumb_instr_573(instr);
 }
}

static int test_thumb_instr_696(uint32_t instr) {
 if((instr & 0x51u) == 0x51u) {
  return 0;
 } else {
  return 2;
 }
}

static int test_thumb_instr_727(uint32_t instr) {
 if((instr & (1u << 8))) {
  return test_thumb_instr_573(instr);
 } else {
  return test_thumb_instr_696(instr);
 }
}

static int test_thumb_instr_686(uint32_t instr) {
 if((instr & (1u << 8))) {
  return test_thumb_instr_573(instr);
 } else {
  return test_thumb_instr_630(instr);
 }
}

static int test_thumb_instr_1878(uint32_t instr) {
 if((instr & (1u << 10))) {
  if((instr & (1u << 9))) {
   if((instr & (1u << 8))) {
    return test_thumb_instr_630(instr);
   } else {
    return test_thumb_instr_728(instr);
   }
  } else {
   return test_thumb_instr_1857(instr);
  }
 } else if((instr & (1u << 9))) {
  return test_thumb_instr_727(instr);
 } else {
  return test_thumb_instr_686(instr);
 }
}

static int test_thumb_instr_1842(uint32_t instr) {
 if((instr & 0x500u) == 0x400u) {
  return test_thumb_instr_647(instr);
 } else {
  return test_thumb_instr_270(instr);
 }
}

static int test_thumb_instr_1870(uint32_t instr) {
 if((instr & 0x140u) == 0x100u) {
  return 0;
 } else {
  return test_thumb_instr_306(instr);
 }
}

static int test_thumb_instr_393(uint32_t instr) {
 if((instr & 0x50u) == 0x40u) {
  return 2;
 } else {
  return 0;
 }
}

static int test_thumb_instr_720(uint32_t instr) {
 if((instr & (1u << 7))) {
  return test_thumb_instr_393(instr);
 } else if((instr & (1u << 6))) {
  return test_thumb_instr_670(instr);
 } else {
  return test_thumb_instr_572(instr);
 }
}

static int test_thumb_instr_1864(uint32_t instr) {
 if((instr & (1u << 6))) {
  return test_thumb_instr_670(instr);
 } else {
  return test_thumb_instr_632(instr);
 }
}

static int test_thumb_instr_1865(uint32_t instr) {
 if((instr & (1u << 8))) {
  return test_thumb_instr_630(instr);
 } else {
  return test_thumb_instr_1864(instr);
 }
}

static int test_thumb_instr_683(uint32_t instr) {
 if((instr & 0x691u) == 0x10u) {
  return 2;
 } else {
  return 0;
 }
}

static int test_thumb_instr_1860(uint32_t instr) {
 if((instr & (1u << 11))) {
  return test_thumb_instr_683(instr);
 } else {
  return test_thumb_instr_270(instr);
 }
}

static int test_thumb_instr_622(uint32_t instr) {
 if((instr & 0x350u) == 0x0u) {
  return 2;
 } else {
  return 0;
 }
}

static int test_thumb_instr_1855(uint32_t instr) {
 if((instr & (1u << 11))) {
  return test_thumb_instr_683(instr);
 } else {
  return test_thumb_instr_1842(instr);
 }
}

static int test_thumb_instr_677(uint32_t instr) {
 if((instr & (1u << 9))) {
  if((instr & (1u << 8))) {
   return 0;
  } else if((instr & (1u << 7))) {
   return test_thumb_instr_355(instr);
  } else {
   return test_thumb_instr_569(instr);
  }
 } else {
  return test_thumb_instr_674(instr);
 }
}

static int test_thumb_instr_1853(uint32_t instr) {
 if((instr & (1u << 10))) {
  return test_thumb_instr_622(instr);
 } else {
  return test_thumb_instr_677(instr);
 }
}

static int test_thumb_instr_655(uint32_t instr) {
 if((instr & 0x700u) == 0x700u) {
  if((instr & 0xf0u) == 0x10u) {
   return 2;
  } else {
   return 0;
  }
 } else {
  return test_thumb_instr_271(instr);
 }
}

static int test_thumb_instr_642(uint32_t instr) {
 if((instr & 0xb0u) == 0x10u) {
  return 2;
 } else {
  return 0;
 }
}

static int test_thumb_instr_639(uint32_t instr) {
 if((instr & 0x90u) == 0x10u) {
  return 2;
 } else {
  return 0;
 }
}

static int test_thumb_instr_629(uint32_t instr) {
 if((instr & 0x50u) == 0x40u) {
  return 0;
 } else {
  return 2;
 }
}

static int test_thumb_instr_638(uint32_t instr) {
 if((instr & (1u << 7))) {
  return test_thumb_instr_355(instr);
 } else {
  return test_thumb_instr_629(instr);
 }
}

static int test_thumb_instr_640(uint32_t instr) {
 if((instr & (1u << 8))) {
  return test_thumb_instr_639(instr);
 } else {
  return test_thumb_instr_638(instr);
 }
}

static int test_thumb_instr_1839(uint32_t instr) {
 if((instr & 0x600u) == 0x600u) {
  if((instr & (1u << 8))) {
   return test_thumb_instr_642(instr);
  } else {
   return test_thumb_instr_639(instr);
  }
 } else {
  return test_thumb_instr_640(instr);
 }
}

static int test_thumb_instr_631(uint32_t instr) {
 if((instr & (1u << 7))) {
  return test_thumb_instr_630(instr);
 } else {
  return test_thumb_instr_629(instr);
 }
}

static int test_thumb_instr_658(uint32_t instr) {
 if((instr & 0xc0u) == 0xc0u) {
  return test_thumb_instr_572(instr);
 } else {
  return test_thumb_instr_266(instr);
 }
}

static int test_thumb_instr_659(uint32_t instr) {
 if((instr & (1u << 8))) {
  return test_thumb_instr_658(instr);
 } else {
  return test_thumb_instr_631(instr);
 }
}

static int test_thumb_instr_633(uint32_t instr) {
 if((instr & (1u << 6))) {
  return test_thumb_instr_266(instr);
 } else {
  return test_thumb_instr_632(instr);
 }
}

static int test_thumb_instr_576(uint32_t instr) {
 if((instr & (1u << 6))) {
  return test_thumb_instr_575(instr);
 } else {
  return test_thumb_instr_306(instr);
 }
}

static int test_thumb_instr_626(uint32_t instr) {
 if((instr & 0x11000u) == 0x0u) {
  if((instr & (1u << 11))) {
   if((instr & 0x700u) == 0x0u) {
    return test_thumb_instr_576(instr);
   } else {
    return 0;
   }
  } else if((instr & (1u << 10))) {
   if((instr & (1u << 9))) {
    return 0;
   } else {
    return test_thumb_instr_568(instr);
   }
  } else if((instr & 0x300u) == 0x300u) {
   return 0;
  } else {
   return test_thumb_instr_573(instr);
  }
 } else if((instr & (1u << 11))) {
  if((instr & 0x750u) == 0x0u) {
   return 2;
  } else {
   return 0;
  }
 } else if((instr & (1u << 10))) {
  if((instr & 0x240u) == 0x0u) {
   return 2;
  } else {
   return 0;
  }
 } else if((instr & 0x300u) == 0x300u) {
  return 0;
 } else {
  return test_thumb_instr_270(instr);
 }
}

static int test_thumb_instr_1800(uint32_t instr) {
 if((instr & (1u << 8))) {
  return test_thumb_instr_355(instr);
 } else {
  return test_thumb_instr_568(instr);
 }
}

static int test_thumb_instr_1813(uint32_t instr) {
 if((instr & (1u << 9))) {
  if((instr & (1u << 8))) {
   return test_thumb_instr_576(instr);
  } else {
   return test_thumb_instr_569(instr);
  }
 } else if((instr & (1u << 8))) {
  return test_thumb_instr_576(instr);
 } else {
  return test_thumb_instr_568(instr);
 }
}

static int test_thumb_instr_1805(uint32_t instr) {
 if((instr & (1u << 8))) {
  return test_thumb_instr_355(instr);
 } else {
  return test_thumb_instr_569(instr);
 }
}

static int test_thumb_instr_687(uint32_t instr) {
 if((instr & (1u << 8))) {
  return test_thumb_instr_630(instr);
 } else {
  return 0;
 }
}

static int test_thumb_instr_603(uint32_t instr) {
 if((instr & 0x140u) == 0x100u) {
  return 2;
 } else {
  return 0;
 }
}

static int test_thumb_instr_260(uint32_t instr) {
 if((instr & 0xe00u) == 0xa00u) {
  return 0;
 } else {
  return 2;
 }
}

static int test_thumb_instr_321(uint32_t instr) {
 if((instr & 0xe10u) == 0xa10u) {
  return 0;
 } else {
  return 2;
 }
}

static int test_thumb_instr_389(uint32_t instr) {
 if((instr & 0xf000u) == 0xf000u) {
  return test_thumb_instr_306(instr);
 } else {
  return test_thumb_instr_321(instr);
 }
}

static int test_thumb_instr_363(uint32_t instr) {
 if((instr & 0xe40u) == 0xa40u) {
  return 0;
 } else {
  return test_thumb_instr_306(instr);
 }
}

static int test_thumb_instr_289(uint32_t instr) {
 if((instr & 0xf0000u) == 0xf0000u) {
  return 0;
 } else {
  return 2;
 }
}

static int test_thumb_instr_166(uint32_t instr) {
 if((instr & 0xfu) == 0xfu) {
  return 0;
 } else {
  return 2;
 }
}

static int test_thumb_instr_1206(uint32_t instr) {
 if((instr & 0x70u) == 0x0u) {
  return test_thumb_instr_166(instr);
 } else {
  return 0;
 }
}

static int test_thumb_instr_1233(uint32_t instr) {
 if((instr & 0xf00u) == 0xf00u) {
  return 0;
 } else if((instr & 0xf0u) == 0x0u) {
  return test_thumb_instr_166(instr);
 } else {
  return 0;
 }
}

static int test_thumb_instr_1650(uint32_t instr) {
 if((instr & 0xf00u) == 0xf00u) {
  return 0;
 } else if((instr & 0xe0u) == 0x0u) {
  return test_thumb_instr_166(instr);
 } else {
  return 0;
 }
}

static int test_thumb_instr_1670(uint32_t instr) {
 if((instr & 0xf0000u) == 0xf0000u) {
  return 0;
 } else {
  return test_thumb_instr_1650(instr);
 }
}

static int test_thumb_instr_1244(uint32_t instr) {
 if((instr & 0x30u) == 0x30u) {
  return 0;
 } else {
  return test_thumb_instr_166(instr);
 }
}

static int test_thumb_instr_164(uint32_t instr) {
 if((instr & 0x3u) == 0x3u) {
  return 0;
 } else {
  return 2;
 }
}

static int test_thumb_instr_1272(uint32_t instr) {
 if((instr & 0x5u) == 0x5u) {
  return 0;
 } else {
  return 2;
 }
}

static int test_thumb_instr_1259(uint32_t instr) {
 if((instr & 0x6u) == 0x6u) {
  return 0;
 } else {
  return 2;
 }
}

static int test_thumb_instr_165(uint32_t instr) {
 if((instr & 0x7u) == 0x7u) {
  return 0;
 } else {
  return 2;
 }
}

static int test_thumb_instr_1321(uint32_t instr) {
 if((instr & 0x9u) == 0x9u) {
  return 0;
 } else {
  return 2;
 }
}

static int test_thumb_instr_1309(uint32_t instr) {
 if((instr & 0xau) == 0xau) {
  return 0;
 } else {
  return 2;
 }
}

static int test_thumb_instr_1298(uint32_t instr) {
 if((instr & 0xbu) == 0xbu) {
  return 0;
 } else {
  return 2;
 }
}

static int test_thumb_instr_1285(uint32_t instr) {
 if((instr & 0xcu) == 0xcu) {
  return 0;
 } else {
  return 2;
 }
}

static int test_thumb_instr_1273(uint32_t instr) {
 if((instr & 0xdu) == 0xdu) {
  return 0;
 } else {
  return 2;
 }
}

static int test_thumb_instr_1260(uint32_t instr) {
 if((instr & 0xeu) == 0xeu) {
  return 0;
 } else {
  return 2;
 }
}

static int test_thumb_instr_1253(uint32_t instr) {
 if((instr & 0xf000u) == 0xf000u) {
  if((instr & 0xf00u) == 0xf00u) {
   return 0;
  } else if((instr & (1u << 7))) {
   if((instr & (1u << 6))) {
    return 0;
   } else {
    return test_thumb_instr_166(instr);
   }
  } else {
   return test_thumb_instr_1244(instr);
  }
 } else {
  return 0;
 }
}

static int test_thumb_instr_1117(uint32_t instr) {
 if((instr & (1u << 8))) {
  if((instr & 0xd0u) == 0xc0u) {
   return 0;
  } else {
   return 2;
  }
 } else if((instr & 0xc0u) == 0xc0u) {
  return 0;
 } else {
  return test_thumb_instr_306(instr);
 }
}

static int test_thumb_instr_67(uint32_t instr) {
 if((instr & 0x60u) == 0x0u) {
  return 2;
 } else {
  return 0;
 }
}

static int test_thumb_instr_66(uint32_t instr) {
 if((instr & (1u << 5))) {
  return 0;
 } else {
  return 2;
 }
}

static int test_thumb_instr_391(uint32_t instr) {
 if((instr & 0x30u) == 0x0u) {
  return 2;
 } else {
  return 0;
 }
}

static int test_thumb_instr_1100(uint32_t instr) {
 if((instr & (1u << 5))) {
  return test_thumb_instr_266(instr);
 } else {
  return test_thumb_instr_306(instr);
 }
}

static int test_thumb_instr_1161(uint32_t instr) {
 if((instr & (1u << 10))) {
  if((instr & 0x320u) == 0x0u) {
   return 2;
  } else {
   return 0;
  }
 } else if((instr & 0x310u) == 0x0u) {
  return 2;
 } else {
  return 0;
 }
}

static int test_thumb_instr_1113(uint32_t instr) {
 if((instr & (1u << 7))) {
  return test_thumb_instr_569(instr);
 } else if((instr & 0x50u) == 0x10u) {
  return 0;
 } else {
  return 2;
 }
}

static int test_thumb_instr_1101(uint32_t instr) {
 if((instr & (1u << 6))) {
  return 0;
 } else {
  return test_thumb_instr_1100(instr);
 }
}

static int test_thumb_instr_1155(uint32_t instr) {
 if((instr & (1u << 8))) {
  return test_thumb_instr_67(instr);
 } else {
  return test_thumb_instr_1101(instr);
 }
}

static int test_thumb_instr_1096(uint32_t instr) {
 if((instr & 0x110u) == 0x10u) {
  return 0;
 } else {
  return 2;
 }
}

static int test_thumb_instr_1154(uint32_t instr) {
 if((instr & (1u << 10))) {
  if((instr & 0x220u) == 0x0u) {
   return 2;
  } else {
   return 0;
  }
 } else if((instr & (1u << 9))) {
  return 0;
 } else {
  return test_thumb_instr_1096(instr);
 }
}

static int test_thumb_instr_1062(uint32_t instr) {
 if((instr & 0xc0u) == 0xc0u) {
  return 0;
 } else {
  return 2;
 }
}

static int test_thumb_instr_1118(uint32_t instr) {
 if((instr & (1u << 9))) {
  return test_thumb_instr_1117(instr);
 } else if((instr & (1u << 8))) {
  return test_thumb_instr_1062(instr);
 } else {
  return test_thumb_instr_1113(instr);
 }
}

static int test_thumb_instr_1102(uint32_t instr) {
 if((instr & (1u << 8))) {
  return test_thumb_instr_66(instr);
 } else {
  return test_thumb_instr_1101(instr);
 }
}

static int test_thumb_instr_1148(uint32_t instr) {
 if((instr & (1u << 9))) {
  if((instr & 0x170u) == 0x0u) {
   return 2;
  } else {
   return 0;
  }
 } else {
  return test_thumb_instr_1102(instr);
 }
}

static int test_thumb_instr_1097(uint32_t instr) {
 if((instr & 0x120u) == 0x20u) {
  return 0;
 } else {
  return 2;
 }
}

static int test_thumb_instr_1146(uint32_t instr) {
 if((instr & (1u << 10))) {
  if((instr & (1u << 9))) {
   if((instr & 0x130u) == 0x0u) {
    return 2;
   } else {
    return 0;
   }
  } else {
   return test_thumb_instr_1097(instr);
  }
 } else if((instr & (1u << 9))) {
  if((instr & 0x110u) == 0x0u) {
   return 2;
  } else {
   return 0;
  }
 } else {
  return test_thumb_instr_1096(instr);
 }
}

static int test_thumb_instr_932(uint32_t instr) {
 if((instr & 0x30u) == 0x30u) {
  return 0;
 } else {
  return 2;
 }
}

static int test_thumb_instr_1068(uint32_t instr) {
 if((instr & (1u << 6))) {
  return 0;
 } else {
  return test_thumb_instr_932(instr);
 }
}

static int test_thumb_instr_1139(uint32_t instr) {
 if((instr & (1u << 9))) {
  if((instr & (1u << 8))) {
   return test_thumb_instr_1068(instr);
  } else if((instr & 0x70u) == 0x0u) {
   return 2;
  } else {
   return 0;
  }
 } else {
  return test_thumb_instr_1102(instr);
 }
}

static int test_thumb_instr_1137(uint32_t instr) {
 if((instr & (1u << 10))) {
  if((instr & (1u << 9))) {
   if((instr & (1u << 8))) {
    return test_thumb_instr_66(instr);
   } else {
    return test_thumb_instr_391(instr);
   }
  } else {
   return test_thumb_instr_1097(instr);
  }
 } else {
  return test_thumb_instr_1096(instr);
 }
}

static int test_thumb_instr_1131(uint32_t instr) {
 if((instr & (1u << 9))) {
  if((instr & (1u << 8))) {
   return test_thumb_instr_1068(instr);
  } else {
   return test_thumb_instr_391(instr);
  }
 } else {
  return test_thumb_instr_1102(instr);
 }
}

static int test_thumb_instr_1129(uint32_t instr) {
 if((instr & (1u << 10))) {
  if((instr & (1u << 9))) {
   if((instr & (1u << 8))) {
    return test_thumb_instr_66(instr);
   } else {
    return test_thumb_instr_306(instr);
   }
  } else {
   return test_thumb_instr_1097(instr);
  }
 } else {
  return test_thumb_instr_1096(instr);
 }
}

static int test_thumb_instr_1104(uint32_t instr) {
 if((instr & (1u << 9))) {
  if((instr & (1u << 8))) {
   return test_thumb_instr_932(instr);
  } else {
   return test_thumb_instr_391(instr);
  }
 } else {
  return test_thumb_instr_1102(instr);
 }
}

static int test_thumb_instr_1099(uint32_t instr) {
 if((instr & 0x600u) == 0x400u) {
  return test_thumb_instr_1097(instr);
 } else {
  return test_thumb_instr_1096(instr);
 }
}

static int test_thumb_instr_1120(uint32_t instr) {
 if((instr & (1u << 11))) {
  if((instr & (1u << 10))) {
   return test_thumb_instr_1118(instr);
  } else {
   return test_thumb_instr_1104(instr);
  }
 } else {
  return test_thumb_instr_1099(instr);
 }
}

static int test_thumb_instr_1106(uint32_t instr) {
 if((instr & (1u << 11))) {
  if((instr & (1u << 10))) {
   return 0;
  } else {
   return test_thumb_instr_1104(instr);
  }
 } else {
  return test_thumb_instr_1099(instr);
 }
}

static int test_thumb_instr_1057(uint32_t instr) {
 if((instr & 0xf0000u) == 0xf0000u) {
  return 0;
 } else if((instr & 0xf000u) == 0xf000u) {
  return 0;
 } else {
  return 2;
 }
}

static int test_thumb_instr_1067(uint32_t instr) {
 if((instr & (1u << 10))) {
  if((instr & 0x2c0u) == 0xc0u) {
   return 0;
  } else {
   return test_thumb_instr_66(instr);
  }
 } else if((instr & 0x300u) == 0x200u) {
  return 2;
 } else {
  return test_thumb_instr_1062(instr);
 }
}

static int test_thumb_instr_1070(uint32_t instr) {
 if((instr & (1u << 8))) {
  return 0;
 } else {
  return test_thumb_instr_932(instr);
 }
}

static int test_thumb_instr_1069(uint32_t instr) {
 if((instr & 0xc0u) == 0xc0u) {
  return 0;
 } else {
  return test_thumb_instr_932(instr);
 }
}

static int test_thumb_instr_1073(uint32_t instr) {
 if((instr & (1u << 11))) {
  if((instr & (1u << 10))) {
   return 0;
  } else if((instr & (1u << 9))) {
   return test_thumb_instr_1070(instr);
  } else {
   return test_thumb_instr_1069(instr);
  }
 } else {
  return test_thumb_instr_1067(instr);
 }
}

static int test_thumb_instr_1026(uint32_t instr) {
 if((instr & 0x7c0u) == 0x0u) {
  return test_thumb_instr_166(instr);
 } else {
  return 0;
 }
}

static int test_thumb_instr_1028(uint32_t instr) {
 if((instr & (1u << 11))) {
  if((instr & 0x500u) == 0x0u) {
   return 0;
  } else {
   return 2;
  }
 } else {
  return test_thumb_instr_1026(instr);
 }
}

static int test_thumb_instr_1036(uint32_t instr) {
 if((instr & 0xf0000u) == 0xf0000u) {
  return 0;
 } else if((instr & 0xf000u) == 0xf000u) {
  return 0;
 } else {
  return test_thumb_instr_1028(instr);
 }
}

static int test_thumb_instr_1077(uint32_t instr) {
 if((instr & 0xf0000u) == 0xf0000u) {
  return 0;
 } else {
  return test_thumb_instr_1073(instr);
 }
}

static int test_thumb_instr_1047(uint32_t instr) {
 if((instr & 0xf0000u) == 0xf0000u) {
  return 0;
 } else if((instr & 0xf000u) == 0xf000u) {
  if((instr & (1u << 11))) {
   if((instr & 0x700u) == 0x400u) {
    return 2;
   } else {
    return 0;
   }
  } else {
   return test_thumb_instr_1026(instr);
  }
 } else {
  return test_thumb_instr_1028(instr);
 }
}

static int test_thumb_instr_105(uint32_t instr) {
 if((instr & 0xf00u) == 0xf00u) {
  return 0;
 } else {
  return 2;
 }
}

static int test_thumb_instr_800(uint32_t instr) {
 if((instr & (1u << 15))) {
  return 0;
 } else {
  return test_thumb_instr_105(instr);
 }
}

static int test_thumb_instr_804(uint32_t instr) {
 if((instr & 0xf0000u) == 0xf0000u) {
  return 0;
 } else {
  return test_thumb_instr_800(instr);
 }
}

static int test_thumb_instr_822(uint32_t instr) {
 if((instr & (1u << 20))) {
  return 0;
 } else {
  return test_thumb_instr_804(instr);
 }
}

static int test_thumb_instr_823(uint32_t instr) {
 if((instr & 0x300000u) == 0x0u) {
  return test_thumb_instr_804(instr);
 } else {
  return 0;
 }
}

static int test_thumb_instr_833(uint32_t instr) {
 if((instr & 0xf00u) == 0xf00u) {
  return 0;
 } else {
  return test_thumb_instr_66(instr);
 }
}

static int test_thumb_instr_855(uint32_t instr) {
 if((instr & (1u << 21))) {
  if((instr & (1u << 20))) {
   return 0;
  } else if((instr & 0xf0000u) == 0xf0000u) {
   return 0;
  } else if((instr & (1u << 15))) {
   return 0;
  } else if((instr & 0x7000u) == 0x0u) {
   if((instr & 0xf00u) == 0xf00u) {
    return 0;
   } else if((instr & 0xc0u) == 0x0u) {
    return test_thumb_instr_391(instr);
   } else {
    return test_thumb_instr_66(instr);
   }
  } else {
   return test_thumb_instr_833(instr);
  }
 } else if((instr & (1u << 20))) {
  return 0;
 } else if((instr & 0xf0000u) == 0xf0000u) {
  return 0;
 } else if((instr & (1u << 15))) {
  return 0;
 } else {
  return test_thumb_instr_833(instr);
 }
}

static int test_thumb_instr_825(uint32_t instr) {
 if((instr & 0x308000u) == 0x0u) {
  return test_thumb_instr_105(instr);
 } else {
  return 0;
 }
}

static int test_thumb_instr_829(uint32_t instr) {
 if((instr & (1u << 23))) {
  if((instr & (1u << 22))) {
   return test_thumb_instr_825(instr);
  } else if((instr & 0x300000u) == 0x200000u) {
   return test_thumb_instr_804(instr);
  } else {
   return 0;
  }
 } else if((instr & (1u << 22))) {
  return test_thumb_instr_825(instr);
 } else {
  return test_thumb_instr_823(instr);
 }
}

static int test_thumb_instr_809(uint32_t instr) {
 if((instr & 0xf0000u) == 0xf0000u) {
  return 0;
 } else if((instr & (1u << 15))) {
  return 0;
 } else {
  return 2;
 }
}

static int test_thumb_instr_810(uint32_t instr) {
 if((instr & (1u << 20))) {
  return test_thumb_instr_809(instr);
 } else {
  return test_thumb_instr_804(instr);
 }
}

static int test_thumb_instr_821(uint32_t instr) {
 if((instr & (1u << 24))) {
  if((instr & (1u << 23))) {
   if((instr & (1u << 22))) {
    if((instr & (1u << 21))) {
     return 0;
    } else {
     return test_thumb_instr_804(instr);
    }
   } else if((instr & (1u << 21))) {
    return test_thumb_instr_810(instr);
   } else {
    return 0;
   }
  } else if((instr & (1u << 22))) {
   return test_thumb_instr_804(instr);
  } else if((instr & (1u << 21))) {
   return 0;
  } else {
   return test_thumb_instr_810(instr);
  }
 } else if((instr & (1u << 23))) {
  if((instr & 0x600000u) == 0x0u) {
   return test_thumb_instr_810(instr);
  } else {
   return 0;
  }
 } else if((instr & (1u << 22))) {
  return test_thumb_instr_800(instr);
 } else if((instr & 0x300000u) == 0x100000u) {
  return test_thumb_instr_809(instr);
 } else {
  return test_thumb_instr_804(instr);
 }
}

static int test_thumb_instr_84(uint32_t instr) {
 if((instr & 0xfu) == 0x0u) {
  return 2;
 } else {
  return 0;
 }
}

static int test_thumb_instr_862(uint32_t instr) {
 if((instr & 0x30u) == 0x10u) {
  return 0;
 } else {
  return test_thumb_instr_84(instr);
 }
}

static int test_thumb_instr_864(uint32_t instr) {
 if((instr & 0xc0u) == 0x0u) {
  return test_thumb_instr_862(instr);
 } else {
  return 0;
 }
}

static int test_thumb_instr_861(uint32_t instr) {
 if((instr & 0xefu) == 0x20u) {
  return 2;
 } else {
  return 0;
 }
}

static int test_thumb_instr_85(uint32_t instr) {
 if((instr & 0x1fu) == 0x0u) {
  return 2;
 } else {
  return 0;
 }
}

static int test_thumb_instr_895(uint32_t instr) {
 if((instr & (1u << 8))) {
  return test_thumb_instr_861(instr);
 } else if((instr & 0xffu) == 0x20u) {
  return 2;
 } else {
  return 0;
 }
}

static int test_thumb_instr_782(uint32_t instr) {
 if((instr & (1u << 11))) {
  return test_thumb_instr_781(instr);
 } else if((instr & 0x500u) == 0x400u) {
  return test_thumb_instr_355(instr);
 } else {
  return test_thumb_instr_569(instr);
 }
}

static int test_thumb_instr_768(uint32_t instr) {
 if((instr & 0x51u) == 0x40u) {
  return 2;
 } else {
  return 0;
 }
}

static int test_thumb_instr_755(uint32_t instr) {
 if((instr & (1u << 10))) {
  if((instr & (1u << 9))) {
   return test_thumb_instr_271(instr);
  } else {
   return test_thumb_instr_393(instr);
  }
 } else if((instr & (1u << 9))) {
  return 0;
 } else {
  return test_thumb_instr_720(instr);
 }
}

static int test_thumb_instr_690(uint32_t instr) {
 if((instr & 0x150u) == 0x110u) {
  return 2;
 } else {
  return 0;
 }
}

static int test_thumb_instr_713(uint32_t instr) {
 if((instr & (1u << 6))) {
  return test_thumb_instr_306(instr);
 } else {
  return test_thumb_instr_266(instr);
 }
}

static int test_thumb_instr_749(uint32_t instr) {
 if((instr & (1u << 9))) {
  return test_thumb_instr_270(instr);
 } else {
  return test_thumb_instr_713(instr);
 }
}

static int test_thumb_instr_747(uint32_t instr) {
 if((instr & (1u << 10))) {
  if((instr & (1u << 9))) {
   return test_thumb_instr_745(instr);
  } else {
   return test_thumb_instr_306(instr);
  }
 } else {
  return test_thumb_instr_744(instr);
 }
}

static int test_thumb_instr_732(uint32_t instr) {
 if((instr & (1u << 8))) {
  return test_thumb_instr_696(instr);
 } else {
  return test_thumb_instr_393(instr);
 }
}

static int test_thumb_instr_729(uint32_t instr) {
 if((instr & (1u << 8))) {
  return test_thumb_instr_728(instr);
 } else {
  return test_thumb_instr_696(instr);
 }
}

static int test_thumb_instr_634(uint32_t instr) {
 if((instr & 0x51u) == 0x0u) {
  return 2;
 } else {
  return 0;
 }
}

static int test_thumb_instr_680(uint32_t instr) {
 if((instr & (1u << 8))) {
  return test_thumb_instr_270(instr);
 } else {
  return test_thumb_instr_634(instr);
 }
}

static int test_thumb_instr_699(uint32_t instr) {
 if((instr & (1u << 6))) {
  return test_thumb_instr_306(instr);
 } else {
  return test_thumb_instr_575(instr);
 }
}

static int test_thumb_instr_724(uint32_t instr) {
 if((instr & (1u << 10))) {
  if((instr & 0x250u) == 0x40u) {
   return 2;
  } else {
   return 0;
  }
 } else if((instr & (1u << 9))) {
  return 0;
 } else if((instr & (1u << 8))) {
  return test_thumb_instr_673(instr);
 } else {
  return test_thumb_instr_720(instr);
 }
}

static int test_thumb_instr_715(uint32_t instr) {
 if((instr & 0x300u) == 0x0u) {
  return test_thumb_instr_713(instr);
 } else {
  return test_thumb_instr_270(instr);
 }
}

static int test_thumb_instr_711(uint32_t instr) {
 if((instr & (1u << 10))) {
  if((instr & 0x210u) == 0x0u) {
   return 2;
  } else {
   return 0;
  }
 } else if((instr & (1u << 9))) {
  return test_thumb_instr_708(instr);
 } else if((instr & (1u << 8))) {
  return test_thumb_instr_672(instr);
 } else {
  return test_thumb_instr_704(instr);
 }
}

static int test_thumb_instr_678(uint32_t instr) {
 if((instr & (1u << 10))) {
  return test_thumb_instr_621(instr);
 } else {
  return test_thumb_instr_677(instr);
 }
}

static int test_thumb_instr_645(uint32_t instr) {
 if((instr & 0x600u) == 0x600u) {
  if((instr & (1u << 8))) {
   return test_thumb_instr_642(instr);
  } else {
   return test_thumb_instr_638(instr);
  }
 } else {
  return test_thumb_instr_640(instr);
 }
}

static int test_thumb_instr_574(uint32_t instr) {
 if((instr & (1u << 8))) {
  return test_thumb_instr_568(instr);
 } else {
  return test_thumb_instr_573(instr);
 }
}

static int test_thumb_instr_591(uint32_t instr) {
 if((instr & (1u << 9))) {
  if((instr & (1u << 8))) {
   if((instr & (1u << 6))) {
    return test_thumb_instr_575(instr);
   } else {
    return 2;
   }
  } else {
   return test_thumb_instr_569(instr);
  }
 } else {
  return test_thumb_instr_568(instr);
 }
}

static int test_thumb_instr_582(uint32_t instr) {
 if((instr & (1u << 8))) {
  return test_thumb_instr_569(instr);
 } else {
  return test_thumb_instr_270(instr);
 }
}

static int test_thumb_instr_394(uint32_t instr) {
 if((instr & (1u << 7))) {
  return test_thumb_instr_393(instr);
 } else if((instr & 0x60u) == 0x20u) {
  return 0;
 } else {
  return test_thumb_instr_306(instr);
 }
}

static int test_thumb_instr_407(uint32_t instr) {
 if((instr & 0xe00u) == 0xa00u) {
  return test_thumb_instr_394(instr);
 } else {
  return 2;
 }
}

static int test_thumb_instr_305(uint32_t instr) {
 if((instr & (1u << 4))) {
  return test_thumb_instr_84(instr);
 } else {
  return 2;
 }
}

static int test_thumb_instr_395(uint32_t instr) {
 if((instr & 0x1fu) == 0x10u) {
  return 2;
 } else {
  return 0;
 }
}

static int test_thumb_instr_503(uint32_t instr) {
 if((instr & (1u << 7))) {
  if((instr & (1u << 6))) {
   return test_thumb_instr_305(instr);
  } else {
   return test_thumb_instr_395(instr);
  }
 } else if((instr & 0x60u) == 0x20u) {
  return test_thumb_instr_395(instr);
 } else {
  return test_thumb_instr_305(instr);
 }
}

static int test_thumb_instr_528(uint32_t instr) {
 if((instr & 0xf000u) == 0xf000u) {
  return test_thumb_instr_407(instr);
 } else if((instr & 0xe00u) == 0xa00u) {
  if((instr & (1u << 8))) {
   return test_thumb_instr_503(instr);
  } else {
   return test_thumb_instr_394(instr);
  }
 } else {
  return 2;
 }
}

static int test_thumb_instr_497(uint32_t instr) {
 if((instr & (1u << 5))) {
  return 0;
 } else {
  return test_thumb_instr_305(instr);
 }
}

static int test_thumb_instr_499(uint32_t instr) {
 if((instr & (1u << 7))) {
  return test_thumb_instr_393(instr);
 } else if((instr & (1u << 6))) {
  return test_thumb_instr_306(instr);
 } else {
  return test_thumb_instr_497(instr);
 }
}

static int test_thumb_instr_507(uint32_t instr) {
 if((instr & 0xe00u) == 0xa00u) {
  if((instr & (1u << 8))) {
   return test_thumb_instr_503(instr);
  } else {
   return test_thumb_instr_499(instr);
  }
 } else {
  return 2;
 }
}

static int test_thumb_instr_511(uint32_t instr) {
 if((instr & 0xf000u) == 0xf000u) {
  return test_thumb_instr_407(instr);
 } else {
  return test_thumb_instr_507(instr);
 }
}

static int test_thumb_instr_434(uint32_t instr) {
 if((instr & 0xf0u) == 0x0u) {
  return 2;
 } else {
  return 0;
 }
}

static int test_thumb_instr_443(uint32_t instr) {
 if((instr & 0xe00u) == 0xa00u) {
  return test_thumb_instr_434(instr);
 } else {
  return 2;
 }
}

static int test_thumb_instr_413(uint32_t instr) {
 if((instr & 0x7fu) == 0x40u) {
  return 2;
 } else {
  return 0;
 }
}

static int test_thumb_instr_86(uint32_t instr) {
 if((instr & 0x3fu) == 0x0u) {
  return 2;
 } else {
  return 0;
 }
}

static int test_thumb_instr_414(uint32_t instr) {
 if((instr & (1u << 7))) {
  return test_thumb_instr_413(instr);
 } else if((instr & (1u << 6))) {
  return test_thumb_instr_86(instr);
 } else {
  return test_thumb_instr_391(instr);
 }
}

static int test_thumb_instr_425(uint32_t instr) {
 if((instr & 0xe00u) == 0xa00u) {
  return test_thumb_instr_414(instr);
 } else {
  return 2;
 }
}

static int test_thumb_instr_490(uint32_t instr) {
 if((instr & (1u << 16))) {
  return test_thumb_instr_389(instr);
 } else if((instr & 0xf000u) == 0xf000u) {
  return test_thumb_instr_306(instr);
 } else if((instr & 0xe00u) == 0xa00u) {
  if((instr & 0x160u) == 0x100u) {
   return test_thumb_instr_305(instr);
  } else {
   return test_thumb_instr_306(instr);
  }
 } else {
  return 2;
 }
}

static int test_thumb_instr_308(uint32_t instr) {
 if((instr & 0x60u) == 0x0u) {
  return test_thumb_instr_305(instr);
 } else {
  return test_thumb_instr_306(instr);
 }
}

static int test_thumb_instr_472(uint32_t instr) {
 if((instr & 0xf000u) == 0xf000u) {
  return test_thumb_instr_306(instr);
 } else if((instr & 0xe00u) == 0xa00u) {
  if((instr & (1u << 8))) {
   return test_thumb_instr_308(instr);
  } else if((instr & 0x90u) == 0x90u) {
   return 0;
  } else {
   return 2;
  }
 } else {
  return 2;
 }
}

static int test_thumb_instr_481(uint32_t instr) {
 if((instr & (1u << 16))) {
  if((instr & 0xf000u) == 0xf000u) {
   return test_thumb_instr_306(instr);
  } else if((instr & 0xe00u) == 0xa00u) {
   if((instr & 0x180u) == 0x0u) {
    return 2;
   } else {
    return test_thumb_instr_306(instr);
   }
  } else {
   return 2;
  }
 } else {
  return test_thumb_instr_472(instr);
 }
}

static int test_thumb_instr_344(uint32_t instr) {
 if((instr & 0xe00u) == 0xa00u) {
  if((instr & (1u << 8))) {
   return test_thumb_instr_305(instr);
  } else {
   return test_thumb_instr_306(instr);
  }
 } else {
  return 2;
 }
}

static int test_thumb_instr_352(uint32_t instr) {
 if((instr & 0xf000u) == 0xf000u) {
  return test_thumb_instr_321(instr);
 } else {
  return test_thumb_instr_344(instr);
 }
}

static int test_thumb_instr_396(uint32_t instr) {
 if((instr & (1u << 5))) {
  return test_thumb_instr_395(instr);
 } else {
  return test_thumb_instr_306(instr);
 }
}

static int test_thumb_instr_398(uint32_t instr) {
 if((instr & 0x3fu) == 0x30u) {
  return 2;
 } else {
  return 0;
 }
}

static int test_thumb_instr_309(uint32_t instr) {
 if((instr & (1u << 5))) {
  return test_thumb_instr_305(instr);
 } else {
  return test_thumb_instr_306(instr);
 }
}

static int test_thumb_instr_411(uint32_t instr) {
 if((instr & 0xf000u) == 0xf000u) {
  return test_thumb_instr_407(instr);
 } else if((instr & 0xe00u) == 0xa00u) {
  if((instr & (1u << 8))) {
   if((instr & (1u << 7))) {
    if((instr & (1u << 6))) {
     return test_thumb_instr_309(instr);
    } else {
     return test_thumb_instr_398(instr);
    }
   } else if((instr & (1u << 6))) {
    return test_thumb_instr_309(instr);
   } else {
    return test_thumb_instr_396(instr);
   }
  } else {
   return test_thumb_instr_394(instr);
  }
 } else {
  return 2;
 }
}

static int test_thumb_instr_415(uint32_t instr) {
 if((instr & (1u << 5))) {
  return test_thumb_instr_395(instr);
 } else {
  return test_thumb_instr_85(instr);
 }
}

static int test_thumb_instr_310(uint32_t instr) {
 if((instr & 0x60u) == 0x40u) {
  return test_thumb_instr_306(instr);
 } else {
  return test_thumb_instr_305(instr);
 }
}

static int test_thumb_instr_330(uint32_t instr) {
 if((instr & 0xe00u) == 0xa00u) {
  if((instr & (1u << 8))) {
   return test_thumb_instr_310(instr);
  } else {
   return test_thumb_instr_306(instr);
  }
 } else {
  return 2;
 }
}

static int test_thumb_instr_314(uint32_t instr) {
 if((instr & 0xe00u) == 0xa00u) {
  if((instr & (1u << 8))) {
   return test_thumb_instr_310(instr);
  } else {
   return test_thumb_instr_308(instr);
  }
 } else {
  return 2;
 }
}

static int test_thumb_instr_264(uint32_t instr) {
 if((instr & 0xf0000u) == 0xf0000u) {
  return 0;
 } else {
  return test_thumb_instr_260(instr);
 }
}

static int test_thumb_instr_297(uint32_t instr) {
 if((instr & 0xf0000u) == 0xf0000u) {
  return 0;
 } else if((instr & 0xf01u) == 0xb01u) {
  return 0;
 } else {
  return 2;
 }
}

static int test_thumb_instr_225(uint32_t instr) {
 if((instr & 0xf00u) == 0xf00u) {
  return 0;
 } else {
  return test_thumb_instr_166(instr);
 }
}

static int test_thumb_instr_229(uint32_t instr) {
 if((instr & 0xf0000u) == 0xf0000u) {
  return 0;
 } else {
  return test_thumb_instr_225(instr);
 }
}

static int test_thumb_instr_233(uint32_t instr) {
 if((instr & 0xf0000u) == 0xf0000u) {
  return 0;
 } else {
  return test_thumb_instr_166(instr);
 }
}

static int test_thumb_instr_234(uint32_t instr) {
 if((instr & (1u << 20))) {
  return test_thumb_instr_233(instr);
 } else {
  return test_thumb_instr_229(instr);
 }
}

static int test_thumb_instr_237(uint32_t instr) {
 if((instr & (1u << 21))) {
  return 0;
 } else {
  return test_thumb_instr_234(instr);
 }
}

static int test_thumb_instr_113(uint32_t instr) {
 if((instr & 0xf0000u) == 0xf0000u) {
  return 0;
 } else if((instr & 0xf000u) == 0xf000u) {
  return 0;
 } else {
  return test_thumb_instr_105(instr);
 }
}

static int test_thumb_instr_101(uint32_t instr) {
 if((instr & (1u << 20))) {
  if((instr & 0xf0000u) == 0xf0000u) {
   return 0;
  } else if((instr & 0xffffu) == 0xc000u) {
   return 2;
  } else {
   return 0;
  }
 } else if((instr & 0xfffe0u) == 0xdc000u) {
  return 2;
 } else {
  return 0;
 }
}

static int test_thumb_instr_151(uint32_t instr) {
 if((instr & 0x7fffu) == 0x0u) {
  return 0;
 } else {
  return 2;
 }
}

static int test_thumb_instr_152(uint32_t instr) {
 if((instr & (1u << 15))) {
  return 0;
 } else {
  return test_thumb_instr_151(instr);
 }
}

static int test_thumb_instr_145(uint32_t instr) {
 if((instr & (1u << 15))) {
  return 0;
 } else if((instr & (1u << 14))) {
  if((instr & (1u << 13))) {
   return 0;
  } else {
   return 2;
  }
 } else if((instr & (1u << 13))) {
  return 0;
 } else if((instr & 0x1fffu) == 0x0u) {
  return 0;
 } else {
  return 2;
 }
}

static int test_thumb_instr_149(uint32_t instr) {
 if((instr & 0xf0000u) == 0xf0000u) {
  return 0;
 } else {
  return test_thumb_instr_145(instr);
 }
}

static int test_thumb_instr_157(uint32_t instr) {
 if((instr & (1u << 20))) {
  if((instr & 0xf0000u) == 0xf0000u) {
   return 0;
  } else {
   return test_thumb_instr_152(instr);
  }
 } else {
  return test_thumb_instr_149(instr);
 }
}

static int test_thumb_instr(uint32_t instr) {
 if((instr & (1u << 31))) {
  if((instr & (1u << 30))) {
   if((instr & (1u << 29))) {
    if((instr & (1u << 28))) {
     if((instr & (1u << 27))) {
      if((instr & (1u << 26))) {
       if((instr & (1u << 25))) {
        if((instr & (1u << 24))) {
         if((instr & (1u << 23))) {
          if((instr & (1u << 21))) {
           if((instr & (1u << 20))) {
            if((instr & (1u << 19))) {
             if((instr & (1u << 18))) {
              if((instr & 0x30000u) == 0x30000u) {
               if((instr & (1u << 12))) {
                if((instr & (1u << 11))) {
                 if((instr & (1u << 10))) {
                  return test_thumb_instr_1930(instr);
                 } else if((instr & (1u << 9))) {
                  return test_thumb_instr_306(instr);
                 } else {
                  return test_thumb_instr_2010(instr);
                 }
                } else {
                 return test_thumb_instr_270(instr);
                }
               } else if((instr & (1u << 11))) {
                if((instr & (1u << 10))) {
                 return test_thumb_instr_1922(instr);
                } else if((instr & (1u << 9))) {
                 return test_thumb_instr_708(instr);
                } else {
                 return test_thumb_instr_2010(instr);
                }
               } else {
                return test_thumb_instr_573(instr);
               }
              } else {
               return test_thumb_instr_1949(instr);
              }
             } else if((instr & (1u << 17))) {
              if((instr & (1u << 16))) {
               if((instr & (1u << 12))) {
                if((instr & (1u << 11))) {
                 return test_thumb_instr_1931(instr);
                } else if((instr & (1u << 10))) {
                 return test_thumb_instr_569(instr);
                } else {
                 return test_thumb_instr_270(instr);
                }
               } else if((instr & (1u << 11))) {
                return test_thumb_instr_1923(instr);
               } else if((instr & (1u << 10))) {
                return test_thumb_instr_568(instr);
               } else {
                return test_thumb_instr_573(instr);
               }
              } else if((instr & (1u << 12))) {
               if((instr & (1u << 11))) {
                return test_thumb_instr_1931(instr);
               } else if((instr & (1u << 10))) {
                return test_thumb_instr_270(instr);
               } else if((instr & (1u << 9))) {
                return test_thumb_instr_1942(instr);
               } else if((instr & 0x180u) == 0x80u) {
                return test_thumb_instr_569(instr);
               } else {
                return test_thumb_instr_270(instr);
               }
              } else if((instr & (1u << 11))) {
               return test_thumb_instr_1923(instr);
              } else if((instr & (1u << 10))) {
               return test_thumb_instr_573(instr);
              } else if((instr & (1u << 9))) {
               return test_thumb_instr_1937(instr);
              } else if((instr & (1u << 8))) {
               return test_thumb_instr_774(instr);
              } else if((instr & (1u << 7))) {
               return test_thumb_instr_568(instr);
              } else {
               return test_thumb_instr_573(instr);
              }
             } else if((instr & (1u << 16))) {
              if((instr & (1u << 12))) {
               if((instr & (1u << 11))) {
                return test_thumb_instr_1931(instr);
               } else {
                return test_thumb_instr_1926(instr);
               }
              } else if((instr & (1u << 11))) {
               return test_thumb_instr_1923(instr);
              } else {
               return test_thumb_instr_1918(instr);
              }
             } else if((instr & (1u << 12))) {
              if((instr & (1u << 11))) {
               return test_thumb_instr_1914(instr);
              } else if((instr & (1u << 10))) {
               if((instr & 0x300u) == 0x100u) {
                return test_thumb_instr_270(instr);
               } else {
                return test_thumb_instr_569(instr);
               }
              } else if((instr & (1u << 9))) {
               return test_thumb_instr_570(instr);
              } else if((instr & 0x180u) == 0x0u) {
               return test_thumb_instr_569(instr);
              } else {
               return test_thumb_instr_270(instr);
              }
             } else if((instr & (1u << 11))) {
              return test_thumb_instr_1907(instr);
             } else if((instr & (1u << 10))) {
              if((instr & 0x300u) == 0x100u) {
               return test_thumb_instr_573(instr);
              } else {
               return test_thumb_instr_568(instr);
              }
             } else if((instr & (1u << 9))) {
              return test_thumb_instr_1904(instr);
             } else if((instr & 0x180u) == 0x0u) {
              return test_thumb_instr_568(instr);
             } else {
              return test_thumb_instr_573(instr);
             }
            } else if((instr & (1u << 18))) {
             if((instr & (1u << 17))) {
              if((instr & (1u << 16))) {
               return test_thumb_instr_1949(instr);
              } else if((instr & (1u << 12))) {
               if((instr & (1u << 11))) {
                return test_thumb_instr_1931(instr);
               } else if((instr & (1u << 10))) {
                if((instr & 0x380u) == 0x200u) {
                 return test_thumb_instr_647(instr);
                } else {
                 return test_thumb_instr_270(instr);
                }
               } else if((instr & (1u << 9))) {
                return test_thumb_instr_1942(instr);
               } else if((instr & 0x180u) == 0x0u) {
                return test_thumb_instr_270(instr);
               } else {
                return test_thumb_instr_569(instr);
               }
              } else if((instr & (1u << 11))) {
               return test_thumb_instr_1923(instr);
              } else if((instr & (1u << 10))) {
               if((instr & (1u << 9))) {
                if((instr & (1u << 8))) {
                 return test_thumb_instr_1936(instr);
                } else if((instr & (1u << 7))) {
                 return test_thumb_instr_573(instr);
                } else {
                 return test_thumb_instr_1833(instr);
                }
               } else {
                return test_thumb_instr_573(instr);
               }
              } else if((instr & (1u << 9))) {
               return test_thumb_instr_1937(instr);
              } else if((instr & 0x180u) == 0x0u) {
               return test_thumb_instr_573(instr);
              } else {
               return test_thumb_instr_568(instr);
              }
             } else if((instr & (1u << 16))) {
              return test_thumb_instr_1933(instr);
             } else if((instr & (1u << 12))) {
              if((instr & (1u << 11))) {
               return test_thumb_instr_1931(instr);
              } else if((instr & 0x600u) == 0x600u) {
               return test_thumb_instr_569(instr);
              } else {
               return test_thumb_instr_570(instr);
              }
             } else if((instr & (1u << 11))) {
              return test_thumb_instr_1923(instr);
             } else if((instr & 0x600u) == 0x600u) {
              return test_thumb_instr_568(instr);
             } else {
              return test_thumb_instr_1904(instr);
             }
            } else if((instr & (1u << 17))) {
             if((instr & (1u << 16))) {
              return test_thumb_instr_1949(instr);
             } else if((instr & (1u << 12))) {
              if((instr & (1u << 11))) {
               return test_thumb_instr_1931(instr);
              } else if((instr & (1u << 10))) {
               return test_thumb_instr_270(instr);
              } else if((instr & (1u << 9))) {
               return test_thumb_instr_1942(instr);
              } else {
               return test_thumb_instr_569(instr);
              }
             } else if((instr & (1u << 11))) {
              return test_thumb_instr_1923(instr);
             } else if((instr & (1u << 10))) {
              return test_thumb_instr_573(instr);
             } else if((instr & (1u << 9))) {
              return test_thumb_instr_1937(instr);
             } else {
              return test_thumb_instr_568(instr);
             }
            } else if((instr & (1u << 16))) {
             return test_thumb_instr_1933(instr);
            } else if((instr & (1u << 12))) {
             if((instr & (1u << 11))) {
              return test_thumb_instr_1914(instr);
             } else if((instr & (1u << 10))) {
              return test_thumb_instr_569(instr);
             } else if((instr & (1u << 9))) {
              return test_thumb_instr_570(instr);
             } else if((instr & 0x180u) == 0x180u) {
              return test_thumb_instr_270(instr);
             } else {
              return test_thumb_instr_569(instr);
             }
            } else if((instr & (1u << 11))) {
             return test_thumb_instr_1907(instr);
            } else if((instr & (1u << 10))) {
             return test_thumb_instr_568(instr);
            } else if((instr & (1u << 9))) {
             return test_thumb_instr_1904(instr);
            } else if((instr & 0x180u) == 0x180u) {
             return test_thumb_instr_573(instr);
            } else {
             return test_thumb_instr_568(instr);
            }
           } else if((instr & (1u << 16))) {
            if((instr & (1u << 12))) {
             if((instr & (1u << 11))) {
              return test_thumb_instr_781(instr);
             } else {
              return test_thumb_instr_270(instr);
             }
            } else if((instr & (1u << 11))) {
             if((instr & (1u << 10))) {
              if((instr & (1u << 9))) {
               return test_thumb_instr_745(instr);
              } else {
               return test_thumb_instr_621(instr);
              }
             } else {
              return test_thumb_instr_1879(instr);
             }
            } else {
             return test_thumb_instr_1878(instr);
            }
           } else if((instr & (1u << 12))) {
            if((instr & (1u << 11))) {
             return test_thumb_instr_781(instr);
            } else {
             return test_thumb_instr_1842(instr);
            }
           } else if((instr & (1u << 11))) {
            if((instr & (1u << 10))) {
             if((instr & (1u << 9))) {
              return test_thumb_instr_745(instr);
             } else {
              return test_thumb_instr_1870(instr);
             }
            } else if((instr & (1u << 9))) {
             return test_thumb_instr_1868(instr);
            } else if((instr & (1u << 8))) {
             return test_thumb_instr_720(instr);
            } else {
             return test_thumb_instr_704(instr);
            }
           } else if((instr & (1u << 10))) {
            if((instr & (1u << 9))) {
             return test_thumb_instr_1865(instr);
            } else if((instr & (1u << 8))) {
             return test_thumb_instr_696(instr);
            } else {
             return test_thumb_instr_1864(instr);
            }
           } else if((instr & 0x300u) == 0x300u) {
            return test_thumb_instr_630(instr);
           } else {
            return test_thumb_instr_696(instr);
           }
          } else if((instr & (1u << 20))) {
           if((instr & (1u << 16))) {
            if((instr & (1u << 12))) {
             return test_thumb_instr_1860(instr);
            } else if((instr & (1u << 11))) {
             if((instr & (1u << 10))) {
              return test_thumb_instr_622(instr);
             } else {
              return test_thumb_instr_1879(instr);
             }
            } else {
             return test_thumb_instr_1878(instr);
            }
           } else if((instr & (1u << 12))) {
            return test_thumb_instr_1855(instr);
           } else if((instr & (1u << 11))) {
            if((instr & (1u << 10))) {
             if((instr & (1u << 9))) {
              return 0;
             } else {
              return test_thumb_instr_1870(instr);
             }
            } else if((instr & (1u << 9))) {
             return test_thumb_instr_1868(instr);
            } else if((instr & (1u << 8))) {
             return test_thumb_instr_673(instr);
            } else {
             return test_thumb_instr_704(instr);
            }
           } else if((instr & (1u << 10))) {
            return test_thumb_instr_1865(instr);
           } else if((instr & (1u << 8))) {
            return test_thumb_instr_630(instr);
           } else {
            return test_thumb_instr_696(instr);
           }
          } else if((instr & (1u << 19))) {
           if((instr & (1u << 16))) {
            if((instr & (1u << 12))) {
             return test_thumb_instr_1860(instr);
            } else if((instr & (1u << 11))) {
             return test_thumb_instr_1853(instr);
            } else if((instr & (1u << 10))) {
             return test_thumb_instr_1857(instr);
            } else {
             return test_thumb_instr_686(instr);
            }
           } else if((instr & (1u << 12))) {
            return test_thumb_instr_1855(instr);
           } else if((instr & (1u << 11))) {
            return test_thumb_instr_1853(instr);
           } else if((instr & 0x500u) == 0x400u) {
            return test_thumb_instr_1833(instr);
           } else {
            return test_thumb_instr_630(instr);
           }
          } else if((instr & (1u << 16))) {
           if((instr & (1u << 12))) {
            if((instr & (1u << 11))) {
             return test_thumb_instr_655(instr);
            } else {
             return test_thumb_instr_270(instr);
            }
           } else if((instr & (1u << 11))) {
            return test_thumb_instr_1839(instr);
           } else if((instr & (1u << 10))) {
            if((instr & (1u << 8))) {
             return test_thumb_instr_631(instr);
            } else {
             return test_thumb_instr_658(instr);
            }
           } else {
            return test_thumb_instr_659(instr);
           }
          } else if((instr & (1u << 12))) {
           if((instr & (1u << 11))) {
            return test_thumb_instr_655(instr);
           } else {
            return test_thumb_instr_1842(instr);
           }
          } else if((instr & (1u << 11))) {
           return test_thumb_instr_1839(instr);
          } else if((instr & 0x500u) == 0x400u) {
           if((instr & (1u << 7))) {
            return test_thumb_instr_1833(instr);
           } else {
            return test_thumb_instr_633(instr);
           }
          } else {
           return test_thumb_instr_631(instr);
          }
         } else if((instr & (1u << 21))) {
          if((instr & (1u << 20))) {
           return test_thumb_instr_626(instr);
          } else if((instr & 0x11000u) == 0x0u) {
           if((instr & (1u << 11))) {
            if((instr & (1u << 10))) {
             if((instr & (1u << 9))) {
              return test_thumb_instr_1800(instr);
             } else if((instr & (1u << 8))) {
              return test_thumb_instr_576(instr);
             } else {
              return 0;
             }
            } else {
             return test_thumb_instr_1813(instr);
            }
           } else {
            return test_thumb_instr_568(instr);
           }
          } else if((instr & (1u << 11))) {
           if((instr & 0x600u) == 0x400u) {
            if((instr & 0x150u) == 0x100u) {
             return 2;
            } else {
             return 0;
            }
           } else {
            return test_thumb_instr_1805(instr);
           }
          } else {
           return test_thumb_instr_569(instr);
          }
         } else if((instr & (1u << 20))) {
          if((instr & 0x11000u) == 0x0u) {
           if((instr & (1u << 11))) {
            if((instr & (1u << 10))) {
             return 0;
            } else {
             return test_thumb_instr_1813(instr);
            }
           } else {
            return test_thumb_instr_568(instr);
           }
          } else if((instr & (1u << 11))) {
           if((instr & (1u << 10))) {
            return 0;
           } else {
            return test_thumb_instr_1805(instr);
           }
          } else {
           return test_thumb_instr_569(instr);
          }
         } else if((instr & 0x11000u) == 0x0u) {
          if((instr & (1u << 11))) {
           if((instr & (1u << 10))) {
            if((instr & (1u << 9))) {
             return test_thumb_instr_1800(instr);
            } else {
             return test_thumb_instr_687(instr);
            }
           } else if((instr & (1u << 9))) {
            if((instr & 0x140u) == 0x0u) {
             return 2;
            } else {
             return 0;
            }
           } else {
            return test_thumb_instr_568(instr);
           }
          } else {
           return test_thumb_instr_568(instr);
          }
         } else if((instr & (1u << 11))) {
          if((instr & (1u << 10))) {
           if((instr & (1u << 9))) {
            return test_thumb_instr_1805(instr);
           } else {
            return test_thumb_instr_603(instr);
           }
          } else if((instr & 0x300u) == 0x300u) {
           return 0;
          } else {
           return test_thumb_instr_569(instr);
          }
         } else {
          return test_thumb_instr_569(instr);
         }
        } else if((instr & (1u << 23))) {
         if((instr & (1u << 21))) {
          if((instr & (1u << 20))) {
           if((instr & (1u << 19))) {
            if((instr & (1u << 18))) {
             if((instr & 0xe00u) == 0xa00u) {
              return test_thumb_instr_393(instr);
             } else {
              return 2;
             }
            } else if((instr & 0xe00u) == 0xa00u) {
             if((instr & 0xd0u) == 0x40u) {
              return 2;
             } else {
              return 0;
             }
            } else {
             return 2;
            }
           } else {
            return test_thumb_instr_260(instr);
           }
          } else if((instr & 0xf000u) == 0xf000u) {
           if((instr & 0xe00u) == 0xa00u) {
            return 0;
           } else {
            return test_thumb_instr_306(instr);
           }
          } else {
           return test_thumb_instr_260(instr);
          }
         } else if((instr & (1u << 20))) {
          return test_thumb_instr_260(instr);
         } else {
          return test_thumb_instr_389(instr);
         }
        } else if((instr & 0x10f000u) == 0xf000u) {
         return test_thumb_instr_363(instr);
        } else if((instr & 0xe00u) == 0xa00u) {
         return test_thumb_instr_355(instr);
        } else {
         return 2;
        }
       } else if((instr & 0x1800000u) == 0x0u) {
        if((instr & (1u << 22))) {
         if((instr & (1u << 21))) {
          return test_thumb_instr_289(instr);
         } else if((instr & 0xf0000u) == 0xf0000u) {
          return 0;
         } else if((instr & 0xf000u) == 0xf000u) {
          return 0;
         } else {
          return test_thumb_instr_260(instr);
         }
        } else if((instr & (1u << 21))) {
         return test_thumb_instr_289(instr);
        } else {
         return 0;
        }
       } else {
        return test_thumb_instr_289(instr);
       }
      } else if((instr & (1u << 25))) {
       if((instr & (1u << 24))) {
        if((instr & (1u << 23))) {
         if((instr & (1u << 22))) {
          if((instr & (1u << 21))) {
           if((instr & (1u << 20))) {
            return 0;
           } else if((instr & 0xf0000u) == 0xf0000u) {
            return 0;
           } else if((instr & 0xf000u) == 0xf000u) {
            return 0;
           } else if((instr & 0xf00u) == 0xf00u) {
            return 0;
           } else if((instr & (1u << 7))) {
            return 0;
           } else if((instr & (1u << 6))) {
            if((instr & 0x30u) == 0x20u) {
             return test_thumb_instr_166(instr);
            } else {
             return 0;
            }
           } else if((instr & 0x30u) == 0x0u) {
            return test_thumb_instr_166(instr);
           } else {
            return 0;
           }
          } else if((instr & (1u << 20))) {
           if((instr & 0xf0000u) == 0xf0000u) {
            return 0;
           } else if((instr & 0xf000u) == 0xf000u) {
            return 0;
           } else if((instr & 0xf00u) == 0xf00u) {
            return 0;
           } else if((instr & 0xe0u) == 0xc0u) {
            return test_thumb_instr_166(instr);
           } else {
            return 0;
           }
          } else if((instr & 0xf0000u) == 0xf0000u) {
           return 0;
          } else if((instr & 0xf000u) == 0xf000u) {
           return 0;
          } else if((instr & 0xf00u) == 0xf00u) {
           return 0;
          } else if((instr & (1u << 7))) {
           if((instr & 0x60u) == 0x60u) {
            return 0;
           } else {
            return test_thumb_instr_166(instr);
           }
          } else {
           return test_thumb_instr_1206(instr);
          }
         } else if((instr & (1u << 20))) {
          if((instr & 0xf0000u) == 0xf0000u) {
           return 0;
          } else if((instr & 0xf000u) == 0xf000u) {
           if((instr & 0xf00u) == 0xf00u) {
            return 0;
           } else if((instr & 0xf0u) == 0xf0u) {
            return test_thumb_instr_166(instr);
           } else {
            return 0;
           }
          } else {
           return 0;
          }
         } else if((instr & 0xf0000u) == 0xf0000u) {
          return 0;
         } else if((instr & 0xf000u) == 0xf000u) {
          return 0;
         } else {
          return test_thumb_instr_1233(instr);
         }
        } else if((instr & (1u << 22))) {
         if((instr & (1u << 21))) {
          if((instr & (1u << 20))) {
           if((instr & 0xf0000u) == 0xf0000u) {
            return 0;
           } else {
            return test_thumb_instr_1233(instr);
           }
          } else if((instr & 0xf0000u) == 0xf0000u) {
           return 0;
          } else if((instr & 0xf000u) == 0xf000u) {
           return 0;
          } else {
           return test_thumb_instr_1650(instr);
          }
         } else {
          return test_thumb_instr_1670(instr);
         }
        } else if((instr & (1u << 21))) {
         return test_thumb_instr_1670(instr);
        } else if((instr & (1u << 20))) {
         if((instr & 0xf0000u) == 0xf0000u) {
          return 0;
         } else if((instr & 0xf00u) == 0xf00u) {
          return 0;
         } else if((instr & 0xc0u) == 0x0u) {
          return test_thumb_instr_166(instr);
         } else {
          return 0;
         }
        } else if((instr & 0xf0000u) == 0xf0000u) {
         return 0;
        } else if((instr & 0xf000u) == 0xf000u) {
         return test_thumb_instr_1233(instr);
        } else {
         return test_thumb_instr_1650(instr);
        }
       } else if((instr & (1u << 23))) {
        if((instr & (1u << 22))) {
         if((instr & 0x300000u) == 0x300000u) {
          return 0;
         } else if((instr & 0xf0000u) == 0xf0000u) {
          return 0;
         } else if((instr & 0xf000u) == 0xf000u) {
          if((instr & 0xf00u) == 0xf00u) {
           return 0;
          } else if((instr & (1u << 7))) {
           return 0;
          } else {
           return test_thumb_instr_1244(instr);
          }
         } else {
          return 0;
         }
        } else if((instr & (1u << 21))) {
         if((instr & (1u << 20))) {
          if((instr & (1u << 19))) {
           if((instr & (1u << 18))) {
            if((instr & (1u << 17))) {
             if((instr & 0x1f000u) == 0xf000u) {
              if((instr & 0xf00u) == 0xf00u) {
               return 0;
              } else if((instr & 0xf1u) == 0x80u) {
               return 2;
              } else {
               return 0;
              }
             } else {
              return 0;
             }
            } else if((instr & (1u << 16))) {
             if((instr & 0xf000u) == 0xf000u) {
              if((instr & 0xf00u) == 0xf00u) {
               return 0;
              } else if((instr & 0xf2u) == 0x80u) {
               return 2;
              } else {
               return 0;
              }
             } else {
              return 0;
             }
            } else if((instr & 0xf000u) == 0xf000u) {
             if((instr & 0xf00u) == 0xf00u) {
              return 0;
             } else if((instr & 0xf0u) == 0x80u) {
              return test_thumb_instr_164(instr);
             } else {
              return 0;
             }
            } else {
             return 0;
            }
           } else if((instr & (1u << 17))) {
            if((instr & (1u << 16))) {
             if((instr & 0xf000u) == 0xf000u) {
              if((instr & 0xf00u) == 0xf00u) {
               return 0;
              } else if((instr & 0xf4u) == 0x80u) {
               return 2;
              } else {
               return 0;
              }
             } else {
              return 0;
             }
            } else if((instr & 0xf000u) == 0xf000u) {
             if((instr & 0xf00u) == 0xf00u) {
              return 0;
             } else if((instr & 0xf0u) == 0x80u) {
              return test_thumb_instr_1272(instr);
             } else {
              return 0;
             }
            } else {
             return 0;
            }
           } else if((instr & (1u << 16))) {
            if((instr & 0xf000u) == 0xf000u) {
             if((instr & 0xf00u) == 0xf00u) {
              return 0;
             } else if((instr & 0xf0u) == 0x80u) {
              return test_thumb_instr_1259(instr);
             } else {
              return 0;
             }
            } else {
             return 0;
            }
           } else if((instr & 0xf000u) == 0xf000u) {
            if((instr & 0xf00u) == 0xf00u) {
             return 0;
            } else if((instr & 0xf0u) == 0x80u) {
             return test_thumb_instr_165(instr);
            } else {
             return 0;
            }
           } else {
            return 0;
           }
          } else if((instr & (1u << 18))) {
           if((instr & (1u << 17))) {
            if((instr & (1u << 16))) {
             if((instr & 0xf000u) == 0xf000u) {
              if((instr & 0xf00u) == 0xf00u) {
               return 0;
              } else if((instr & 0xf8u) == 0x80u) {
               return 2;
              } else {
               return 0;
              }
             } else {
              return 0;
             }
            } else if((instr & 0xf000u) == 0xf000u) {
             if((instr & 0xf00u) == 0xf00u) {
              return 0;
             } else if((instr & 0xf0u) == 0x80u) {
              return test_thumb_instr_1321(instr);
             } else {
              return 0;
             }
            } else {
             return 0;
            }
           } else if((instr & (1u << 16))) {
            if((instr & 0xf000u) == 0xf000u) {
             if((instr & 0xf00u) == 0xf00u) {
              return 0;
             } else if((instr & 0xf0u) == 0x80u) {
              return test_thumb_instr_1309(instr);
             } else {
              return 0;
             }
            } else {
             return 0;
            }
           } else if((instr & 0xf000u) == 0xf000u) {
            if((instr & 0xf00u) == 0xf00u) {
             return 0;
            } else if((instr & 0xf0u) == 0x80u) {
             return test_thumb_instr_1298(instr);
            } else {
             return 0;
            }
           } else {
            return 0;
           }
          } else if((instr & (1u << 17))) {
           if((instr & (1u << 16))) {
            if((instr & 0xf000u) == 0xf000u) {
             if((instr & 0xf00u) == 0xf00u) {
              return 0;
             } else if((instr & 0xf0u) == 0x80u) {
              return test_thumb_instr_1285(instr);
             } else {
              return 0;
             }
            } else {
             return 0;
            }
           } else if((instr & 0xf000u) == 0xf000u) {
            if((instr & 0xf00u) == 0xf00u) {
             return 0;
            } else if((instr & 0xf0u) == 0x80u) {
             return test_thumb_instr_1273(instr);
            } else {
             return 0;
            }
           } else {
            return 0;
           }
          } else if((instr & (1u << 16))) {
           if((instr & 0xf000u) == 0xf000u) {
            if((instr & 0xf00u) == 0xf00u) {
             return 0;
            } else if((instr & 0xf0u) == 0x80u) {
             return test_thumb_instr_1260(instr);
            } else {
             return 0;
            }
           } else {
            return 0;
           }
          } else if((instr & 0xf000u) == 0xf000u) {
           if((instr & 0xf00u) == 0xf00u) {
            return 0;
           } else if((instr & 0xf0u) == 0x80u) {
            return test_thumb_instr_166(instr);
           } else {
            return 0;
           }
          } else {
           return 0;
          }
         } else if((instr & 0xf0000u) == 0xf0000u) {
          return 0;
         } else if((instr & 0xf000u) == 0xf000u) {
          if((instr & 0xf00u) == 0xf00u) {
           return 0;
          } else if((instr & (1u << 7))) {
           return test_thumb_instr_1206(instr);
          } else {
           return test_thumb_instr_1244(instr);
          }
         } else {
          return 0;
         }
        } else if((instr & (1u << 20))) {
         if((instr & (1u << 19))) {
          if((instr & (1u << 18))) {
           if((instr & (1u << 17))) {
            if((instr & 0x1f000u) == 0xf000u) {
             if((instr & 0xf00u) == 0xf00u) {
              return 0;
             } else if((instr & (1u << 7))) {
              if((instr & 0x41u) == 0x0u) {
               return 2;
              } else {
               return 0;
              }
             } else {
              return test_thumb_instr_1244(instr);
             }
            } else {
             return 0;
            }
           } else if((instr & (1u << 16))) {
            if((instr & 0xf000u) == 0xf000u) {
             if((instr & 0xf00u) == 0xf00u) {
              return 0;
             } else if((instr & (1u << 7))) {
              if((instr & 0x42u) == 0x0u) {
               return 2;
              } else {
               return 0;
              }
             } else {
              return test_thumb_instr_1244(instr);
             }
            } else {
             return 0;
            }
           } else if((instr & 0xf000u) == 0xf000u) {
            if((instr & 0xf00u) == 0xf00u) {
             return 0;
            } else if((instr & (1u << 7))) {
             if((instr & (1u << 6))) {
              return 0;
             } else {
              return test_thumb_instr_164(instr);
             }
            } else {
             return test_thumb_instr_1244(instr);
            }
           } else {
            return 0;
           }
          } else if((instr & (1u << 17))) {
           if((instr & (1u << 16))) {
            if((instr & 0xf000u) == 0xf000u) {
             if((instr & 0xf00u) == 0xf00u) {
              return 0;
             } else if((instr & (1u << 7))) {
              if((instr & 0x44u) == 0x0u) {
               return 2;
              } else {
               return 0;
              }
             } else {
              return test_thumb_instr_1244(instr);
             }
            } else {
             return 0;
            }
           } else if((instr & 0xf000u) == 0xf000u) {
            if((instr & 0xf00u) == 0xf00u) {
             return 0;
            } else if((instr & (1u << 7))) {
             if((instr & (1u << 6))) {
              return 0;
             } else {
              return test_thumb_instr_1272(instr);
             }
            } else {
             return test_thumb_instr_1244(instr);
            }
           } else {
            return 0;
           }
          } else if((instr & (1u << 16))) {
           if((instr & 0xf000u) == 0xf000u) {
            if((instr & 0xf00u) == 0xf00u) {
             return 0;
            } else if((instr & (1u << 7))) {
             if((instr & (1u << 6))) {
              return 0;
             } else {
              return test_thumb_instr_1259(instr);
             }
            } else {
             return test_thumb_instr_1244(instr);
            }
           } else {
            return 0;
           }
          } else if((instr & 0xf000u) == 0xf000u) {
           if((instr & 0xf00u) == 0xf00u) {
            return 0;
           } else if((instr & (1u << 7))) {
            if((instr & (1u << 6))) {
             return 0;
            } else {
             return test_thumb_instr_165(instr);
            }
           } else {
            return test_thumb_instr_1244(instr);
           }
          } else {
           return 0;
          }
         } else if((instr & (1u << 18))) {
          if((instr & (1u << 17))) {
           if((instr & (1u << 16))) {
            if((instr & 0xf000u) == 0xf000u) {
             if((instr & 0xf00u) == 0xf00u) {
              return 0;
             } else if((instr & (1u << 7))) {
              if((instr & 0x48u) == 0x0u) {
               return 2;
              } else {
               return 0;
              }
             } else {
              return test_thumb_instr_1244(instr);
             }
            } else {
             return 0;
            }
           } else if((instr & 0xf000u) == 0xf000u) {
            if((instr & 0xf00u) == 0xf00u) {
             return 0;
            } else if((instr & (1u << 7))) {
             if((instr & (1u << 6))) {
              return 0;
             } else {
              return test_thumb_instr_1321(instr);
             }
            } else {
             return test_thumb_instr_1244(instr);
            }
           } else {
            return 0;
           }
          } else if((instr & (1u << 16))) {
           if((instr & 0xf000u) == 0xf000u) {
            if((instr & 0xf00u) == 0xf00u) {
             return 0;
            } else if((instr & (1u << 7))) {
             if((instr & (1u << 6))) {
              return 0;
             } else {
              return test_thumb_instr_1309(instr);
             }
            } else {
             return test_thumb_instr_1244(instr);
            }
           } else {
            return 0;
           }
          } else if((instr & 0xf000u) == 0xf000u) {
           if((instr & 0xf00u) == 0xf00u) {
            return 0;
           } else if((instr & (1u << 7))) {
            if((instr & (1u << 6))) {
             return 0;
            } else {
             return test_thumb_instr_1298(instr);
            }
           } else {
            return test_thumb_instr_1244(instr);
           }
          } else {
           return 0;
          }
         } else if((instr & (1u << 17))) {
          if((instr & (1u << 16))) {
           if((instr & 0xf000u) == 0xf000u) {
            if((instr & 0xf00u) == 0xf00u) {
             return 0;
            } else if((instr & (1u << 7))) {
             if((instr & (1u << 6))) {
              return 0;
             } else {
              return test_thumb_instr_1285(instr);
             }
            } else {
             return test_thumb_instr_1244(instr);
            }
           } else {
            return 0;
           }
          } else if((instr & 0xf000u) == 0xf000u) {
           if((instr & 0xf00u) == 0xf00u) {
            return 0;
           } else if((instr & (1u << 7))) {
            if((instr & (1u << 6))) {
             return 0;
            } else {
             return test_thumb_instr_1273(instr);
            }
           } else {
            return test_thumb_instr_1244(instr);
           }
          } else {
           return 0;
          }
         } else if((instr & (1u << 16))) {
          if((instr & 0xf000u) == 0xf000u) {
           if((instr & 0xf00u) == 0xf00u) {
            return 0;
           } else if((instr & (1u << 7))) {
            if((instr & (1u << 6))) {
             return 0;
            } else {
             return test_thumb_instr_1260(instr);
            }
           } else {
            return test_thumb_instr_1244(instr);
           }
          } else {
           return 0;
          }
         } else {
          return test_thumb_instr_1253(instr);
         }
        } else if((instr & 0xf0000u) == 0xf0000u) {
         return 0;
        } else {
         return test_thumb_instr_1253(instr);
        }
       } else if((instr & 0x600000u) == 0x600000u) {
        if((instr & 0xf0000u) == 0xf0000u) {
         return 0;
        } else if((instr & 0xf000u) == 0xf000u) {
         return test_thumb_instr_1233(instr);
        } else {
         return 0;
        }
       } else if((instr & 0xf0000u) == 0xf0000u) {
        if((instr & 0xf000u) == 0xf000u) {
         if((instr & 0xf00u) == 0xf00u) {
          return 0;
         } else if((instr & (1u << 7))) {
          return test_thumb_instr_166(instr);
         } else {
          return 0;
         }
        } else {
         return 0;
        }
       } else if((instr & 0xf000u) == 0xf000u) {
        if((instr & 0xf00u) == 0xf00u) {
         return 0;
        } else if((instr & (1u << 7))) {
         return test_thumb_instr_166(instr);
        } else {
         return test_thumb_instr_1206(instr);
        }
       } else {
        return 0;
       }
      } else if((instr & (1u << 24))) {
       if((instr & (1u << 23))) {
        if((instr & (1u << 22))) {
         if((instr & (1u << 21))) {
          if((instr & (1u << 20))) {
           return 0;
          } else if((instr & 0xf0000u) == 0xf0000u) {
           return 0;
          } else if((instr & (1u << 15))) {
           if((instr & (1u << 14))) {
            if((instr & (1u << 13))) {
             if((instr & (1u << 12))) {
              if((instr & (1u << 11))) {
               if((instr & (1u << 10))) {
                if((instr & (1u << 9))) {
                 return test_thumb_instr_1117(instr);
                } else if((instr & (1u << 8))) {
                 return 0;
                } else if((instr & (1u << 7))) {
                 return test_thumb_instr_67(instr);
                } else if((instr & (1u << 6))) {
                 return test_thumb_instr_66(instr);
                } else {
                 return test_thumb_instr_391(instr);
                }
               } else if((instr & 0x340u) == 0x0u) {
                return test_thumb_instr_1100(instr);
               } else {
                return 0;
               }
              } else {
               return test_thumb_instr_1161(instr);
              }
             } else if((instr & (1u << 11))) {
              if((instr & (1u << 10))) {
               if((instr & (1u << 9))) {
                return test_thumb_instr_1117(instr);
               } else if((instr & (1u << 8))) {
                if((instr & 0xc0u) == 0xc0u) {
                 return 0;
                } else {
                 return test_thumb_instr_66(instr);
                }
               } else {
                return test_thumb_instr_1113(instr);
               }
              } else if((instr & (1u << 9))) {
               return 0;
              } else {
               return test_thumb_instr_1155(instr);
              }
             } else {
              return test_thumb_instr_1154(instr);
             }
            } else if((instr & (1u << 12))) {
             if((instr & (1u << 11))) {
              if((instr & (1u << 10))) {
               return test_thumb_instr_1118(instr);
              } else {
               return test_thumb_instr_1148(instr);
              }
             } else {
              return test_thumb_instr_1146(instr);
             }
            } else if((instr & (1u << 11))) {
             if((instr & (1u << 10))) {
              return test_thumb_instr_1118(instr);
             } else {
              return test_thumb_instr_1139(instr);
             }
            } else {
             return test_thumb_instr_1137(instr);
            }
           } else if((instr & (1u << 13))) {
            if((instr & (1u << 11))) {
             if((instr & (1u << 10))) {
              return test_thumb_instr_1118(instr);
             } else {
              return test_thumb_instr_1131(instr);
             }
            } else {
             return test_thumb_instr_1129(instr);
            }
           } else {
            return test_thumb_instr_1120(instr);
           }
          } else {
           return test_thumb_instr_1120(instr);
          }
         } else if((instr & (1u << 20))) {
          return 0;
         } else if((instr & 0xf0000u) == 0xf0000u) {
          return 0;
         } else if((instr & (1u << 15))) {
          if((instr & (1u << 14))) {
           if((instr & (1u << 13))) {
            if((instr & (1u << 12))) {
             if((instr & (1u << 11))) {
              if((instr & 0x740u) == 0x0u) {
               return test_thumb_instr_1100(instr);
              } else {
               return 0;
              }
             } else {
              return test_thumb_instr_1161(instr);
             }
            } else if((instr & (1u << 11))) {
             if((instr & 0x600u) == 0x0u) {
              return test_thumb_instr_1155(instr);
             } else {
              return 0;
             }
            } else {
             return test_thumb_instr_1154(instr);
            }
           } else if((instr & (1u << 12))) {
            if((instr & (1u << 11))) {
             if((instr & (1u << 10))) {
              return 0;
             } else {
              return test_thumb_instr_1148(instr);
             }
            } else {
             return test_thumb_instr_1146(instr);
            }
           } else if((instr & (1u << 11))) {
            if((instr & (1u << 10))) {
             return 0;
            } else {
             return test_thumb_instr_1139(instr);
            }
           } else {
            return test_thumb_instr_1137(instr);
           }
          } else if((instr & (1u << 13))) {
           if((instr & (1u << 11))) {
            if((instr & (1u << 10))) {
             return 0;
            } else {
             return test_thumb_instr_1131(instr);
            }
           } else {
            return test_thumb_instr_1129(instr);
           }
          } else {
           return test_thumb_instr_1106(instr);
          }
         } else {
          return test_thumb_instr_1106(instr);
         }
        } else if((instr & (1u << 21))) {
         if((instr & (1u << 20))) {
          return test_thumb_instr_1057(instr);
         } else if((instr & 0xf0000u) == 0xf0000u) {
          return 0;
         } else {
          return test_thumb_instr_1120(instr);
         }
        } else if((instr & (1u << 20))) {
         return test_thumb_instr_289(instr);
        } else if((instr & 0xf0000u) == 0xf0000u) {
         return 0;
        } else {
         return test_thumb_instr_1106(instr);
        }
       } else if((instr & (1u << 22))) {
        if((instr & (1u << 20))) {
         return 0;
        } else if((instr & 0xf0000u) == 0xf0000u) {
         return 0;
        } else if((instr & 0xe000u) == 0xe000u) {
         if((instr & (1u << 12))) {
          if((instr & (1u << 11))) {
           return 0;
          } else {
           return test_thumb_instr_1067(instr);
          }
         } else if((instr & (1u << 11))) {
          if((instr & (1u << 10))) {
           return 0;
          } else if((instr & (1u << 9))) {
           return test_thumb_instr_1070(instr);
          } else if((instr & (1u << 8))) {
           return 0;
          } else {
           return test_thumb_instr_1069(instr);
          }
         } else {
          return test_thumb_instr_1067(instr);
         }
        } else {
         return test_thumb_instr_1073(instr);
        }
       } else if((instr & (1u << 21))) {
        if((instr & (1u << 20))) {
         return test_thumb_instr_1036(instr);
        } else {
         return test_thumb_instr_1077(instr);
        }
       } else if((instr & (1u << 20))) {
        return test_thumb_instr_1047(instr);
       } else {
        return test_thumb_instr_1077(instr);
       }
      } else if((instr & (1u << 23))) {
       if((instr & (1u << 22))) {
        if((instr & (1u << 21))) {
         return 0;
        } else {
         return test_thumb_instr_1057(instr);
        }
       } else if((instr & (1u << 20))) {
        return test_thumb_instr_289(instr);
       } else {
        return test_thumb_instr_1057(instr);
       }
      } else if((instr & (1u << 22))) {
       if((instr & (1u << 21))) {
        return 0;
       } else {
        return test_thumb_instr_1036(instr);
       }
      } else if((instr & (1u << 20))) {
       return test_thumb_instr_1047(instr);
      } else {
       return test_thumb_instr_1036(instr);
      }
     } else if((instr & (1u << 26))) {
      if((instr & (1u << 25))) {
       if((instr & (1u << 24))) {
        if((instr & (1u << 23))) {
         if((instr & (1u << 22))) {
          if((instr & (1u << 21))) {
           if((instr & (1u << 20))) {
            if((instr & 0xd000u) == 0x8000u) {
             return 2;
            } else {
             return 0;
            }
           } else if((instr & 0xf000u) == 0x8000u) {
            return 2;
           } else {
            return 0;
           }
          } else {
           return test_thumb_instr_822(instr);
          }
         } else if((instr & (1u << 21))) {
          if((instr & (1u << 20))) {
           return 0;
          } else if((instr & 0xf0000u) == 0xf0000u) {
           return 0;
          } else if((instr & (1u << 15))) {
           return 0;
          } else if((instr & 0x7000u) == 0x0u) {
           if((instr & 0xf00u) == 0xf00u) {
            return 0;
           } else if((instr & 0xc0u) == 0x0u) {
            return 0;
           } else {
            return 2;
           }
          } else {
           return test_thumb_instr_105(instr);
          }
         } else {
          return test_thumb_instr_822(instr);
         }
        } else if((instr & (1u << 22))) {
         return test_thumb_instr_823(instr);
        } else {
         return test_thumb_instr_855(instr);
        }
       } else {
        return test_thumb_instr_829(instr);
       }
      } else {
       return test_thumb_instr_821(instr);
      }
     } else if((instr & (1u << 25))) {
      if((instr & (1u << 24))) {
       if((instr & (1u << 23))) {
        if((instr & (1u << 22))) {
         if((instr & (1u << 21))) {
          if((instr & 0xf0000u) == 0xf0000u) {
           if((instr & 0xf000u) == 0x8000u) {
            if((instr & 0xf00u) == 0xf00u) {
             return 0;
            } else {
             return test_thumb_instr_864(instr);
            }
           } else {
            return 0;
           }
          } else if((instr & 0xf000u) == 0x8000u) {
           if((instr & 0xf00u) == 0xf00u) {
            return 0;
           } else {
            return test_thumb_instr_861(instr);
           }
          } else {
           return 0;
          }
         } else if((instr & (1u << 20))) {
          if((instr & 0xfffffu) == 0xe8f00u) {
           return 2;
          } else {
           return 0;
          }
         } else if((instr & 0xf0000u) == 0xf0000u) {
          return 0;
         } else if((instr & (1u << 15))) {
          if((instr & 0x7fffu) == 0xf00u) {
           return 2;
          } else {
           return 0;
          }
         } else {
          return test_thumb_instr_105(instr);
         }
        } else if((instr & (1u << 21))) {
         if((instr & (1u << 20))) {
          if((instr & 0xfff80u) == 0xf8f00u) {
           if((instr & (1u << 6))) {
            return test_thumb_instr_932(instr);
           } else if((instr & 0x3fu) == 0x2fu) {
            return 2;
           } else {
            return 0;
           }
          } else {
           return 0;
          }
         } else if((instr & 0xf0000u) == 0xf0000u) {
          if((instr & 0xf800u) == 0x8000u) {
           if((instr & (1u << 10))) {
            if((instr & (1u << 8))) {
             return 2;
            } else {
             return test_thumb_instr_85(instr);
            }
           } else if((instr & (1u << 9))) {
            return 0;
           } else {
            return 2;
           }
          } else {
           return 0;
          }
         } else if((instr & (1u << 15))) {
          return 0;
         } else if((instr & 0x7000u) == 0x0u) {
          if((instr & 0xf00u) == 0xf00u) {
           return 0;
          } else if((instr & 0xc0u) == 0x0u) {
           return test_thumb_instr_391(instr);
          } else {
           return 2;
          }
         } else {
          return test_thumb_instr_105(instr);
         }
        } else if((instr & (1u << 20))) {
         if((instr & 0xf0000u) == 0xf0000u) {
          if((instr & 0xf000u) == 0x8000u) {
           if((instr & (1u << 11))) {
            if((instr & (1u << 10))) {
             if((instr & (1u << 9))) {
              if((instr & 0x1efu) == 0x120u) {
               return 2;
              } else {
               return 0;
              }
             } else {
              return test_thumb_instr_895(instr);
             }
            } else {
             return test_thumb_instr_861(instr);
            }
           } else {
            return test_thumb_instr_895(instr);
           }
          } else {
           return 0;
          }
         } else if((instr & 0xf0c0u) == 0x8000u) {
          return test_thumb_instr_862(instr);
         } else {
          return 0;
         }
        } else if((instr & 0xf0000u) == 0xf0000u) {
         if((instr & 0xf000u) == 0x8000u) {
          if((instr & (1u << 11))) {
           if((instr & (1u << 10))) {
            if((instr & 0x3ffu) == 0x320u) {
             return 2;
            } else {
             return 0;
            }
           } else if((instr & 0xffu) == 0x30u) {
            return 2;
           } else {
            return 0;
           }
          } else if((instr & 0x7ffu) == 0x720u) {
           return 2;
          } else {
           return 0;
          }
         } else {
          return 0;
         }
        } else if((instr & (1u << 15))) {
         if((instr & 0x7000u) == 0x0u) {
          if((instr & 0xf00u) == 0x0u) {
           return test_thumb_instr_861(instr);
          } else {
           return test_thumb_instr_864(instr);
          }
         } else {
          return 0;
         }
        } else {
         return test_thumb_instr_105(instr);
        }
       } else if((instr & (1u << 22))) {
        if((instr & (1u << 21))) {
         if((instr & 0x108000u) == 0x0u) {
          return test_thumb_instr_833(instr);
         } else {
          return 0;
         }
        } else {
         return test_thumb_instr_822(instr);
        }
       } else {
        return test_thumb_instr_855(instr);
       }
      } else {
       return test_thumb_instr_829(instr);
      }
     } else {
      return test_thumb_instr_821(instr);
     }
    } else if((instr & (1u << 27))) {
     if((instr & (1u << 26))) {
      if((instr & (1u << 25))) {
       if((instr & (1u << 24))) {
        if((instr & (1u << 23))) {
         if((instr & (1u << 21))) {
          if((instr & (1u << 20))) {
           if((instr & (1u << 16))) {
            if((instr & (1u << 12))) {
             return test_thumb_instr_782(instr);
            } else if((instr & (1u << 11))) {
             if((instr & (1u << 10))) {
              return test_thumb_instr_788(instr);
             } else if((instr & (1u << 9))) {
              if((instr & 0x1d0u) == 0x10u) {
               return 2;
              } else {
               return 0;
              }
             } else {
              return test_thumb_instr_673(instr);
             }
            } else if((instr & 0x500u) == 0x400u) {
             return test_thumb_instr_355(instr);
            } else {
             return test_thumb_instr_630(instr);
            }
           } else if((instr & (1u << 12))) {
            return test_thumb_instr_782(instr);
           } else if((instr & (1u << 11))) {
            if((instr & (1u << 10))) {
             if((instr & 0x280u) == 0x200u) {
              return test_thumb_instr_774(instr);
             } else {
              return test_thumb_instr_768(instr);
             }
            } else if((instr & (1u << 9))) {
             if((instr & 0x180u) == 0x0u) {
              if((instr & (1u << 6))) {
               return test_thumb_instr_575(instr);
              } else {
               return test_thumb_instr_266(instr);
              }
             } else {
              return test_thumb_instr_768(instr);
             }
            } else if((instr & (1u << 7))) {
             return test_thumb_instr_768(instr);
            } else if((instr & 0x50u) == 0x0u) {
             return 0;
            } else {
             return test_thumb_instr_81(instr);
            }
           } else if((instr & 0x500u) == 0x400u) {
            return test_thumb_instr_576(instr);
           } else {
            return test_thumb_instr_568(instr);
           }
          } else if((instr & (1u << 16))) {
           if((instr & (1u << 12))) {
            if((instr & (1u << 11))) {
             return test_thumb_instr_755(instr);
            } else if((instr & (1u << 10))) {
             if((instr & (1u << 9))) {
              return test_thumb_instr_690(instr);
             } else if((instr & (1u << 8))) {
              return test_thumb_instr_713(instr);
             } else {
              return test_thumb_instr_393(instr);
             }
            } else {
             return test_thumb_instr_749(instr);
            }
           } else if((instr & (1u << 11))) {
            return test_thumb_instr_747(instr);
           } else if((instr & (1u << 10))) {
            return test_thumb_instr_732(instr);
           } else {
            return test_thumb_instr_729(instr);
           }
          } else if((instr & (1u << 12))) {
           if((instr & (1u << 11))) {
            return test_thumb_instr_755(instr);
           } else if((instr & (1u << 10))) {
            if((instr & (1u << 9))) {
             return test_thumb_instr_680(instr);
            } else if((instr & (1u << 8))) {
             return test_thumb_instr_713(instr);
            } else {
             return test_thumb_instr_699(instr);
            }
           } else {
            return test_thumb_instr_749(instr);
           }
          } else if((instr & (1u << 11))) {
           return test_thumb_instr_747(instr);
          } else if((instr & 0x500u) == 0x400u) {
           return test_thumb_instr_699(instr);
          } else {
           return test_thumb_instr_696(instr);
          }
         } else if((instr & (1u << 20))) {
          if((instr & (1u << 16))) {
           if((instr & (1u << 12))) {
            if((instr & (1u << 11))) {
             return test_thumb_instr_724(instr);
            } else if((instr & (1u << 10))) {
             if((instr & (1u << 9))) {
              return test_thumb_instr_690(instr);
             } else if((instr & (1u << 8))) {
              return test_thumb_instr_270(instr);
             } else {
              return test_thumb_instr_393(instr);
             }
            } else {
             return test_thumb_instr_715(instr);
            }
           } else if((instr & (1u << 11))) {
            return test_thumb_instr_711(instr);
           } else if((instr & (1u << 10))) {
            if((instr & (1u << 9))) {
             return test_thumb_instr_732(instr);
            } else if((instr & (1u << 8))) {
             return test_thumb_instr_630(instr);
            } else {
             return test_thumb_instr_393(instr);
            }
           } else if((instr & (1u << 9))) {
            return test_thumb_instr_729(instr);
           } else {
            return test_thumb_instr_727(instr);
           }
          } else if((instr & (1u << 12))) {
           if((instr & (1u << 11))) {
            return test_thumb_instr_724(instr);
           } else if((instr & (1u << 10))) {
            if((instr & (1u << 9))) {
             return test_thumb_instr_680(instr);
            } else if((instr & (1u << 8))) {
             return test_thumb_instr_270(instr);
            } else {
             return test_thumb_instr_699(instr);
            }
           } else {
            return test_thumb_instr_715(instr);
           }
          } else if((instr & (1u << 11))) {
           return test_thumb_instr_711(instr);
          } else if((instr & (1u << 10))) {
           if((instr & (1u << 9))) {
            if((instr & (1u << 8))) {
             return test_thumb_instr_696(instr);
            } else {
             return test_thumb_instr_699(instr);
            }
           } else if((instr & (1u << 8))) {
            return test_thumb_instr_630(instr);
           } else {
            return test_thumb_instr_699(instr);
           }
          } else if((instr & 0x300u) == 0x100u) {
           return test_thumb_instr_630(instr);
          } else {
           return test_thumb_instr_696(instr);
          }
         } else if((instr & (1u << 19))) {
          if((instr & (1u << 16))) {
           if((instr & (1u << 12))) {
            if((instr & (1u << 11))) {
             return test_thumb_instr_683(instr);
            } else if((instr & 0x500u) == 0x400u) {
             return 0;
            } else {
             return test_thumb_instr_270(instr);
            }
           } else if((instr & (1u << 11))) {
            return test_thumb_instr_678(instr);
           } else if((instr & (1u << 10))) {
            return test_thumb_instr_687(instr);
           } else {
            return test_thumb_instr_686(instr);
           }
          } else if((instr & (1u << 12))) {
           if((instr & (1u << 11))) {
            return test_thumb_instr_683(instr);
           } else if((instr & 0x500u) == 0x400u) {
            return test_thumb_instr_634(instr);
           } else {
            return test_thumb_instr_270(instr);
           }
          } else if((instr & (1u << 11))) {
           return test_thumb_instr_678(instr);
          } else if((instr & 0x500u) == 0x400u) {
           return test_thumb_instr_634(instr);
          } else {
           return test_thumb_instr_630(instr);
          }
         } else if((instr & (1u << 16))) {
          if((instr & (1u << 12))) {
           if((instr & (1u << 11))) {
            return test_thumb_instr_655(instr);
           } else if((instr & 0x580u) == 0x480u) {
            return 0;
           } else {
            return test_thumb_instr_270(instr);
           }
          } else if((instr & (1u << 11))) {
           return test_thumb_instr_645(instr);
          } else if((instr & (1u << 10))) {
           if((instr & (1u << 8))) {
            return test_thumb_instr_631(instr);
           } else {
            return test_thumb_instr_639(instr);
           }
          } else {
           return test_thumb_instr_659(instr);
          }
         } else if((instr & (1u << 12))) {
          if((instr & (1u << 11))) {
           return test_thumb_instr_655(instr);
          } else if((instr & 0x500u) == 0x400u) {
           if((instr & (1u << 7))) {
            return test_thumb_instr_634(instr);
           } else {
            return test_thumb_instr_647(instr);
           }
          } else {
           return test_thumb_instr_270(instr);
          }
         } else if((instr & (1u << 11))) {
          return test_thumb_instr_645(instr);
         } else if((instr & 0x500u) == 0x400u) {
          if((instr & (1u << 7))) {
           return test_thumb_instr_634(instr);
          } else {
           return test_thumb_instr_633(instr);
          }
         } else {
          return test_thumb_instr_631(instr);
         }
        } else if((instr & (1u << 21))) {
         if((instr & (1u << 20))) {
          return test_thumb_instr_626(instr);
         } else if((instr & 0x11000u) == 0x0u) {
          if((instr & (1u << 11))) {
           if((instr & (1u << 10))) {
            if((instr & (1u << 9))) {
             if((instr & (1u << 8))) {
              return test_thumb_instr_568(instr);
             } else {
              return 0;
             }
            } else {
             return test_thumb_instr_574(instr);
            }
           } else {
            return test_thumb_instr_591(instr);
           }
          } else {
           return test_thumb_instr_568(instr);
          }
         } else if((instr & 0xc00u) == 0xc00u) {
          if((instr & (1u << 9))) {
           return test_thumb_instr_603(instr);
          } else {
           return test_thumb_instr_582(instr);
          }
         } else {
          return test_thumb_instr_569(instr);
         }
        } else if((instr & (1u << 20))) {
         if((instr & 0x11000u) == 0x0u) {
          if((instr & (1u << 11))) {
           if((instr & (1u << 10))) {
            return 0;
           } else {
            return test_thumb_instr_591(instr);
           }
          } else {
           return test_thumb_instr_568(instr);
          }
         } else if((instr & 0xc00u) == 0xc00u) {
          return 0;
         } else {
          return test_thumb_instr_569(instr);
         }
        } else if((instr & 0x11000u) == 0x0u) {
         if((instr & (1u << 11))) {
          if((instr & (1u << 10))) {
           if((instr & (1u << 9))) {
            if((instr & (1u << 8))) {
             return test_thumb_instr_568(instr);
            } else {
             return test_thumb_instr_576(instr);
            }
           } else {
            return test_thumb_instr_574(instr);
           }
          } else if((instr & (1u << 9))) {
           return test_thumb_instr_570(instr);
          } else {
           return test_thumb_instr_568(instr);
          }
         } else {
          return test_thumb_instr_568(instr);
         }
        } else if((instr & (1u << 11))) {
         if((instr & (1u << 10))) {
          if((instr & (1u << 9))) {
           if((instr & (1u << 8))) {
            return test_thumb_instr_569(instr);
           } else {
            return test_thumb_instr_355(instr);
           }
          } else {
           return test_thumb_instr_582(instr);
          }
         } else if((instr & 0x300u) == 0x300u) {
          return test_thumb_instr_270(instr);
         } else {
          return test_thumb_instr_569(instr);
         }
        } else {
         return test_thumb_instr_569(instr);
        }
       } else if((instr & (1u << 23))) {
        if((instr & (1u << 22))) {
         if((instr & (1u << 21))) {
          if((instr & (1u << 20))) {
           if((instr & (1u << 19))) {
            if((instr & (1u << 18))) {
             return test_thumb_instr_528(instr);
            } else if((instr & (1u << 17))) {
             if((instr & (1u << 16))) {
              return test_thumb_instr_528(instr);
             } else {
              return test_thumb_instr_511(instr);
             }
            } else if((instr & (1u << 16))) {
             if((instr & 0xf000u) == 0xf000u) {
              return test_thumb_instr_443(instr);
             } else if((instr & 0xe00u) == 0xa00u) {
              if((instr & (1u << 8))) {
               if((instr & 0xe0u) == 0x0u) {
                return test_thumb_instr_305(instr);
               } else {
                return test_thumb_instr_395(instr);
               }
              } else if((instr & 0xe0u) == 0x0u) {
               return test_thumb_instr_305(instr);
              } else {
               return 0;
              }
             } else {
              return 2;
             }
            } else {
             return test_thumb_instr_511(instr);
            }
           } else if((instr & (1u << 18))) {
            if((instr & (1u << 17))) {
             return test_thumb_instr_511(instr);
            } else if((instr & (1u << 16))) {
             if((instr & 0xf000u) == 0xf000u) {
              return test_thumb_instr_425(instr);
             } else if((instr & 0xe00u) == 0xa00u) {
              if((instr & (1u << 8))) {
               if((instr & (1u << 7))) {
                if((instr & 0x60u) == 0x40u) {
                 return test_thumb_instr_84(instr);
                } else {
                 return test_thumb_instr_395(instr);
                }
               } else if((instr & (1u << 6))) {
                if((instr & 0x30u) == 0x20u) {
                 return 0;
                } else {
                 return test_thumb_instr_84(instr);
                }
               } else if((instr & (1u << 5))) {
                return test_thumb_instr_395(instr);
               } else {
                return test_thumb_instr_305(instr);
               }
              } else if((instr & (1u << 7))) {
               return test_thumb_instr_413(instr);
              } else if((instr & (1u << 6))) {
               return test_thumb_instr_86(instr);
              } else {
               return test_thumb_instr_497(instr);
              }
             } else {
              return 2;
             }
            } else {
             return test_thumb_instr_528(instr);
            }
           } else if((instr & (1u << 17))) {
            return test_thumb_instr_528(instr);
           } else if((instr & (1u << 16))) {
            if((instr & 0xf000u) == 0xf000u) {
             if((instr & 0xe00u) == 0xa00u) {
              if((instr & (1u << 8))) {
               return test_thumb_instr_394(instr);
              } else {
               return test_thumb_instr_499(instr);
              }
             } else {
              return 2;
             }
            } else {
             return test_thumb_instr_507(instr);
            }
           } else {
            return test_thumb_instr_511(instr);
           }
          } else if((instr & (1u << 19))) {
           if((instr & (1u << 18))) {
            return test_thumb_instr_490(instr);
           } else if((instr & (1u << 17))) {
            if((instr & (1u << 16))) {
             return test_thumb_instr_389(instr);
            } else {
             return test_thumb_instr_472(instr);
            }
           } else {
            return test_thumb_instr_481(instr);
           }
          } else if((instr & 0x60000u) == 0x0u) {
           return test_thumb_instr_481(instr);
          } else {
           return test_thumb_instr_490(instr);
          }
         } else if((instr & (1u << 20))) {
          return test_thumb_instr_352(instr);
         } else if((instr & 0xf000u) == 0xf000u) {
          return test_thumb_instr_363(instr);
         } else if((instr & 0xe00u) == 0xa00u) {
          if((instr & (1u << 8))) {
           if((instr & (1u << 6))) {
            return 0;
           } else if((instr & (1u << 5))) {
            return test_thumb_instr_306(instr);
           } else {
            return test_thumb_instr_305(instr);
           }
          } else {
           return test_thumb_instr_355(instr);
          }
         } else {
          return 2;
         }
        } else if((instr & (1u << 21))) {
         if((instr & (1u << 20))) {
          if((instr & (1u << 19))) {
           if((instr & 0x70000u) == 0x10000u) {
            if((instr & 0xf000u) == 0xf000u) {
             return test_thumb_instr_443(instr);
            } else if((instr & 0xe00u) == 0xa00u) {
             if((instr & (1u << 8))) {
              if((instr & 0xc0u) == 0x0u) {
               return test_thumb_instr_396(instr);
              } else {
               return test_thumb_instr_398(instr);
              }
             } else {
              return test_thumb_instr_434(instr);
             }
            } else {
             return 2;
            }
           } else {
            return test_thumb_instr_411(instr);
           }
          } else if((instr & 0x70000u) == 0x50000u) {
           if((instr & 0xf000u) == 0xf000u) {
            return test_thumb_instr_425(instr);
           } else if((instr & 0xe00u) == 0xa00u) {
            if((instr & (1u << 8))) {
             if((instr & (1u << 7))) {
              if((instr & (1u << 6))) {
               return test_thumb_instr_415(instr);
              } else {
               return test_thumb_instr_398(instr);
              }
             } else if((instr & (1u << 6))) {
              return test_thumb_instr_415(instr);
             } else {
              return test_thumb_instr_396(instr);
             }
            } else {
             return test_thumb_instr_414(instr);
            }
           } else {
            return 2;
           }
          } else {
           return test_thumb_instr_411(instr);
          }
         } else if((instr & (1u << 16))) {
          return test_thumb_instr_389(instr);
         } else if((instr & 0xf000u) == 0xf000u) {
          return test_thumb_instr_306(instr);
         } else if((instr & 0xe00u) == 0xa00u) {
          if((instr & 0x140u) == 0x100u) {
           return test_thumb_instr_305(instr);
          } else {
           return test_thumb_instr_306(instr);
          }
         } else {
          return 2;
         }
        } else if((instr & (1u << 20))) {
         if((instr & 0xf000u) == 0xf000u) {
          return test_thumb_instr_321(instr);
         } else if((instr & 0xe00u) == 0xa00u) {
          if((instr & 0x120u) == 0x120u) {
           return test_thumb_instr_305(instr);
          } else {
           return test_thumb_instr_306(instr);
          }
         } else {
          return 2;
         }
        } else if((instr & 0xf000u) == 0xf000u) {
         return test_thumb_instr_363(instr);
        } else if((instr & 0xe00u) == 0xa00u) {
         if((instr & (1u << 8))) {
          if((instr & (1u << 6))) {
           return 0;
          } else {
           return test_thumb_instr_305(instr);
          }
         } else {
          return test_thumb_instr_355(instr);
         }
        } else {
         return 2;
        }
       } else if((instr & (1u << 22))) {
        if((instr & (1u << 20))) {
         return test_thumb_instr_352(instr);
        } else if((instr & 0xf000u) == 0xf000u) {
         return test_thumb_instr_306(instr);
        } else {
         return test_thumb_instr_344(instr);
        }
       } else if((instr & (1u << 21))) {
        if((instr & (1u << 20))) {
         if((instr & 0xf000u) == 0xf000u) {
          return test_thumb_instr_321(instr);
         } else {
          return test_thumb_instr_330(instr);
         }
        } else if((instr & 0xf000u) == 0xf000u) {
         return test_thumb_instr_306(instr);
        } else {
         return test_thumb_instr_330(instr);
        }
       } else if((instr & (1u << 20))) {
        if((instr & 0xf000u) == 0xf000u) {
         return test_thumb_instr_321(instr);
        } else {
         return test_thumb_instr_314(instr);
        }
       } else if((instr & 0xf000u) == 0xf000u) {
        return test_thumb_instr_306(instr);
       } else {
        return test_thumb_instr_314(instr);
       }
      } else if((instr & (1u << 24))) {
       if((instr & (1u << 23))) {
        if((instr & (1u << 21))) {
         return test_thumb_instr_264(instr);
        } else {
         return test_thumb_instr_289(instr);
        }
       } else if((instr & 0x600000u) == 0x600000u) {
        return test_thumb_instr_297(instr);
       } else {
        return test_thumb_instr_289(instr);
       }
      } else if((instr & (1u << 23))) {
       if((instr & (1u << 22))) {
        return test_thumb_instr_297(instr);
       } else {
        return test_thumb_instr_289(instr);
       }
      } else if((instr & (1u << 22))) {
       if((instr & (1u << 21))) {
        return test_thumb_instr_264(instr);
       } else if((instr & 0xf0000u) == 0xf0000u) {
        return 0;
       } else if((instr & 0xf000u) == 0xf000u) {
        return 0;
       } else if((instr & 0xe00u) == 0xa00u) {
        if((instr & (1u << 8))) {
         return test_thumb_instr_271(instr);
        } else if((instr & 0xc0u) == 0x0u) {
         if((instr & (1u << 5))) {
          if((instr & (1u << 4))) {
           return test_thumb_instr_166(instr);
          } else {
           return 0;
          }
         } else {
          return test_thumb_instr_266(instr);
         }
        } else {
         return 0;
        }
       } else {
        return 2;
       }
      } else if((instr & (1u << 21))) {
       return test_thumb_instr_264(instr);
      } else {
       return 0;
      }
     } else if((instr & (1u << 25))) {
      if((instr & (1u << 24))) {
       if((instr & (1u << 23))) {
        if((instr & (1u << 22))) {
         if((instr & (1u << 21))) {
          return 0;
         } else {
          return test_thumb_instr_229(instr);
         }
        } else if((instr & (1u << 21))) {
         return test_thumb_instr_234(instr);
        } else {
         return 0;
        }
       } else if((instr & (1u << 22))) {
        return test_thumb_instr_229(instr);
       } else {
        return test_thumb_instr_237(instr);
       }
      } else if((instr & (1u << 23))) {
       if((instr & (1u << 22))) {
        if((instr & 0x300000u) == 0x0u) {
         if((instr & 0xf0000u) == 0xf0000u) {
          return 0;
         } else if((instr & 0xf00u) == 0xf00u) {
          return 0;
         } else if((instr & (1u << 4))) {
          return 0;
         } else {
          return test_thumb_instr_166(instr);
         }
        } else {
         return 0;
        }
       } else {
        return test_thumb_instr_237(instr);
       }
      } else if((instr & (1u << 22))) {
       return test_thumb_instr_225(instr);
      } else if((instr & 0x300000u) == 0x100000u) {
       return test_thumb_instr_233(instr);
      } else {
       return test_thumb_instr_229(instr);
      }
     } else if((instr & (1u << 24))) {
      if((instr & (1u << 23))) {
       if((instr & (1u << 22))) {
        return test_thumb_instr_113(instr);
       } else {
        return test_thumb_instr_101(instr);
       }
      } else if((instr & (1u << 22))) {
       return test_thumb_instr_113(instr);
      } else {
       return test_thumb_instr_157(instr);
      }
     } else if((instr & (1u << 23))) {
      if((instr & (1u << 22))) {
       if((instr & (1u << 21))) {
        return test_thumb_instr_113(instr);
       } else if((instr & (1u << 20))) {
        if((instr & 0xf0000u) == 0xf0000u) {
         return 0;
        } else if((instr & 0xf000u) == 0xf000u) {
         if((instr & 0xfe0u) == 0x0u) {
          return test_thumb_instr_166(instr);
         } else {
          return 0;
         }
        } else if((instr & 0xf00u) == 0xf00u) {
         if((instr & 0xefu) == 0x4fu) {
          return 2;
         } else {
          return 0;
         }
        } else if((instr & 0xffu) == 0x7fu) {
         return 2;
        } else {
         return 0;
        }
       } else if((instr & 0xf0000u) == 0xf0000u) {
        return 0;
       } else if((instr & 0xf000u) == 0xf000u) {
        return 0;
       } else if((instr & 0xf00u) == 0xf00u) {
        if((instr & 0xe0u) == 0x40u) {
         return test_thumb_instr_166(instr);
        } else {
         return 0;
        }
       } else if((instr & 0xf0u) == 0x70u) {
        return test_thumb_instr_166(instr);
       } else {
        return 0;
       }
      } else if((instr & (1u << 21))) {
       if((instr & (1u << 20))) {
        if((instr & 0xc0000u) == 0xc0000u) {
         if((instr & (1u << 17))) {
          if((instr & 0x18000u) == 0x0u) {
           return test_thumb_instr_151(instr);
          } else {
           return 0;
          }
         } else if((instr & (1u << 16))) {
          return test_thumb_instr_145(instr);
         } else {
          return test_thumb_instr_152(instr);
         }
        } else {
         return test_thumb_instr_152(instr);
        }
       } else {
        return test_thumb_instr_149(instr);
       }
      } else {
       return test_thumb_instr_157(instr);
      }
     } else if((instr & (1u << 22))) {
      if((instr & 0x300000u) == 0x100000u) {
       if((instr & 0xf0000u) == 0xf0000u) {
        return 0;
       } else if((instr & 0xf000u) == 0xf000u) {
        return 0;
       } else if((instr & 0xf00u) == 0xf00u) {
        return 2;
       } else {
        return 0;
       }
      } else {
       return test_thumb_instr_113(instr);
      }
     } else {
      return test_thumb_instr_101(instr);
     }
    } else {
     return 0;
    }
   } else if((instr & (1u << 28))) {
    return !!((instr & 0xe000000u) == 0xe000000u);
   } else {
    return !((instr & 0xff0000u) == 0x0u);
   }
  } else if((instr & 0x30000000u) == 0x30000000u) {
   if((instr & (1u << 27))) {
    if((instr & (1u << 26))) {
     if((instr & (1u << 25))) {
      if((instr & 0x10f0000u) == 0x1080000u) {
       return 3;
      } else {
       return 1;
      }
     } else if((instr & (1u << 24))) {
      return 0;
     } else {
      return !((instr & 0xff0000u) == 0x0u);
     }
    } else if((instr & 0x3000000u) == 0x2000000u) {
     return !((instr & 0xc00000u) == 0x800000u);
    } else {
     return 0;
    }
   } else if((instr & (1u << 26))) {
    if((instr & (1u << 25))) {
     if((instr & 0x1c00000u) == 0x400000u) {
      if((instr & (1u << 21))) {
       return !((instr & (1u << 19)));
      } else {
       return !!((instr & 0x170000u) == 0x100000u);
      }
     } else {
      return 0;
     }
    } else {
     return !((instr & 0x1ff0000u) == 0x0u);
    }
   } else {
    return !((instr & (1u << 24)));
   }
  } else {
   return 1;
  }
 } else if((instr & 0x70000000u) == 0x40000000u) {
  if((instr & (1u << 27))) {
   return 0;
  } else if((instr & (1u << 26))) {
   if((instr & 0x3000000u) == 0x3000000u) {
    if((instr & (1u << 23))) {
     if((instr & 0x780000u) == 0x780000u) {
      return 0;
     } else {
      return !!((instr & 0x70000u) == 0x0u);
     }
    } else {
     return !((instr & 0x780000u) == 0x780000u);
    }
   } else if((instr & (1u << 23))) {
    if((instr & 0x780000u) == 0x780000u) {
     return 0;
    } else {
     return !((instr & 0x70000u) == 0x70000u);
    }
   } else {
    return !((instr & 0x780000u) == 0x780000u);
   }
  } else {
   return 1;
  }
 } else {
  return 1;
 }
}
