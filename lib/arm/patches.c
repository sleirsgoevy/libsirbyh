#include <stdint.h>
#include <sys/mman.h>
#include <unistd.h>
#include "../jit.h"
#include "../tls_emu.h"
#include "../patches.h"
#include "../die.h"
#include "../mmap_hook.h"
#include "instr_test_arm.h"
#include "instr_test_thumb.h"

static const uint32_t mov_r0_tpidruro = 0xee1d0f70;
static const uint32_t mov_r7_1 = 0xe3a07001;
static const uint32_t mov_r7_192 = 0xe3a070c0;
static const uint32_t mov_r7_256 = 0xe3a07c01;
static const uint32_t syscall_instr = 0xef000000;
static const uint32_t ldr_r0_r0_imm = 0xe5900000;
static const uint32_t ldr_r0_r0_minus_imm = 0xe5100000;
static const uint32_t ldr_pc_pc_minus_4 = 0xe51ff004;
static const uint32_t cmp_r0_0 = 0xe3500000;
static const uint32_t b_pc_plus_imm = 0xea000000;
static const uint32_t bne_pc_plus_imm = 0x1a000000;
static const uint32_t mov_reg_lr = 0xe1a0000e;
static const uint32_t mov_lr_reg = 0xe1a0e000;
static const uint32_t add_lr_pc_4 = 0xe28fe004;
static const uint16_t thumb_nop = 0x46c0;
static const uint16_t thumb_ldr_reg_pc = 0x4800;
static const uint16_t thumb_ldr_reg_reg = 0x6800;
static const uint16_t thumb_bx_reg = 0x4700;
static const uint16_t thumb_bx_pc = 0x4778;
static const uint16_t thumb_b = 0xe000;
static const uint16_t thumb_cbz = 0xb100;
static const uint16_t thumb_ldr_pc_pc[2] = {0xf8df, 0xf000};

//note: this code has a commented-out fallback path for detours involving a scratch register
//i kinda hope no hardware before ARMv6T2 has been released with android...

extern uint32_t set_tid_address_syscall[2];
extern uint32_t emu_set_tid_address[7];
extern uint32_t emu_mmap2[7];

static uint32_t get_ldr_r7_pc(uint32_t* instr)
{
    if((instr[0] & -4096) == 0xe59f7000)
    {
        uintptr_t pc = (uintptr_t)instr + 8;
        pc += instr[0] & 4095;
        return *(uint32_t*)pc;
    }
    return -1;
}

extern char asm_ensure_emutls_exists[];

static void generate_load_emutls(uint32_t* start, int reg)
{
    uintptr_t* access = get_emutls_access_details();
    *start++ = mov_r0_tpidruro | (reg << 12);
    *start++ = ldr_r0_r0_minus_imm | (reg << 16) | (reg << 12) | 4;
    *start++ = ldr_r0_r0_imm | (reg << 16) | (reg << 12) | (access[0] << 2);
    *start++ = ldr_r0_r0_imm | (reg << 16) | (reg << 12) | access[1];
    *start++ = cmp_r0_0 | (reg << 16);
    *start++ = bne_pc_plus_imm | 5;
    *start++ = mov_reg_lr | (reg << 12);
    *start++ = add_lr_pc_4;
    *start++ = ldr_pc_pc_minus_4;
    *start++ = (uint32_t)asm_ensure_emutls_exists;
    *start++ = mov_lr_reg | reg;
    *start++ = b_pc_plus_imm | (-13 & 0xffffff);
}

static void patch_arm(uint32_t* start, uint32_t* end)
{
    while(start + 1 < end)
    {
        if(start[1] == syscall_instr && start[0] == mov_r7_192) //mmap2
        {
            uint32_t* jit = alloc_jit(sizeof(*jit) * 9);
            for(size_t i = 0; i < 7; i++)
                jit[i] = emu_mmap2[i];
            jit[4] = (uint32_t)modified_mmap;
            jit[7] = ldr_pc_pc_minus_4;
            jit[8] = (uint32_t)(start+2);
            start[0] = ldr_pc_pc_minus_4;
            start[1] = (uint32_t)jit;
        }
        else if(start[1] == syscall_instr && (start[0] == mov_r7_256 || get_ldr_r7_pc(start) == 0xf0005)) //set_tid_address, ARM_set_tls
        {
            uintptr_t* access = get_emutls_access_details();
            uint32_t* jit = alloc_jit(sizeof(*jit) * 9);
            jit[0] = emu_set_tid_address[0];
            jit[1] = emu_set_tid_address[1];
            jit[2] = emu_set_tid_address[2];
            jit[3] = emu_set_tid_address[3] | (access[0] << 2);
            if(start[0] == mov_r7_256) //set_tid_address
                jit[4] = emu_set_tid_address[4] | (access[1] + 4);
            else //ARM_set_tls
                jit[4] = emu_set_tid_address[4] | access[1];
            jit[5] = emu_set_tid_address[5];
            jit[6] = emu_set_tid_address[6];
            jit[7] = ldr_pc_pc_minus_4;
            jit[8] = (uint32_t)(start+2);
            start[0] = ldr_pc_pc_minus_4;
            start[1] = (uint32_t)jit;
        }
        else if(start[1] == syscall_instr && start[0] == mov_r7_1) //exit
        {
            uintptr_t* access = get_emutls_access_details();
            uint32_t* jit = alloc_jit(sizeof(*jit) * 5);
            jit[0] = mov_r0_tpidruro | (1 << 12);
            jit[1] = ldr_r0_r0_minus_imm | (1 << 16) | (1 << 12) | 4;
            jit[2] = ldr_r0_r0_imm | (1 << 16) | (1 << 12) | (access[0] << 2);
            jit[3] = ldr_r0_r0_imm | (1 << 16) | (13 << 12) | (access[1] + 12);
            jit[4] = ldr_r0_r0_imm | (1 << 16) | (15 << 12) | (access[1] + 8);
            start[0] = ldr_pc_pc_minus_4;
            start[1] = (uint32_t)jit;
        }
        else if((start[0] & 0xffff0fffu) == mov_r0_tpidruro)
        {
            uint32_t reg = (start[0] >> 12) & 15;
            if(!test_arm_instr(reg))
            {
                die("test_arm_instr failed\n");
                __builtin_trap();
            }
            uint32_t* jit = alloc_jit(sizeof(*jit) * 15);
            generate_load_emutls(jit, reg);
            jit[12] = start[1];
            jit[13] = ldr_pc_pc_minus_4;
            jit[14] = (uint32_t)(start + 2);
            start[0] = ldr_pc_pc_minus_4;
            start[1] = (uint32_t)jit;
        }
        start++;
    }
}

static int parse_thumb_b(uint16_t* i, uint32_t* tgt)
{
    if((i[0] & 0xf800) == 0xe000)
    {
        int32_t offset = i[0] & 0x7ff;
        if(offset >= 0x400)
            offset -= 0x800;
        if(tgt)
            *tgt = (uint32_t)(i + 2 + offset);
        return 1;
    }
    return 0;
}

static int parse_thumb_cbz(uint16_t* i, int* reg, uint32_t* tgt)
{
    if((i[0] & 0xfd00) == 0xb100)
    {
        if(reg)
            *reg = (i[0] & 7);
        uint16_t offset = (i[0] & 0x2f8) >> 3;
        if(offset >= 128)
            offset -= 64;
        if(tgt)
            *tgt = (uint32_t)(i + 2 + offset);
        return 1;
    }
    return 0;
}

static int parse_thumb_bl_bw(uint16_t* i, uint32_t* dest, uint32_t mask)
{
    uint32_t q = (i[0] << 16) | i[1];
    if((q & 0xf000d000) == mask)
    {
        if(dest)
        {
            q ^= mask;
            if(!(q & 0x4000000))
                q ^= 0x2800;
            q = (q & 0x7ff) | ((q & 0x800) << 10) | ((q & 0x2000) << 9) | ((q & 0x3ff0000) >> 5) | ((q & 0x4000000) >> 3);
            if(q >= 0x800000)
                q -= 0x1000000;
            *dest = (uint32_t)(i + 2) + q * 2;
        }
        return 1;
    }
    return 0;
}

//TODO: blx
static int parse_thumb_bl(uint16_t* i, uint32_t* dest)
{
    return parse_thumb_bl_bw(i, dest, 0xf000d000);
}

static int parse_thumb_bw(uint16_t* i, uint32_t* dest)
{
    return parse_thumb_bl_bw(i, dest, 0xf0009000);
}

static int parse_thumb_ldr_pc(uint16_t* i, int* reg, uint32_t* addr)
{
    if((i[0] & 0xf800) == 0x4800)
    {
        uint32_t base = (((uint32_t)i) & -4) + 4;
        if(reg)
            *reg = (i[0] >> 8) & 7;
        if(addr)
            *addr = base + ((i[0] & 0xff) << 2);
        return 1;
    }
    return 0;
}

static void make_thumb_jmp(uint16_t* i, uint32_t dst)
{
    uint32_t pc = (uint32_t)(i + 2);
    if(dst >= pc + 2048 || dst < pc - 2048)
        die("synthetic thumb jump too long: %p -> %p\n", i, (void*)dst);
    *i = thumb_b | (((dst - pc) / 2) & 0x7ff);
}

static uint16_t* get_patchable_space(uint16_t* start, uint16_t* limit/*, int* scratch_reg*/)
{
    uint16_t* start0 = start;
    while(start < limit)
    {
        int kind = test_thumb_instr((start[0] << 16) | start[1]);
        if(kind == 1)
            start++;
        else if(kind == 2)
            start += 2;
        else if(parse_thumb_b(start, NULL))
        {
            start++;
            return start;
        }
        else if(parse_thumb_bw(start, NULL))
        {
            start += 2;
            return start;
        }
        else if(parse_thumb_cbz(start, NULL, NULL))
            start++;
        else if(parse_thumb_ldr_pc(start, NULL, NULL))
            start++;
        else if(parse_thumb_bl(start, NULL))
        {
            start += 2;
            /*if(scratch_reg)
                *scratch_reg = 3;*/ //undefined per ABI, can be used as a scratch register
            return start;
        }
        else
            break;
    }
    if(start > start0)
        return start;
    else
        return 0;
}

static uint32_t* make_thumb_shadow_copy(uint16_t* start, uint16_t* end, size_t payload_size)
{
    payload_size = ((payload_size - 1) | 3) + 1;
    size_t n_cbz = 0;
    size_t n_ldr = 0;
    size_t n_bl = 0;
    uint16_t* i = start;
    while(i < end)
    {
        int kind = test_thumb_instr((i[0] << 16) | i[1]);
        if(kind == 1)
            i++;
        else if(kind == 2)
            i += 2;
        else if(parse_thumb_b(i, NULL))
            i++;
        else if(parse_thumb_bw(i, NULL))
            i += 2;
        else if(parse_thumb_cbz(i, NULL, NULL))
        {
            n_cbz++;
            i++;
        }
        else if(parse_thumb_ldr_pc(i, NULL, NULL))
        {
            n_ldr++;
            i++;
        }
        else if(parse_thumb_bl(i, NULL))
        {
            if(i + 2 != end)
                die("make_thumb_shadow_copy at %p->%p: BUG: bl not at the end\n", start, end);
            i += 2;
            n_bl++;
        }
        else
            die("make_thumb_shadow_copy at %p->%p: BUG: unsupported instruction\n", start, end);
    }
    if(i > end)
        die("make_thumb_shadow_copy at %p->%p: BUG: end is in the middle of an instruction\n", start, end);
    size_t jit_size = payload_size + (end - start) * 2 + 20 + 12 * n_cbz + 6 * n_ldr + 4 * n_bl;
    void* jit_base = alloc_jit(jit_size);
    uint32_t* jit = (void*)((char*)jit_base + payload_size);
    jit[0] = ldr_pc_pc_minus_4;
    jit[1] = (uint32_t)&jit[2] + 1;
    uint16_t* jit_thumb = (uint16_t*)&jit[2];
    uint32_t* cbz_slots = (uint32_t*)(jit_thumb + (end - start + n_ldr) + (end - start + n_ldr) % 2 + 6);
    i = start;
    while(i < end)
    {
        int kind = test_thumb_instr((i[0] << 16) | i[1]);
        int reg;
        uint32_t addr;
        if(kind == 1)
            *jit_thumb++ = *i++;
        else if(kind == 2)
        {
            *jit_thumb++ = *i++;
            *jit_thumb++ = *i++;
        }
        else if(parse_thumb_b(i, &addr) || parse_thumb_bw(i, &addr))
        {
            if(addr >= (uint32_t)start && addr < (uint32_t)end)
                die("make_thumb_shadow_copy at %p->%p: inner branch\n", start, end);
            if((((uint32_t)jit_thumb) & 2))
                *jit_thumb++ = thumb_nop;
            uint32_t* jit_arm = (uint32_t*)jit_thumb;
            *jit_arm++ = thumb_bx_pc;
            *jit_arm++ = ldr_pc_pc_minus_4;
            *jit_arm++ = addr | 1;
            n_bl = 1;
            if(parse_thumb_bw(i, &addr))
                i += 2;
            else
                i++;
        }
        else if(parse_thumb_cbz(i, &reg, &addr))
        {
            if(addr >= (uint32_t)start && addr < (uint32_t)end)
                die("make_thumb_shadow_copy at %p->%p: inner cbz\n", start, end);
            uint32_t shift = (uint32_t)cbz_slots - (uint32_t)(jit_thumb + 2);
            if(shift >= 128)
                die("make_thumb_shadow_copy at %p->%p: cbz too long\n", start, end);
            *jit_thumb++ = thumb_cbz | reg | ((shift & 64) << 4) | ((shift & 63) << 3);
            *cbz_slots++ = thumb_bx_pc;
            *cbz_slots++ = ldr_pc_pc_minus_4;
            *cbz_slots++ = addr | 1;
            i++;
        }
        else if(parse_thumb_ldr_pc(i, &reg, &addr))
        {
            uint32_t base = (uint32_t)jit_thumb;
            base = (base & -4) + 4;
            uint32_t shift = ((uint32_t)cbz_slots - base) / 4;
            if(shift >= 256)
                die("make_thumb_shadow_copy at %p->%p: ldr[pc] too long\n", start, end);
            *jit_thumb++ = thumb_ldr_reg_pc | (reg << 8) | shift;
            *jit_thumb++ = thumb_ldr_reg_reg | (reg << 3) | reg;
            *cbz_slots++ = addr;
            i++;
        }
        else if(parse_thumb_bl(i, &addr))
        {
            if((((uint32_t)jit_thumb) & 2))
                *jit_thumb++ = thumb_nop;
            uint32_t* jit_arm = (uint32_t*)jit_thumb;
            *jit_arm++ = thumb_bx_pc;
            *jit_arm++ = ldr_r0_r0_imm | (15 << 16) | (14 << 12) | 4;
            *jit_arm++ = ldr_pc_pc_minus_4;
            *jit_arm++ = addr | 1;
            *jit_arm++ = (uint32_t)(i+2) | 1;
            jit_thumb = (uint16_t*)jit_arm;
            i += 2;
        }
    }
    if(i > end)
        die("make_thumb_shadow_copy at %p->%p: BUG: overrun when emitting instructions\n");
    if(!n_bl)
    {
        if((((uint32_t)jit_thumb) & 2))
            *jit_thumb++ = thumb_nop;
        uint32_t* jit_arm = (uint32_t*)jit_thumb;
        *jit_arm++ = thumb_bx_pc;
        *jit_arm++ = ldr_pc_pc_minus_4;
        *jit_arm++ = (uint32_t)end | 1;
    }
    return jit_base;
}

static void make_6byte_jump(uint16_t* source, uint32_t target/*, int scratch_reg*/)
{
    uint16_t /*jump_instr = thumb_bx_reg | (scratch_reg << 3);
    if(scratch_reg == 16)*/
        jump_instr = thumb_ldr_pc_pc[1];
    uint32_t mapping = (uint32_t)mmap(0, 65536, PROT_READ|PROT_WRITE|PROT_EXEC, MAP_PRIVATE|MAP_ANON, -1, 0);
    uint32_t addr = (mapping & 0xffff0000u) | jump_instr;
    if(addr < mapping)
        addr  += 65536;
    uint32_t page_size = sysconf(_SC_PAGE_SIZE);
    uint32_t page = addr & -page_size;
    munmap((void*)mapping, page - mapping);
    munmap((void*)(page + page_size), mapping + 65536 - (page + page_size));
    *(uint32_t*)addr = ldr_pc_pc_minus_4;
    *(uint32_t*)(addr + 4) = target;
    //if(scratch_reg == 16)
        *source++ = thumb_ldr_pc_pc[0];
    /*else
        *source++ = thumb_ldr_reg_pc | (scratch_reg << 8);*/
    *(uint32_t*)source = addr; //jump instruction is embedded in the low 16 bytes of address
    mprotect((void*)page, page_size, PROT_READ|PROT_EXEC);
    __builtin___clear_cache((void*)page, (void*)(page + page_size));
}

static uint32_t* make_thumb_detour(uint16_t* start, uint16_t* cont, size_t payload_size, size_t cave_size, void** cave, /*int scratch_reg, */void* destination)
{
    uint16_t* detour_end;
    //int have_reg = (scratch_reg >= 0 && scratch_reg < 7) || scratch_reg == 16;
    //if(have_reg) //low reg available, can stay in thumb mode
        detour_end = start + 2;
    /*else //high reg or no scratch reg, have to switch to arm mode
    {
        detour_end = start;
        if((((uint32_t)detour_end) & 2))
            detour_end++;
        detour_end += 4;
    }*/
    uint16_t* patch_end = detour_end;
    if(((uint32_t)patch_end) & 2)
        patch_end++;
    patch_end += 2; //pc value for detour
    void* the_cave = patch_end;
    patch_end += (cave_size + 1) / 2;
    int inner_scratch = -1;
    uint16_t* space_end = get_patchable_space(cont, patch_end/*, &inner_scratch*/);
    int can_6byte = (((uint32_t)start) & 2) && space_end >= start + 3 && space_end < patch_end /*&& have_reg*/;
    if(space_end == 0)
        die("at %p: not enough space for patch (unsupported instruction)\n", start);
    else if(space_end < patch_end && !can_6byte)
    {
        uint32_t* shadow_copy = make_thumb_shadow_copy(cont, space_end, payload_size);
        if(!destination)
            destination = shadow_copy;
        int need_jump = /*!have_reg || */(space_end < detour_end);
        size_t extra_cavity = need_jump ? (/*have_reg ?*/ 8 /*: 12*/) : 4;
        void* inner_cave;
        make_thumb_detour(space_end, space_end, 0, cave_size + extra_cavity, &inner_cave, /*inner_scratch,*/ NULL);
        if(cave)
            *cave = (char*)inner_cave + extra_cavity;
        if(need_jump)
        {
            make_thumb_jmp(start, (uint32_t)inner_cave);
            /*if(have_reg)
            {*/
                uint16_t* inner = inner_cave;
                /*if(scratch_reg == 16)
                {*/
                    *inner++ = thumb_ldr_pc_pc[0];
                    *inner++ = thumb_ldr_pc_pc[1];
                /*}
                else
                {
                    *inner++ = thumb_ldr_reg_pc | (scratch_reg << 8);
                    *inner++ = thumb_bx_reg | (scratch_reg << 3);
                }*/
                *(uint32_t*)inner = (uint32_t)destination;
            /*}
            else
            {
                uint32_t* inner = inner_cave;
                *inner = thumb_bx_pc;
                *inner = ldr_pc_pc_minus_4;
                *inner = (uint32_t)destination;
            }*/
        }
        else //if(have_reg)
        {
            *(uint32_t*)inner_cave = (uint32_t)destination;
            uint32_t offset = (uint32_t)inner_cave - ((uint32_t)start & -4) - 4;
            if(offset >= 1024 || offset % 4)
                die("at %p: cave too far away\n", start);
            /*if(scratch_reg == 16)
            {*/
                *start++ = thumb_ldr_pc_pc[0];
                *start++ = thumb_ldr_pc_pc[1];
            /*}
            else
            {
                *start++ = thumb_ldr_reg_pc | (scratch_reg << 8) | (offset >> 2);
                *start++ = thumb_bx_reg | (scratch_reg << 3);
            }*/
        }
        /*else
        {
            *(uint32_t*)inner_cave = (uint32_t)destination;
            if((((uint32_t)start) & 2))
                *start++ = thumb_nop;
            uint32_t* p = (uint32_t*)start;
            *p++ = thumb_nop;
            uint32_t offset = (uint32_t)inner_cave - (uint32_t)p - 8;
            if(offset >= 4096)
                die("at %p: cave too far away\n", start);
            *start++ = ldr_r0_r0_imm | (15 << 16) | (15 << 12) | offset;
        }*/
        return shadow_copy;
    }
    else
    {
        if(cave)
            *cave = the_cave;
        //note: using the larger space_end size anyway to avoid splitting instructions in half
        uint32_t* shadow_copy = make_thumb_shadow_copy(cont, space_end, payload_size);
        if(!destination)
            destination = shadow_copy;
        if(can_6byte)
        {
            make_6byte_jump(start, (uint32_t)destination/*, scratch_reg*/);
            return shadow_copy;
        }
        if((((uint32_t)start) & 2))
            *start++ = thumb_nop;
        /*if(have_reg)
        {
            if(scratch_reg == 16)
            {*/
                *start++ = thumb_ldr_pc_pc[0];
                *start++ = thumb_ldr_pc_pc[1];
            /*}
            else
            {
                *start++ = thumb_ldr_reg_pc | (scratch_reg << 8);
                *start++ = thumb_bx_reg | (scratch_reg << 3);
            }*/
            *(uint32_t*)start = (uint32_t)destination;
        /*}
        else
        {
            uint32_t* p = (uint32_t*)start;
            *p++ = thumb_bx_pc;
            *p++ = ldr_pc_pc_minus_4;
            *p++ = (uint32_t)destination;
        }*/
        return shadow_copy;
    }
}

void* hook_function(void* source, void* target)
{
    uint32_t source_ptr = (uint32_t)source;
    if((source_ptr & 1))
    {
        //let's generously assume the detour fits in 32 bytes
        mprotect(source, 32, PROT_READ|PROT_WRITE|PROT_EXEC);
        void* ans = make_thumb_detour((void*)(source_ptr-1), (void*)(source_ptr-1), 0, 0, NULL, /*-1,*/ target);
        mprotect(source, 32, PROT_READ|PROT_EXEC);
        __builtin___clear_cache(source, (char*)source + 32);
        lock_jit();
        return ans;
    }
    else
    {
        uint32_t* arm = source;
        if(!test_arm_instr(arm[0]) || !test_arm_instr(arm[1]))
            die("hook_function: unsupported instructions at function prologue\n");
        uint32_t* jit = alloc_jit(sizeof(*jit) * 4);
        jit[0] = arm[0];
        jit[1] = arm[1];
        jit[2] = ldr_pc_pc_minus_4;
        jit[3] = (uint32_t)&arm[2];
        mprotect(arm, sizeof(*arm) * 2, PROT_READ|PROT_WRITE|PROT_EXEC);
        arm[0] = ldr_pc_pc_minus_4;
        arm[1] = (uint32_t)target;
        mprotect(arm, sizeof(*arm) * 2, PROT_READ|PROT_EXEC);
        __builtin___clear_cache(arm, arm + 2);
        lock_jit();
        return jit;
    }
}

static void patch_thumb(uint16_t* start, uint16_t* end)
{
    while(start + 7 < end)
    {
        uint32_t opcode = (start[0] << 16) | start[1];
        if((opcode & 0xffff0fffu) == mov_r0_tpidruro)
        {
            uint32_t reg = (opcode >> 12) & 15;
            uint32_t* jit = make_thumb_detour(start, start + 2, 48, 0, NULL, /*reg,*/ NULL);
            generate_load_emutls(jit, reg);
        }
        start++;
    }
}

void patch_dyld(uintptr_t start, uintptr_t end)
{
    patch_arm((uint32_t*)start, (uint32_t*)end);
    patch_thumb((uint16_t*)start, (uint16_t*)end);
    lock_jit();
}
