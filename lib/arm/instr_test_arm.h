static int test_arm_instr_1078(uint32_t instr) {
 if((instr & 0xf000u) == 0xf000u) {
  return !((instr & (1u << 4)));
 } else {
  return !((instr & 0xe10u) == 0xa10u);
 }
}

static int test_arm_instr_1052(uint32_t instr) {
 if((instr & 0xe40u) == 0xa40u) {
  return 0;
 } else {
  return !((instr & (1u << 4)));
 }
}

static int test_arm_instr_1893(uint32_t instr) {
 if((instr & 0xf0000u) == 0xf0000u) {
  return 0;
 } else {
  return !!((instr & 0xf000u) == 0xf000u);
 }
}

static int test_arm_instr_1919(uint32_t instr) {
 if((instr & (1u << 8))) {
  return !((instr & 0xd0u) == 0xc0u);
 } else if((instr & 0xc0u) == 0xc0u) {
  return 0;
 } else {
  return !((instr & (1u << 4)));
 }
}

static int test_arm_instr_1902(uint32_t instr) {
 if((instr & (1u << 5))) {
  return !!((instr & (1u << 4)));
 } else {
  return !((instr & (1u << 4)));
 }
}

static int test_arm_instr_1964(uint32_t instr) {
 if((instr & (1u << 10))) {
  return !!((instr & 0x320u) == 0x0u);
 } else {
  return !!((instr & 0x310u) == 0x0u);
 }
}

static int test_arm_instr_1915(uint32_t instr) {
 if((instr & (1u << 7))) {
  return !((instr & (1u << 6)));
 } else {
  return !((instr & 0x50u) == 0x10u);
 }
}

static int test_arm_instr_1903(uint32_t instr) {
 if((instr & (1u << 6))) {
  return 0;
 } else {
  return test_arm_instr_1902(instr);
 }
}

static int test_arm_instr_1957(uint32_t instr) {
 if((instr & (1u << 8))) {
  return !!((instr & 0x60u) == 0x0u);
 } else {
  return test_arm_instr_1903(instr);
 }
}

static int test_arm_instr_1956(uint32_t instr) {
 if((instr & (1u << 10))) {
  return !!((instr & 0x220u) == 0x0u);
 } else if((instr & (1u << 9))) {
  return 0;
 } else {
  return !((instr & 0x110u) == 0x10u);
 }
}

static int test_arm_instr_1920(uint32_t instr) {
 if((instr & (1u << 9))) {
  return test_arm_instr_1919(instr);
 } else if((instr & (1u << 8))) {
  return !((instr & 0xc0u) == 0xc0u);
 } else {
  return test_arm_instr_1915(instr);
 }
}

static int test_arm_instr_1904(uint32_t instr) {
 if((instr & (1u << 8))) {
  return !((instr & (1u << 5)));
 } else {
  return test_arm_instr_1903(instr);
 }
}

static int test_arm_instr_1950(uint32_t instr) {
 if((instr & (1u << 9))) {
  return !!((instr & 0x170u) == 0x0u);
 } else {
  return test_arm_instr_1904(instr);
 }
}

static int test_arm_instr_1948(uint32_t instr) {
 if((instr & (1u << 10))) {
  if((instr & (1u << 9))) {
   return !!((instr & 0x130u) == 0x0u);
  } else {
   return !((instr & 0x120u) == 0x20u);
  }
 } else if((instr & (1u << 9))) {
  return !!((instr & 0x110u) == 0x0u);
 } else {
  return !((instr & 0x110u) == 0x10u);
 }
}

static int test_arm_instr_1866(uint32_t instr) {
 if((instr & (1u << 6))) {
  return 0;
 } else {
  return !((instr & 0x30u) == 0x30u);
 }
}

static int test_arm_instr_1941(uint32_t instr) {
 if((instr & (1u << 9))) {
  if((instr & (1u << 8))) {
   return test_arm_instr_1866(instr);
  } else {
   return !!((instr & 0x70u) == 0x0u);
  }
 } else {
  return test_arm_instr_1904(instr);
 }
}

static int test_arm_instr_1939(uint32_t instr) {
 if((instr & (1u << 10))) {
  if((instr & (1u << 9))) {
   if((instr & (1u << 8))) {
    return !((instr & (1u << 5)));
   } else {
    return !!((instr & 0x30u) == 0x0u);
   }
  } else {
   return !((instr & 0x120u) == 0x20u);
  }
 } else {
  return !((instr & 0x110u) == 0x10u);
 }
}

static int test_arm_instr_1933(uint32_t instr) {
 if((instr & (1u << 9))) {
  if((instr & (1u << 8))) {
   return test_arm_instr_1866(instr);
  } else {
   return !!((instr & 0x30u) == 0x0u);
  }
 } else {
  return test_arm_instr_1904(instr);
 }
}

static int test_arm_instr_1931(uint32_t instr) {
 if((instr & (1u << 10))) {
  if((instr & (1u << 9))) {
   if((instr & (1u << 8))) {
    return !((instr & (1u << 5)));
   } else {
    return !((instr & (1u << 4)));
   }
  } else {
   return !((instr & 0x120u) == 0x20u);
  }
 } else {
  return !((instr & 0x110u) == 0x10u);
 }
}

static int test_arm_instr_1906(uint32_t instr) {
 if((instr & (1u << 9))) {
  if((instr & (1u << 8))) {
   return !((instr & 0x30u) == 0x30u);
  } else {
   return !!((instr & 0x30u) == 0x0u);
  }
 } else {
  return test_arm_instr_1904(instr);
 }
}

static int test_arm_instr_1901(uint32_t instr) {
 if((instr & 0x600u) == 0x400u) {
  return !((instr & 0x120u) == 0x20u);
 } else {
  return !((instr & 0x110u) == 0x10u);
 }
}

static int test_arm_instr_1922(uint32_t instr) {
 if((instr & (1u << 11))) {
  if((instr & (1u << 10))) {
   return test_arm_instr_1920(instr);
  } else {
   return test_arm_instr_1906(instr);
  }
 } else {
  return test_arm_instr_1901(instr);
 }
}

static int test_arm_instr_1908(uint32_t instr) {
 if((instr & (1u << 11))) {
  if((instr & (1u << 10))) {
   return 0;
  } else {
   return test_arm_instr_1906(instr);
  }
 } else {
  return test_arm_instr_1901(instr);
 }
}

static int test_arm_instr_1864(uint32_t instr) {
 if((instr & (1u << 10))) {
  if((instr & 0x2c0u) == 0xc0u) {
   return 0;
  } else {
   return !((instr & (1u << 5)));
  }
 } else if((instr & 0x300u) == 0x200u) {
  return 1;
 } else {
  return !((instr & 0xc0u) == 0xc0u);
 }
}

static int test_arm_instr_1868(uint32_t instr) {
 if((instr & (1u << 8))) {
  return 0;
 } else {
  return !((instr & 0x30u) == 0x30u);
 }
}

static int test_arm_instr_1867(uint32_t instr) {
 if((instr & 0xc0u) == 0xc0u) {
  return 0;
 } else {
  return !((instr & 0x30u) == 0x30u);
 }
}

static int test_arm_instr_1871(uint32_t instr) {
 if((instr & (1u << 11))) {
  if((instr & (1u << 10))) {
   return 0;
  } else if((instr & (1u << 9))) {
   return test_arm_instr_1868(instr);
  } else {
   return test_arm_instr_1867(instr);
  }
 } else {
  return test_arm_instr_1864(instr);
 }
}

static int test_arm_instr_1889(uint32_t instr) {
 if((instr & 0xf0000u) == 0xf0000u) {
  return 0;
 } else if((instr & 0xe000u) == 0xe000u) {
  if((instr & (1u << 12))) {
   if((instr & (1u << 11))) {
    return 0;
   } else {
    return test_arm_instr_1864(instr);
   }
  } else if((instr & (1u << 11))) {
   if((instr & (1u << 10))) {
    return 0;
   } else if((instr & (1u << 9))) {
    return test_arm_instr_1868(instr);
   } else if((instr & (1u << 8))) {
    return 0;
   } else {
    return test_arm_instr_1867(instr);
   }
  } else {
   return test_arm_instr_1864(instr);
  }
 } else {
  return test_arm_instr_1871(instr);
 }
}

static int test_arm_instr_1761(uint32_t instr) {
 if((instr & (1u << 9))) {
  return !!((instr & 0xd0u) == 0x10u);
 } else {
  return !!((instr & 0x1d0u) == 0x0u);
 }
}

static int test_arm_instr_1537(uint32_t instr) {
 if((instr & (1u << 7))) {
  return !((instr & (1u << 4)));
 } else {
  return !((instr & 0x11u) == 0x11u);
 }
}

static int test_arm_instr_1841(uint32_t instr) {
 if((instr & (1u << 8))) {
  if((instr & (1u << 7))) {
   return 0;
  } else {
   return !((instr & 0x11u) == 0x11u);
  }
 } else {
  return test_arm_instr_1537(instr);
 }
}

static int test_arm_instr_1408(uint32_t instr) {
 if((instr & (1u << 6))) {
  return !!((instr & 0x11u) == 0x10u);
 } else {
  return !!((instr & (1u << 4)));
 }
}

static int test_arm_instr_1578(uint32_t instr) {
 if((instr & (1u << 7))) {
  return 0;
 } else {
  return test_arm_instr_1408(instr);
 }
}

static int test_arm_instr_1753(uint32_t instr) {
 if((instr & (1u << 9))) {
  return test_arm_instr_1578(instr);
 } else {
  return !!((instr & 0x190u) == 0x0u);
 }
}

static int test_arm_instr_1541(uint32_t instr) {
 if((instr & 0x1c0u) == 0x0u) {
  return 1;
 } else {
  return !((instr & (1u << 4)));
 }
}

static int test_arm_instr_1744(uint32_t instr) {
 if((instr & 0x280u) == 0x0u) {
  return !((instr & 0x11u) == 0x11u);
 } else {
  return !((instr & (1u << 4)));
 }
}

static int test_arm_instr_1762(uint32_t instr) {
 if((instr & (1u << 10))) {
  return test_arm_instr_1761(instr);
 } else {
  return test_arm_instr_1744(instr);
 }
}

static int test_arm_instr_1577(uint32_t instr) {
 if((instr & (1u << 9))) {
  return test_arm_instr_1541(instr);
 } else {
  return test_arm_instr_1537(instr);
 }
}

static int test_arm_instr_1754(uint32_t instr) {
 if((instr & (1u << 10))) {
  return test_arm_instr_1753(instr);
 } else {
  return test_arm_instr_1577(instr);
 }
}

static int test_arm_instr_1780(uint32_t instr) {
 if((instr & (1u << 12))) {
  if((instr & (1u << 11))) {
   return test_arm_instr_1762(instr);
  } else {
   return !!((instr & 0x50u) == 0x10u);
  }
 } else if((instr & (1u << 11))) {
  return test_arm_instr_1754(instr);
 } else {
  return test_arm_instr_1408(instr);
 }
}

static int test_arm_instr_1773(uint32_t instr) {
 if((instr & (1u << 8))) {
  return !!((instr & 0x50u) == 0x10u);
 } else if((instr & (1u << 6))) {
  return !!((instr & 0x11u) == 0x0u);
 } else {
  return !((instr & 0x11u) == 0x1u);
 }
}

static int test_arm_instr_1465(uint32_t instr) {
 if((instr & (1u << 6))) {
  return !!((instr & 0x11u) == 0x10u);
 } else {
  return 1;
 }
}

static int test_arm_instr_1767(uint32_t instr) {
 if((instr & (1u << 7))) {
  return test_arm_instr_1408(instr);
 } else {
  return test_arm_instr_1465(instr);
 }
}

static int test_arm_instr_1768(uint32_t instr) {
 if((instr & (1u << 8))) {
  return test_arm_instr_1767(instr);
 } else if((instr & 0x50u) == 0x10u) {
  return 1;
 } else {
  return !((instr & (1u << 0)));
 }
}

static int test_arm_instr_1607(uint32_t instr) {
 if((instr & (1u << 6))) {
  return !((instr & (1u << 0)));
 } else {
  return !!((instr & (1u << 4)));
 }
}

static int test_arm_instr_1757(uint32_t instr) {
 if((instr & 0x380u) == 0x280u) {
  return !!((instr & 0x50u) == 0x10u);
 } else {
  return !((instr & (1u << 6)));
 }
}

static int test_arm_instr_1749(uint32_t instr) {
 if((instr & 0x380u) == 0x280u) {
  return test_arm_instr_1408(instr);
 } else {
  return !((instr & 0x41u) == 0x41u);
 }
}

static int test_arm_instr_1745(uint32_t instr) {
 if((instr & (1u << 10))) {
  return !!((instr & 0x2d0u) == 0x210u);
 } else {
  return test_arm_instr_1744(instr);
 }
}

static int test_arm_instr_1405(uint32_t instr) {
 if((instr & (1u << 8))) {
  return !!((instr & 0x50u) == 0x10u);
 } else {
  return !((instr & (1u << 6)));
 }
}

static int test_arm_instr_1621(uint32_t instr) {
 if((instr & 0x280u) == 0x200u) {
  return test_arm_instr_1408(instr);
 } else {
  return 0;
 }
}

static int test_arm_instr_1738(uint32_t instr) {
 if((instr & (1u << 10))) {
  return test_arm_instr_1621(instr);
 } else {
  return test_arm_instr_1577(instr);
 }
}

static int test_arm_instr_1735(uint32_t instr) {
 if((instr & (1u << 8))) {
  return test_arm_instr_1408(instr);
 } else {
  return !((instr & 0x41u) == 0x41u);
 }
}

static int test_arm_instr_1481(uint32_t instr) {
 if((instr & (1u << 6))) {
  return 0;
 } else {
  return !((instr & 0x11u) == 0x1u);
 }
}

static int test_arm_instr_1664(uint32_t instr) {
 if((instr & (1u << 6))) {
  return !!((instr & 0x11u) == 0x10u);
 } else {
  return !((instr & 0x11u) == 0x1u);
 }
}

static int test_arm_instr_1764(uint32_t instr) {
 if((instr & (1u << 12))) {
  if((instr & (1u << 11))) {
   return test_arm_instr_1762(instr);
  } else if((instr & (1u << 10))) {
   return !!((instr & 0x50u) == 0x10u);
  } else {
   return test_arm_instr_1757(instr);
  }
 } else if((instr & (1u << 11))) {
  return test_arm_instr_1754(instr);
 } else if((instr & (1u << 10))) {
  return test_arm_instr_1408(instr);
 } else {
  return test_arm_instr_1749(instr);
 }
}

static int test_arm_instr_1614(uint32_t instr) {
 if((instr & (1u << 10))) {
  return !!((instr & 0x2d0u) == 0x210u);
 } else {
  return !!((instr & 0x291u) == 0x10u);
 }
}

static int test_arm_instr_1699(uint32_t instr) {
 if((instr & (1u << 8))) {
  return 0;
 } else if((instr & 0xc0u) == 0x0u) {
  return 1;
 } else {
  return !((instr & (1u << 4)));
 }
}

static int test_arm_instr_1505(uint32_t instr) {
 if((instr & (1u << 7))) {
  return !!((instr & 0x50u) == 0x0u);
 } else if((instr & (1u << 6))) {
  return !!((instr & 0x11u) == 0x10u);
 } else {
  return !((instr & 0x11u) == 0x11u);
 }
}

static int test_arm_instr_1507(uint32_t instr) {
 if((instr & (1u << 8))) {
  return !!((instr & 0x91u) == 0x10u);
 } else {
  return test_arm_instr_1505(instr);
 }
}

static int test_arm_instr_1710(uint32_t instr) {
 if((instr & (1u << 9))) {
  return test_arm_instr_1699(instr);
 } else {
  return test_arm_instr_1507(instr);
 }
}

static int test_arm_instr_1561(uint32_t instr) {
 if((instr & (1u << 6))) {
  return !((instr & 0x11u) == 0x11u);
 } else {
  return !!((instr & (1u << 4)));
 }
}

static int test_arm_instr_1688(uint32_t instr) {
 if((instr & (1u << 8))) {
  return test_arm_instr_1465(instr);
 } else {
  return test_arm_instr_1408(instr);
 }
}

static int test_arm_instr_1560(uint32_t instr) {
 if((instr & (1u << 8))) {
  return test_arm_instr_1408(instr);
 } else {
  return !((instr & 0x51u) == 0x51u);
 }
}

static int test_arm_instr_1519(uint32_t instr) {
 if((instr & (1u << 8))) {
  return test_arm_instr_1408(instr);
 } else {
  return test_arm_instr_1465(instr);
 }
}

static int test_arm_instr_1709(uint32_t instr) {
 if((instr & (1u << 10))) {
  if((instr & (1u << 9))) {
   if((instr & (1u << 8))) {
    return test_arm_instr_1465(instr);
   } else {
    return test_arm_instr_1561(instr);
   }
  } else {
   return test_arm_instr_1688(instr);
  }
 } else if((instr & (1u << 9))) {
  return test_arm_instr_1560(instr);
 } else {
  return test_arm_instr_1519(instr);
 }
}

static int test_arm_instr_1673(uint32_t instr) {
 if((instr & 0x500u) == 0x400u) {
  return test_arm_instr_1481(instr);
 } else {
  return !!((instr & 0x50u) == 0x10u);
 }
}

static int test_arm_instr_1701(uint32_t instr) {
 if((instr & 0x140u) == 0x100u) {
  return 0;
 } else {
  return !((instr & (1u << 4)));
 }
}

static int test_arm_instr_1553(uint32_t instr) {
 if((instr & (1u << 7))) {
  return !!((instr & 0x50u) == 0x40u);
 } else if((instr & (1u << 6))) {
  return !((instr & 0x11u) == 0x11u);
 } else {
  return !!((instr & 0x11u) == 0x10u);
 }
}

static int test_arm_instr_1695(uint32_t instr) {
 if((instr & (1u << 6))) {
  return !((instr & 0x11u) == 0x11u);
 } else {
  return !((instr & 0x11u) == 0x1u);
 }
}

static int test_arm_instr_1696(uint32_t instr) {
 if((instr & (1u << 8))) {
  return test_arm_instr_1465(instr);
 } else {
  return test_arm_instr_1695(instr);
 }
}

static int test_arm_instr_1691(uint32_t instr) {
 if((instr & (1u << 11))) {
  return !!((instr & 0x691u) == 0x10u);
 } else {
  return !!((instr & 0x50u) == 0x10u);
 }
}

static int test_arm_instr_1686(uint32_t instr) {
 if((instr & (1u << 11))) {
  return !!((instr & 0x691u) == 0x10u);
 } else {
  return test_arm_instr_1673(instr);
 }
}

static int test_arm_instr_1510(uint32_t instr) {
 if((instr & (1u << 9))) {
  if((instr & (1u << 8))) {
   return 0;
  } else if((instr & (1u << 7))) {
   return !!((instr & 0x50u) == 0x0u);
  } else {
   return !((instr & (1u << 6)));
  }
 } else {
  return test_arm_instr_1507(instr);
 }
}

static int test_arm_instr_1684(uint32_t instr) {
 if((instr & (1u << 10))) {
  return !!((instr & 0x350u) == 0x0u);
 } else {
  return test_arm_instr_1510(instr);
 }
}

static int test_arm_instr_1488(uint32_t instr) {
 if((instr & 0x700u) == 0x700u) {
  return !!((instr & 0xf0u) == 0x10u);
 } else {
  return !!((instr & 0xd0u) == 0x10u);
 }
}

static int test_arm_instr_1473(uint32_t instr) {
 if((instr & (1u << 7))) {
  return !!((instr & 0x50u) == 0x0u);
 } else {
  return !((instr & 0x50u) == 0x40u);
 }
}

static int test_arm_instr_1475(uint32_t instr) {
 if((instr & (1u << 8))) {
  return !!((instr & 0x90u) == 0x10u);
 } else {
  return test_arm_instr_1473(instr);
 }
}

static int test_arm_instr_1670(uint32_t instr) {
 if((instr & 0x600u) == 0x600u) {
  if((instr & (1u << 8))) {
   return !!((instr & 0xb0u) == 0x10u);
  } else {
   return !!((instr & 0x90u) == 0x10u);
  }
 } else {
  return test_arm_instr_1475(instr);
 }
}

static int test_arm_instr_1466(uint32_t instr) {
 if((instr & (1u << 7))) {
  return test_arm_instr_1465(instr);
 } else {
  return !((instr & 0x50u) == 0x40u);
 }
}

static int test_arm_instr_1491(uint32_t instr) {
 if((instr & 0xc0u) == 0xc0u) {
  return !!((instr & 0x11u) == 0x10u);
 } else {
  return !!((instr & (1u << 4)));
 }
}

static int test_arm_instr_1492(uint32_t instr) {
 if((instr & (1u << 8))) {
  return test_arm_instr_1491(instr);
 } else {
  return test_arm_instr_1466(instr);
 }
}

static int test_arm_instr_1468(uint32_t instr) {
 if((instr & (1u << 6))) {
  return !!((instr & (1u << 4)));
 } else {
  return !((instr & 0x11u) == 0x1u);
 }
}

static int test_arm_instr_1411(uint32_t instr) {
 if((instr & (1u << 6))) {
  return !!((instr & 0x11u) == 0x0u);
 } else {
  return !((instr & (1u << 4)));
 }
}

static int test_arm_instr_1461(uint32_t instr) {
 if((instr & 0x11000u) == 0x0u) {
  if((instr & (1u << 11))) {
   if((instr & 0x700u) == 0x0u) {
    return test_arm_instr_1411(instr);
   } else {
    return 0;
   }
  } else if((instr & (1u << 10))) {
   if((instr & (1u << 9))) {
    return 0;
   } else {
    return !((instr & 0x41u) == 0x41u);
   }
  } else if((instr & 0x300u) == 0x300u) {
   return 0;
  } else {
   return test_arm_instr_1408(instr);
  }
 } else if((instr & (1u << 11))) {
  return !!((instr & 0x750u) == 0x0u);
 } else if((instr & (1u << 10))) {
  return !!((instr & 0x240u) == 0x0u);
 } else if((instr & 0x300u) == 0x300u) {
  return 0;
 } else {
  return !!((instr & 0x50u) == 0x10u);
 }
}

static int test_arm_instr_1631(uint32_t instr) {
 if((instr & (1u << 8))) {
  return !!((instr & 0x50u) == 0x0u);
 } else {
  return !((instr & 0x41u) == 0x41u);
 }
}

static int test_arm_instr_1644(uint32_t instr) {
 if((instr & (1u << 9))) {
  if((instr & (1u << 8))) {
   return test_arm_instr_1411(instr);
  } else {
   return !((instr & (1u << 6)));
  }
 } else if((instr & (1u << 8))) {
  return test_arm_instr_1411(instr);
 } else {
  return !((instr & 0x41u) == 0x41u);
 }
}

static int test_arm_instr_1636(uint32_t instr) {
 if((instr & (1u << 8))) {
  return !!((instr & 0x50u) == 0x0u);
 } else {
  return !((instr & (1u << 6)));
 }
}

static int test_arm_instr_1520(uint32_t instr) {
 if((instr & (1u << 8))) {
  return test_arm_instr_1465(instr);
 } else {
  return 0;
 }
}

static int test_arm_instr_1615(uint32_t instr) {
 if((instr & (1u << 11))) {
  return test_arm_instr_1614(instr);
 } else if((instr & 0x500u) == 0x400u) {
  return !!((instr & 0x50u) == 0x0u);
 } else {
  return !((instr & (1u << 6)));
 }
}

static int test_arm_instr_1588(uint32_t instr) {
 if((instr & (1u << 10))) {
  if((instr & (1u << 9))) {
   return !!((instr & 0xd0u) == 0x10u);
  } else {
   return !!((instr & 0x50u) == 0x40u);
  }
 } else if((instr & (1u << 9))) {
  return 0;
 } else {
  return test_arm_instr_1553(instr);
 }
}

static int test_arm_instr_1546(uint32_t instr) {
 if((instr & (1u << 6))) {
  return !((instr & (1u << 4)));
 } else {
  return !!((instr & (1u << 4)));
 }
}

static int test_arm_instr_1582(uint32_t instr) {
 if((instr & (1u << 9))) {
  return !!((instr & 0x50u) == 0x10u);
 } else {
  return test_arm_instr_1546(instr);
 }
}

static int test_arm_instr_1580(uint32_t instr) {
 if((instr & (1u << 10))) {
  if((instr & (1u << 9))) {
   return test_arm_instr_1578(instr);
  } else {
   return !((instr & (1u << 4)));
  }
 } else {
  return test_arm_instr_1577(instr);
 }
}

static int test_arm_instr_1565(uint32_t instr) {
 if((instr & (1u << 8))) {
  return !((instr & 0x51u) == 0x51u);
 } else {
  return !!((instr & 0x50u) == 0x40u);
 }
}

static int test_arm_instr_1562(uint32_t instr) {
 if((instr & (1u << 8))) {
  return test_arm_instr_1561(instr);
 } else {
  return !((instr & 0x51u) == 0x51u);
 }
}

static int test_arm_instr_1513(uint32_t instr) {
 if((instr & (1u << 8))) {
  return !!((instr & 0x50u) == 0x10u);
 } else {
  return !!((instr & 0x51u) == 0x0u);
 }
}

static int test_arm_instr_1532(uint32_t instr) {
 if((instr & (1u << 6))) {
  return !((instr & (1u << 4)));
 } else {
  return !!((instr & 0x11u) == 0x0u);
 }
}

static int test_arm_instr_1557(uint32_t instr) {
 if((instr & (1u << 10))) {
  return !!((instr & 0x250u) == 0x40u);
 } else if((instr & (1u << 9))) {
  return 0;
 } else if((instr & (1u << 8))) {
  return !!((instr & 0x91u) == 0x10u);
 } else {
  return test_arm_instr_1553(instr);
 }
}

static int test_arm_instr_1548(uint32_t instr) {
 if((instr & 0x300u) == 0x0u) {
  return test_arm_instr_1546(instr);
 } else {
  return !!((instr & 0x50u) == 0x10u);
 }
}

static int test_arm_instr_1544(uint32_t instr) {
 if((instr & (1u << 10))) {
  return !!((instr & 0x210u) == 0x0u);
 } else if((instr & (1u << 9))) {
  return test_arm_instr_1541(instr);
 } else if((instr & (1u << 8))) {
  return test_arm_instr_1505(instr);
 } else {
  return test_arm_instr_1537(instr);
 }
}

static int test_arm_instr_1511(uint32_t instr) {
 if((instr & (1u << 10))) {
  return !!((instr & 0x150u) == 0x0u);
 } else {
  return test_arm_instr_1510(instr);
 }
}

static int test_arm_instr_1479(uint32_t instr) {
 if((instr & 0x600u) == 0x600u) {
  if((instr & (1u << 8))) {
   return !!((instr & 0xb0u) == 0x10u);
  } else {
   return test_arm_instr_1473(instr);
  }
 } else {
  return test_arm_instr_1475(instr);
 }
}

static int test_arm_instr_1409(uint32_t instr) {
 if((instr & (1u << 8))) {
  return !((instr & 0x41u) == 0x41u);
 } else {
  return test_arm_instr_1408(instr);
 }
}

static int test_arm_instr_1426(uint32_t instr) {
 if((instr & (1u << 9))) {
  if((instr & (1u << 8))) {
   if((instr & (1u << 6))) {
    return !!((instr & 0x11u) == 0x0u);
   } else {
    return 1;
   }
  } else {
   return !((instr & (1u << 6)));
  }
 } else {
  return !((instr & 0x41u) == 0x41u);
 }
}

static int test_arm_instr_1417(uint32_t instr) {
 if((instr & (1u << 8))) {
  return !((instr & (1u << 6)));
 } else {
  return !!((instr & 0x50u) == 0x10u);
 }
}

static int test_arm_instr_1366(uint32_t instr) {
 if((instr & (1u << 6))) {
  return !!((instr & 0x30u) == 0x0u);
 } else {
  return !((instr & (1u << 5)));
 }
}

static int test_arm_instr_1369(uint32_t instr) {
 if((instr & (1u << 7))) {
  return test_arm_instr_1366(instr);
 } else {
  return !!((instr & 0x70u) == 0x50u);
 }
}

static int test_arm_instr_1392(uint32_t instr) {
 if((instr & (1u << 16))) {
  return 0;
 } else if((instr & 0xfe00u) == 0x0u) {
  return !((instr & (1u << 5)));
 } else {
  return test_arm_instr_1369(instr);
 }
}

static int test_arm_instr_1376(uint32_t instr) {
 if((instr & 0xfe00u) == 0x0u) {
  if((instr & (1u << 7))) {
   return test_arm_instr_1366(instr);
  } else if((instr & (1u << 6))) {
   if((instr & (1u << 5))) {
    return 0;
   } else if((instr & (1u << 4))) {
    return 1;
   } else {
    return !!((instr & 0xfu) == 0x0u);
   }
  } else {
   return !!((instr & 0x3fu) == 0x0u);
  }
 } else {
  return test_arm_instr_1369(instr);
 }
}

static int test_arm_instr_1082(uint32_t instr) {
 if((instr & (1u << 7))) {
  return !!((instr & 0x50u) == 0x40u);
 } else if((instr & 0x60u) == 0x20u) {
  return 0;
 } else {
  return !((instr & (1u << 4)));
 }
}

static int test_arm_instr_1095(uint32_t instr) {
 if((instr & 0xe00u) == 0xa00u) {
  return test_arm_instr_1082(instr);
 } else {
  return 1;
 }
}

static int test_arm_instr_995(uint32_t instr) {
 if((instr & (1u << 4))) {
  return !!((instr & 0xfu) == 0x0u);
 } else {
  return 1;
 }
}

static int test_arm_instr_1189(uint32_t instr) {
 if((instr & (1u << 7))) {
  if((instr & (1u << 6))) {
   return test_arm_instr_995(instr);
  } else {
   return !!((instr & 0x1fu) == 0x10u);
  }
 } else if((instr & 0x60u) == 0x20u) {
  return !!((instr & 0x1fu) == 0x10u);
 } else {
  return test_arm_instr_995(instr);
 }
}

static int test_arm_instr_1214(uint32_t instr) {
 if((instr & 0xf000u) == 0xf000u) {
  return test_arm_instr_1095(instr);
 } else if((instr & 0xe00u) == 0xa00u) {
  if((instr & (1u << 8))) {
   return test_arm_instr_1189(instr);
  } else {
   return test_arm_instr_1082(instr);
  }
 } else {
  return 1;
 }
}

static int test_arm_instr_1183(uint32_t instr) {
 if((instr & (1u << 5))) {
  return 0;
 } else {
  return test_arm_instr_995(instr);
 }
}

static int test_arm_instr_1185(uint32_t instr) {
 if((instr & (1u << 7))) {
  return !!((instr & 0x50u) == 0x40u);
 } else if((instr & (1u << 6))) {
  return !((instr & (1u << 4)));
 } else {
  return test_arm_instr_1183(instr);
 }
}

static int test_arm_instr_1193(uint32_t instr) {
 if((instr & 0xe00u) == 0xa00u) {
  if((instr & (1u << 8))) {
   return test_arm_instr_1189(instr);
  } else {
   return test_arm_instr_1185(instr);
  }
 } else {
  return 1;
 }
}

static int test_arm_instr_1197(uint32_t instr) {
 if((instr & 0xf000u) == 0xf000u) {
  return test_arm_instr_1095(instr);
 } else {
  return test_arm_instr_1193(instr);
 }
}

static int test_arm_instr_1129(uint32_t instr) {
 if((instr & 0xe00u) == 0xa00u) {
  return !!((instr & 0xf0u) == 0x0u);
 } else {
  return 1;
 }
}

static int test_arm_instr_1102(uint32_t instr) {
 if((instr & (1u << 7))) {
  return !!((instr & 0x7fu) == 0x40u);
 } else if((instr & (1u << 6))) {
  return !!((instr & 0x3fu) == 0x0u);
 } else {
  return !!((instr & 0x30u) == 0x0u);
 }
}

static int test_arm_instr_1113(uint32_t instr) {
 if((instr & 0xe00u) == 0xa00u) {
  return test_arm_instr_1102(instr);
 } else {
  return 1;
 }
}

static int test_arm_instr_1176(uint32_t instr) {
 if((instr & (1u << 16))) {
  return test_arm_instr_1078(instr);
 } else if((instr & 0xf000u) == 0xf000u) {
  return !((instr & (1u << 4)));
 } else if((instr & 0xe00u) == 0xa00u) {
  if((instr & 0x160u) == 0x100u) {
   return test_arm_instr_995(instr);
  } else {
   return !((instr & (1u << 4)));
  }
 } else {
  return 1;
 }
}

static int test_arm_instr_997(uint32_t instr) {
 if((instr & 0x60u) == 0x0u) {
  return test_arm_instr_995(instr);
 } else {
  return !((instr & (1u << 4)));
 }
}

static int test_arm_instr_1158(uint32_t instr) {
 if((instr & 0xf000u) == 0xf000u) {
  return !((instr & (1u << 4)));
 } else if((instr & 0xe00u) == 0xa00u) {
  if((instr & (1u << 8))) {
   return test_arm_instr_997(instr);
  } else {
   return !((instr & 0x90u) == 0x90u);
  }
 } else {
  return 1;
 }
}

static int test_arm_instr_1167(uint32_t instr) {
 if((instr & (1u << 16))) {
  if((instr & 0xf000u) == 0xf000u) {
   return !((instr & (1u << 4)));
  } else if((instr & 0xe00u) == 0xa00u) {
   if((instr & 0x180u) == 0x0u) {
    return 1;
   } else {
    return !((instr & (1u << 4)));
   }
  } else {
   return 1;
  }
 } else {
  return test_arm_instr_1158(instr);
 }
}

static int test_arm_instr_1033(uint32_t instr) {
 if((instr & 0xe00u) == 0xa00u) {
  if((instr & (1u << 8))) {
   return test_arm_instr_995(instr);
  } else {
   return !((instr & (1u << 4)));
  }
 } else {
  return 1;
 }
}

static int test_arm_instr_1041(uint32_t instr) {
 if((instr & 0xf000u) == 0xf000u) {
  return !((instr & 0xe10u) == 0xa10u);
 } else {
  return test_arm_instr_1033(instr);
 }
}

static int test_arm_instr_1084(uint32_t instr) {
 if((instr & (1u << 5))) {
  return !!((instr & 0x1fu) == 0x10u);
 } else {
  return !((instr & (1u << 4)));
 }
}

static int test_arm_instr_998(uint32_t instr) {
 if((instr & (1u << 5))) {
  return test_arm_instr_995(instr);
 } else {
  return !((instr & (1u << 4)));
 }
}

static int test_arm_instr_1099(uint32_t instr) {
 if((instr & 0xf000u) == 0xf000u) {
  return test_arm_instr_1095(instr);
 } else if((instr & 0xe00u) == 0xa00u) {
  if((instr & (1u << 8))) {
   if((instr & (1u << 7))) {
    if((instr & (1u << 6))) {
     return test_arm_instr_998(instr);
    } else {
     return !!((instr & 0x3fu) == 0x30u);
    }
   } else if((instr & (1u << 6))) {
    return test_arm_instr_998(instr);
   } else {
    return test_arm_instr_1084(instr);
   }
  } else {
   return test_arm_instr_1082(instr);
  }
 } else {
  return 1;
 }
}

static int test_arm_instr_1103(uint32_t instr) {
 if((instr & (1u << 5))) {
  return !!((instr & 0x1fu) == 0x10u);
 } else {
  return !!((instr & 0x1fu) == 0x0u);
 }
}

static int test_arm_instr_999(uint32_t instr) {
 if((instr & 0x60u) == 0x40u) {
  return !((instr & (1u << 4)));
 } else {
  return test_arm_instr_995(instr);
 }
}

static int test_arm_instr_1019(uint32_t instr) {
 if((instr & 0xe00u) == 0xa00u) {
  if((instr & (1u << 8))) {
   return test_arm_instr_999(instr);
  } else {
   return !((instr & (1u << 4)));
  }
 } else {
  return 1;
 }
}

static int test_arm_instr_1003(uint32_t instr) {
 if((instr & 0xe00u) == 0xa00u) {
  if((instr & (1u << 8))) {
   return test_arm_instr_999(instr);
  } else {
   return test_arm_instr_997(instr);
  }
 } else {
  return 1;
 }
}

static int test_arm_instr_959(uint32_t instr) {
 if((instr & 0xf0000u) == 0xf0000u) {
  return 0;
 } else {
  return !((instr & 0xe00u) == 0xa00u);
 }
}

static int test_arm_instr_987(uint32_t instr) {
 if((instr & 0xf0000u) == 0xf0000u) {
  return 0;
 } else {
  return !((instr & 0xf01u) == 0xb01u);
 }
}

static int test_arm_instr_22(uint32_t instr) {
 if((instr & (1u << 4))) {
  return !((instr & 0xfu) == 0xfu);
 } else {
  return 0;
 }
}

static int test_arm_instr_1256(uint32_t instr) {
 if((instr & (1u << 26))) {
  if((instr & (1u << 25))) {
   if((instr & (1u << 24))) {
    return 1;
   } else if((instr & (1u << 23))) {
    if((instr & (1u << 22))) {
     if((instr & (1u << 21))) {
      if((instr & (1u << 20))) {
       if((instr & (1u << 19))) {
        if((instr & (1u << 18))) {
         return test_arm_instr_1214(instr);
        } else if((instr & (1u << 17))) {
         if((instr & (1u << 16))) {
          return test_arm_instr_1214(instr);
         } else {
          return test_arm_instr_1197(instr);
         }
        } else if((instr & (1u << 16))) {
         if((instr & 0xf000u) == 0xf000u) {
          return test_arm_instr_1129(instr);
         } else if((instr & 0xe00u) == 0xa00u) {
          if((instr & (1u << 8))) {
           if((instr & 0xe0u) == 0x0u) {
            return test_arm_instr_995(instr);
           } else {
            return !!((instr & 0x1fu) == 0x10u);
           }
          } else if((instr & 0xe0u) == 0x0u) {
           return test_arm_instr_995(instr);
          } else {
           return 0;
          }
         } else {
          return 1;
         }
        } else {
         return test_arm_instr_1197(instr);
        }
       } else if((instr & (1u << 18))) {
        if((instr & (1u << 17))) {
         return test_arm_instr_1197(instr);
        } else if((instr & (1u << 16))) {
         if((instr & 0xf000u) == 0xf000u) {
          return test_arm_instr_1113(instr);
         } else if((instr & 0xe00u) == 0xa00u) {
          if((instr & (1u << 8))) {
           if((instr & (1u << 7))) {
            if((instr & 0x60u) == 0x40u) {
             return !!((instr & 0xfu) == 0x0u);
            } else {
             return !!((instr & 0x1fu) == 0x10u);
            }
           } else if((instr & (1u << 6))) {
            if((instr & 0x30u) == 0x20u) {
             return 0;
            } else {
             return !!((instr & 0xfu) == 0x0u);
            }
           } else if((instr & (1u << 5))) {
            return !!((instr & 0x1fu) == 0x10u);
           } else {
            return test_arm_instr_995(instr);
           }
          } else if((instr & (1u << 7))) {
           return !!((instr & 0x7fu) == 0x40u);
          } else if((instr & (1u << 6))) {
           return !!((instr & 0x3fu) == 0x0u);
          } else {
           return test_arm_instr_1183(instr);
          }
         } else {
          return 1;
         }
        } else {
         return test_arm_instr_1214(instr);
        }
       } else if((instr & (1u << 17))) {
        return test_arm_instr_1214(instr);
       } else if((instr & (1u << 16))) {
        if((instr & 0xf000u) == 0xf000u) {
         if((instr & 0xe00u) == 0xa00u) {
          if((instr & (1u << 8))) {
           return test_arm_instr_1082(instr);
          } else {
           return test_arm_instr_1185(instr);
          }
         } else {
          return 1;
         }
        } else {
         return test_arm_instr_1193(instr);
        }
       } else {
        return test_arm_instr_1197(instr);
       }
      } else if((instr & (1u << 19))) {
       if((instr & (1u << 18))) {
        return test_arm_instr_1176(instr);
       } else if((instr & (1u << 17))) {
        if((instr & (1u << 16))) {
         return test_arm_instr_1078(instr);
        } else {
         return test_arm_instr_1158(instr);
        }
       } else {
        return test_arm_instr_1167(instr);
       }
      } else if((instr & 0x60000u) == 0x0u) {
       return test_arm_instr_1167(instr);
      } else {
       return test_arm_instr_1176(instr);
      }
     } else if((instr & (1u << 20))) {
      return test_arm_instr_1041(instr);
     } else if((instr & 0xf000u) == 0xf000u) {
      return test_arm_instr_1052(instr);
     } else if((instr & 0xe00u) == 0xa00u) {
      if((instr & (1u << 8))) {
       if((instr & (1u << 6))) {
        return 0;
       } else if((instr & (1u << 5))) {
        return !((instr & (1u << 4)));
       } else {
        return test_arm_instr_995(instr);
       }
      } else {
       return !!((instr & 0x50u) == 0x0u);
      }
     } else {
      return 1;
     }
    } else if((instr & (1u << 21))) {
     if((instr & (1u << 20))) {
      if((instr & (1u << 19))) {
       if((instr & 0x70000u) == 0x10000u) {
        if((instr & 0xf000u) == 0xf000u) {
         return test_arm_instr_1129(instr);
        } else if((instr & 0xe00u) == 0xa00u) {
         if((instr & (1u << 8))) {
          if((instr & 0xc0u) == 0x0u) {
           return test_arm_instr_1084(instr);
          } else {
           return !!((instr & 0x3fu) == 0x30u);
          }
         } else {
          return !!((instr & 0xf0u) == 0x0u);
         }
        } else {
         return 1;
        }
       } else {
        return test_arm_instr_1099(instr);
       }
      } else if((instr & 0x70000u) == 0x50000u) {
       if((instr & 0xf000u) == 0xf000u) {
        return test_arm_instr_1113(instr);
       } else if((instr & 0xe00u) == 0xa00u) {
        if((instr & (1u << 8))) {
         if((instr & (1u << 7))) {
          if((instr & (1u << 6))) {
           return test_arm_instr_1103(instr);
          } else {
           return !!((instr & 0x3fu) == 0x30u);
          }
         } else if((instr & (1u << 6))) {
          return test_arm_instr_1103(instr);
         } else {
          return test_arm_instr_1084(instr);
         }
        } else {
         return test_arm_instr_1102(instr);
        }
       } else {
        return 1;
       }
      } else {
       return test_arm_instr_1099(instr);
      }
     } else if((instr & (1u << 16))) {
      return test_arm_instr_1078(instr);
     } else if((instr & 0xf000u) == 0xf000u) {
      return !((instr & (1u << 4)));
     } else if((instr & 0xe00u) == 0xa00u) {
      if((instr & 0x140u) == 0x100u) {
       return test_arm_instr_995(instr);
      } else {
       return !((instr & (1u << 4)));
      }
     } else {
      return 1;
     }
    } else if((instr & (1u << 20))) {
     if((instr & 0xf000u) == 0xf000u) {
      return !((instr & 0xe10u) == 0xa10u);
     } else if((instr & 0xe00u) == 0xa00u) {
      if((instr & 0x120u) == 0x120u) {
       return test_arm_instr_995(instr);
      } else {
       return !((instr & (1u << 4)));
      }
     } else {
      return 1;
     }
    } else if((instr & 0xf000u) == 0xf000u) {
     return test_arm_instr_1052(instr);
    } else if((instr & 0xe00u) == 0xa00u) {
     if((instr & (1u << 8))) {
      if((instr & (1u << 6))) {
       return 0;
      } else {
       return test_arm_instr_995(instr);
      }
     } else {
      return !!((instr & 0x50u) == 0x0u);
     }
    } else {
     return 1;
    }
   } else if((instr & (1u << 22))) {
    if((instr & (1u << 20))) {
     return test_arm_instr_1041(instr);
    } else if((instr & 0xf000u) == 0xf000u) {
     return !((instr & (1u << 4)));
    } else {
     return test_arm_instr_1033(instr);
    }
   } else if((instr & (1u << 21))) {
    if((instr & (1u << 20))) {
     if((instr & 0xf000u) == 0xf000u) {
      return !((instr & 0xe10u) == 0xa10u);
     } else {
      return test_arm_instr_1019(instr);
     }
    } else if((instr & 0xf000u) == 0xf000u) {
     return !((instr & (1u << 4)));
    } else {
     return test_arm_instr_1019(instr);
    }
   } else if((instr & (1u << 20))) {
    if((instr & 0xf000u) == 0xf000u) {
     return !((instr & 0xe10u) == 0xa10u);
    } else {
     return test_arm_instr_1003(instr);
    }
   } else if((instr & 0xf000u) == 0xf000u) {
    return !((instr & (1u << 4)));
   } else {
    return test_arm_instr_1003(instr);
   }
  } else if((instr & (1u << 24))) {
   if((instr & (1u << 23))) {
    if((instr & (1u << 21))) {
     return test_arm_instr_959(instr);
    } else {
     return !((instr & 0xf0000u) == 0xf0000u);
    }
   } else if((instr & 0x600000u) == 0x600000u) {
    return test_arm_instr_987(instr);
   } else {
    return !((instr & 0xf0000u) == 0xf0000u);
   }
  } else if((instr & (1u << 23))) {
   if((instr & (1u << 22))) {
    return test_arm_instr_987(instr);
   } else {
    return !((instr & 0xf0000u) == 0xf0000u);
   }
  } else if((instr & (1u << 22))) {
   if((instr & (1u << 21))) {
    return test_arm_instr_959(instr);
   } else if((instr & 0xf0000u) == 0xf0000u) {
    return 0;
   } else if((instr & 0xf000u) == 0xf000u) {
    return 0;
   } else if((instr & 0xe00u) == 0xa00u) {
    if((instr & (1u << 8))) {
     return !!((instr & 0xd0u) == 0x10u);
    } else if((instr & 0xc0u) == 0x0u) {
     if((instr & (1u << 5))) {
      return test_arm_instr_22(instr);
     } else {
      return !!((instr & (1u << 4)));
     }
    } else {
     return 0;
    }
   } else {
    return 1;
   }
  } else if((instr & (1u << 21))) {
   return test_arm_instr_959(instr);
  } else {
   return 0;
  }
 } else if((instr & (1u << 25))) {
  return 0;
 } else if((instr & 0xf0000u) == 0xf0000u) {
  return 0;
 } else if((instr & (1u << 15))) {
  return 0;
 } else {
  return !((instr & 0x7fffu) == 0x0u);
 }
}

static int test_arm_instr_319(uint32_t instr) {
 if((instr & (1u << 5))) {
  return !!((instr & (1u << 4)));
 } else {
  return test_arm_instr_22(instr);
 }
}

static int test_arm_instr_325(uint32_t instr) {
 if((instr & (1u << 6))) {
  return test_arm_instr_319(instr);
 } else {
  return 0;
 }
}

static int test_arm_instr_149(uint32_t instr) {
 if((instr & 0x70u) == 0x50u) {
  return !((instr & 0xfu) == 0xfu);
 } else {
  return 0;
 }
}

static int test_arm_instr_76(uint32_t instr) {
 if((instr & 0x30u) == 0x30u) {
  return 1;
 } else {
  return !((instr & 0xfu) == 0xfu);
 }
}

static int test_arm_instr_6(uint32_t instr) {
 if((instr & (1u << 4))) {
  return 0;
 } else {
  return !((instr & 0xfu) == 0xfu);
 }
}

static int test_arm_instr_897(uint32_t instr) {
 if((instr & 0x60u) == 0x40u) {
  return !((instr & 0xfu) == 0xfu);
 } else {
  return test_arm_instr_6(instr);
 }
}

static int test_arm_instr_909(uint32_t instr) {
 if((instr & 0xf0000u) == 0xf0000u) {
  if((instr & 0xf000u) == 0xf000u) {
   return 0;
  } else {
   return test_arm_instr_149(instr);
  }
 } else if((instr & 0xf000u) == 0xf000u) {
  return 0;
 } else {
  return test_arm_instr_897(instr);
 }
}

static int test_arm_instr_926(uint32_t instr) {
 if((instr & 0xf0000u) == 0xf0000u) {
  if((instr & 0xf000u) == 0xf000u) {
   return 0;
  } else {
   return !!((instr & 0x70u) == 0x10u);
  }
 } else if((instr & 0xf000u) == 0xf000u) {
  return 0;
 } else if((instr & 0x60u) == 0x0u) {
  return !((instr & 0x1fu) == 0xfu);
 } else {
  return test_arm_instr_6(instr);
 }
}

static int test_arm_instr_735(uint32_t instr) {
 if((instr & 0xf0000u) == 0xf0000u) {
  return 0;
 } else if((instr & 0xf000u) == 0xf000u) {
  return 0;
 } else {
  return test_arm_instr_6(instr);
 }
}

static int test_arm_instr_841(uint32_t instr) {
 if((instr & 0xf00u) == 0xf00u) {
  return 0;
 } else if((instr & 0xf0u) == 0x10u) {
  return !((instr & 0xfu) == 0xfu);
 } else {
  return 0;
 }
}

static int test_arm_instr_910(uint32_t instr) {
 if((instr & (1u << 21))) {
  return test_arm_instr_909(instr);
 } else if((instr & (1u << 20))) {
  return test_arm_instr_735(instr);
 } else if((instr & 0xf0000u) == 0xf0000u) {
  return 0;
 } else if((instr & 0xf000u) == 0xf000u) {
  return test_arm_instr_841(instr);
 } else if((instr & 0xf00u) == 0xf00u) {
  return test_arm_instr_6(instr);
 } else if((instr & 0xe0u) == 0x0u) {
  return !((instr & 0xfu) == 0xfu);
 } else {
  return test_arm_instr_6(instr);
 }
}

static int test_arm_instr_14(uint32_t instr) {
 if((instr & 0x50u) == 0x50u) {
  return 0;
 } else {
  return !((instr & 0xfu) == 0xfu);
 }
}

static int test_arm_instr_226(uint32_t instr) {
 if((instr & 0xf00u) == 0xf00u) {
  return test_arm_instr_6(instr);
 } else if((instr & 0x90u) == 0x90u) {
  return 0;
 } else {
  return !((instr & 0xfu) == 0xfu);
 }
}

static int test_arm_instr_849(uint32_t instr) {
 if((instr & 0xf0000u) == 0xf0000u) {
  return 0;
 } else if((instr & 0xf000u) == 0xf000u) {
  return test_arm_instr_841(instr);
 } else {
  return test_arm_instr_6(instr);
 }
}

static int test_arm_instr_882(uint32_t instr) {
 if((instr & (1u << 22))) {
  if((instr & (1u << 21))) {
   return test_arm_instr_735(instr);
  } else if((instr & (1u << 20))) {
   if((instr & 0xf0000u) == 0xf0000u) {
    return 0;
   } else if((instr & 0xf000u) == 0xf000u) {
    if((instr & 0xf00u) == 0xf00u) {
     return 0;
    } else if((instr & 0xd0u) == 0x10u) {
     return !((instr & 0xfu) == 0xfu);
    } else {
     return 0;
    }
   } else if((instr & 0xf00u) == 0xf00u) {
    return test_arm_instr_6(instr);
   } else if((instr & (1u << 7))) {
    if((instr & 0x50u) == 0x10u) {
     return 0;
    } else {
     return !((instr & 0xfu) == 0xfu);
    }
   } else {
    return test_arm_instr_14(instr);
   }
  } else if((instr & 0xf0000u) == 0xf0000u) {
   return 0;
  } else if((instr & 0xf000u) == 0xf000u) {
   return 0;
  } else {
   return test_arm_instr_226(instr);
  }
 } else if((instr & (1u << 21))) {
  if((instr & (1u << 20))) {
   return test_arm_instr_849(instr);
  } else {
   return test_arm_instr_735(instr);
  }
 } else if((instr & (1u << 20))) {
  return test_arm_instr_849(instr);
 } else if((instr & 0xf0000u) == 0xf0000u) {
  return 0;
 } else if((instr & 0xf000u) == 0xf000u) {
  if((instr & 0xf00u) == 0xf00u) {
   return 0;
  } else if((instr & 0x90u) == 0x10u) {
   return !((instr & 0xfu) == 0xfu);
  } else {
   return 0;
  }
 } else {
  return test_arm_instr_226(instr);
 }
}

static int test_arm_instr_23(uint32_t instr) {
 if((instr & 0x30u) == 0x10u) {
  return !((instr & 0xfu) == 0xfu);
 } else {
  return 0;
 }
}

static int test_arm_instr_777(uint32_t instr) {
 if((instr & 0xc0u) == 0x40u) {
  return test_arm_instr_22(instr);
 } else {
  return test_arm_instr_23(instr);
 }
}

static int test_arm_instr_47(uint32_t instr) {
 if((instr & 0x30u) == 0x30u) {
  return 0;
 } else {
  return !((instr & 0xfu) == 0xfu);
 }
}

static int test_arm_instr_766(uint32_t instr) {
 if((instr & 0xc0u) == 0x40u) {
  return !((instr & 0xfu) == 0xfu);
 } else {
  return test_arm_instr_47(instr);
 }
}

static int test_arm_instr_809(uint32_t instr) {
 if((instr & (1u << 20))) {
  if((instr & 0xf0000u) == 0xf0000u) {
   if((instr & 0xf000u) == 0xf000u) {
    return 0;
   } else if((instr & 0xf00u) == 0xf00u) {
    if((instr & 0xe0u) == 0xe0u) {
     return 0;
    } else {
     return test_arm_instr_22(instr);
    }
   } else {
    return test_arm_instr_777(instr);
   }
  } else if((instr & 0xf000u) == 0xf000u) {
   return 0;
  } else {
   return test_arm_instr_766(instr);
  }
 } else if((instr & 0xf0000u) == 0xf0000u) {
  if((instr & 0xf000u) == 0xf000u) {
   return 0;
  } else if((instr & 0xf00u) == 0xf00u) {
   if((instr & 0xa0u) == 0xa0u) {
    return 0;
   } else {
    return test_arm_instr_22(instr);
   }
  } else {
   return test_arm_instr_777(instr);
  }
 } else if((instr & 0xf000u) == 0xf000u) {
  return 0;
 } else if((instr & 0xf00u) == 0xf00u) {
  if((instr & 0xb0u) == 0xb0u) {
   return 0;
  } else {
   return !((instr & 0xfu) == 0xfu);
  }
 } else {
  return test_arm_instr_766(instr);
 }
}

static int test_arm_instr_760(uint32_t instr) {
 if((instr & 0xf000u) == 0xf000u) {
  return 0;
 } else if((instr & 0xf0u) == 0x70u) {
  return !((instr & 0xfu) == 0xfu);
 } else {
  return 0;
 }
}

static int test_arm_instr_824(uint32_t instr) {
 if((instr & (1u << 23))) {
  if((instr & (1u << 22))) {
   if((instr & (1u << 21))) {
    return test_arm_instr_809(instr);
   } else if((instr & (1u << 20))) {
    return test_arm_instr_735(instr);
   } else if((instr & 0xf0000u) == 0xf0000u) {
    return test_arm_instr_760(instr);
   } else if((instr & 0xf000u) == 0xf000u) {
    return 0;
   } else if((instr & 0xe0u) == 0x60u) {
    return !((instr & 0xfu) == 0xfu);
   } else {
    return test_arm_instr_6(instr);
   }
  } else if((instr & (1u << 21))) {
   return test_arm_instr_809(instr);
  } else if((instr & (1u << 20))) {
   return test_arm_instr_735(instr);
  } else if((instr & 0xf0000u) == 0xf0000u) {
   return test_arm_instr_760(instr);
  } else if((instr & 0xf000u) == 0xf000u) {
   return 0;
  } else if((instr & (1u << 7))) {
   if((instr & 0x70u) == 0x70u) {
    return 0;
   } else {
    return !((instr & 0xfu) == 0xfu);
   }
  } else if((instr & 0x70u) == 0x30u) {
   return 0;
  } else {
   return !((instr & 0xfu) == 0xfu);
  }
 } else if((instr & 0x300000u) == 0x0u) {
  return test_arm_instr_735(instr);
 } else if((instr & 0xf0000u) == 0xf0000u) {
  return 0;
 } else if((instr & 0xf000u) == 0xf000u) {
  return 0;
 } else if((instr & (1u << 7))) {
  if((instr & (1u << 6))) {
   if((instr & 0x30u) == 0x10u) {
    return 0;
   } else {
    return !((instr & 0xfu) == 0xfu);
   }
  } else {
   return test_arm_instr_47(instr);
  }
 } else {
  return !((instr & 0xfu) == 0xfu);
 }
}

static int test_arm_instr_694(uint32_t instr) {
 if((instr & 0xf0000u) == 0xf0000u) {
  return 0;
 } else {
  return !((instr & 0xf000u) == 0xf000u);
 }
}

static int test_arm_instr_699(uint32_t instr) {
 if((instr & (1u << 20))) {
  return !((instr & 0xf0000u) == 0xf0000u);
 } else {
  return !((instr & 0xf000u) == 0xf000u);
 }
}

static int test_arm_instr_726(uint32_t instr) {
 if((instr & (1u << 24))) {
  if((instr & (1u << 23))) {
   if((instr & (1u << 21))) {
    if((instr & 0xf0000u) == 0x0u) {
     return !((instr & 0xf000u) == 0xf000u);
    } else {
     return 0;
    }
   } else {
    return test_arm_instr_694(instr);
   }
  } else if((instr & (1u << 22))) {
   if((instr & (1u << 21))) {
    if((instr & (1u << 20))) {
     return !((instr & 0xf0000u) == 0xf0000u);
    } else {
     return !!((instr & 0xf000u) == 0xf000u);
    }
   } else {
    return test_arm_instr_699(instr);
   }
  } else if((instr & (1u << 21))) {
   if((instr & (1u << 20))) {
    return !((instr & 0xf0000u) == 0xf0000u);
   } else if((instr & 0xf0000u) == 0x0u) {
    return !!((instr & 0xff00u) == 0xf000u);
   } else {
    return !!((instr & 0xf000u) == 0xf000u);
   }
  } else {
   return test_arm_instr_699(instr);
  }
 } else {
  return test_arm_instr_694(instr);
 }
}

static int test_arm_instr_390(uint32_t instr) {
 if((instr & 0x60u) == 0x0u) {
  return 0;
 } else {
  return !!((instr & (1u << 4)));
 }
}

static int test_arm_instr_641(uint32_t instr) {
 if((instr & (1u << 7))) {
  return test_arm_instr_390(instr);
 } else {
  return 0;
 }
}

static int test_arm_instr_674(uint32_t instr) {
 if((instr & 0xf000u) == 0xf000u) {
  return 0;
 } else if((instr & 0xf00u) == 0xf00u) {
  if((instr & (1u << 7))) {
   if((instr & 0x60u) == 0x0u) {
    return !!((instr & 0x1fu) == 0x1fu);
   } else {
    return !!((instr & (1u << 4)));
   }
  } else {
   return 0;
  }
 } else {
  return test_arm_instr_641(instr);
 }
}

static int test_arm_instr_80(uint32_t instr) {
 if((instr & 0x60u) == 0x0u) {
  return test_arm_instr_6(instr);
 } else {
  return !((instr & 0x1fu) == 0xfu);
 }
}

static int test_arm_instr_102(uint32_t instr) {
 if((instr & (1u << 7))) {
  return test_arm_instr_80(instr);
 } else {
  return !((instr & 0xfu) == 0xfu);
 }
}

static int test_arm_instr_635(uint32_t instr) {
 if((instr & 0xf000u) == 0xf000u) {
  return 0;
 } else if((instr & 0xf00u) == 0xf00u) {
  if((instr & (1u << 7))) {
   if((instr & 0x60u) == 0x0u) {
    if((instr & (1u << 4))) {
     return !!((instr & 0xfu) == 0xfu);
    } else {
     return !((instr & 0xfu) == 0xfu);
    }
   } else {
    return !((instr & 0x1fu) == 0xfu);
   }
  } else {
   return test_arm_instr_6(instr);
  }
 } else {
  return test_arm_instr_102(instr);
 }
}

static int test_arm_instr_329(uint32_t instr) {
 if((instr & 0x60u) == 0x0u) {
  return test_arm_instr_22(instr);
 } else {
  return !!((instr & (1u << 4)));
 }
}

static int test_arm_instr_656(uint32_t instr) {
 if((instr & 0xe000u) == 0xe000u) {
  if((instr & (1u << 12))) {
   return 0;
  } else if((instr & 0xf00u) == 0xf00u) {
   if((instr & 0xc0u) == 0x80u) {
    return test_arm_instr_319(instr);
   } else {
    return 0;
   }
  } else {
   return !!((instr & 0xf0u) == 0xb0u);
  }
 } else if((instr & 0xf00u) == 0xf00u) {
  if((instr & (1u << 7))) {
   return test_arm_instr_329(instr);
  } else {
   return 0;
  }
 } else {
  return test_arm_instr_641(instr);
 }
}

static int test_arm_instr_88(uint32_t instr) {
 if((instr & 0x60u) == 0x20u) {
  return !((instr & 0x1fu) == 0xfu);
 } else {
  return test_arm_instr_6(instr);
 }
}

static int test_arm_instr_77(uint32_t instr) {
 if((instr & 0x60u) == 0x0u) {
  return !((instr & 0xfu) == 0xfu);
 } else {
  return !((instr & 0x1fu) == 0xfu);
 }
}

static int test_arm_instr_620(uint32_t instr) {
 if((instr & 0xe000u) == 0xe000u) {
  if((instr & (1u << 12))) {
   return 0;
  } else if((instr & 0xf00u) == 0xf00u) {
   if((instr & 0xc0u) == 0x80u) {
    return test_arm_instr_76(instr);
   } else {
    return test_arm_instr_6(instr);
   }
  } else if((instr & (1u << 7))) {
   return test_arm_instr_88(instr);
  } else {
   return !((instr & 0xfu) == 0xfu);
  }
 } else if((instr & 0xf00u) == 0xf00u) {
  if((instr & (1u << 7))) {
   return test_arm_instr_77(instr);
  } else {
   return test_arm_instr_6(instr);
  }
 } else {
  return test_arm_instr_102(instr);
 }
}

static int test_arm_instr_524(uint32_t instr) {
 if((instr & (1u << 3))) {
  if((instr & (1u << 2))) {
   return !!((instr & 0x3u) == 0x0u);
  } else {
   return !!((instr & 0x3u) == 0x1u);
  }
 } else {
  return !((instr & (1u << 2)));
 }
}

static int test_arm_instr_571(uint32_t instr) {
 if((instr & (1u << 15))) {
  if((instr & (1u << 14))) {
   if((instr & 0x3ff0u) == 0x0u) {
    return test_arm_instr_524(instr);
   } else {
    return 0;
   }
  } else if((instr & 0x3ff0u) == 0x1000u) {
   return test_arm_instr_524(instr);
  } else {
   return 0;
  }
 } else if((instr & 0x4ff0u) == 0x0u) {
  return test_arm_instr_524(instr);
 } else {
  return 0;
 }
}

static int test_arm_instr_238(uint32_t instr) {
 if((instr & 0x60u) == 0x0u) {
  return 0;
 } else {
  return test_arm_instr_22(instr);
 }
}

static int test_arm_instr_583(uint32_t instr) {
 if((instr & (1u << 7))) {
  if((instr & 0x60u) == 0x0u) {
   return !!((instr & 0x1fu) == 0x1fu);
  } else {
   return test_arm_instr_22(instr);
  }
 } else {
  return 0;
 }
}

static int test_arm_instr_529(uint32_t instr) {
 if((instr & (1u << 7))) {
  return test_arm_instr_238(instr);
 } else {
  return 0;
 }
}

static int test_arm_instr_588(uint32_t instr) {
 if((instr & 0xf00u) == 0xf00u) {
  return test_arm_instr_583(instr);
 } else {
  return test_arm_instr_529(instr);
 }
}

static int test_arm_instr_532(uint32_t instr) {
 if((instr & 0x700u) == 0x0u) {
  if((instr & (1u << 7))) {
   return test_arm_instr_238(instr);
  } else if((instr & 0x70u) == 0x0u) {
   return test_arm_instr_524(instr);
  } else {
   return 0;
  }
 } else {
  return test_arm_instr_529(instr);
 }
}

static int test_arm_instr_587(uint32_t instr) {
 if((instr & (1u << 11))) {
  if((instr & 0x700u) == 0x700u) {
   return test_arm_instr_583(instr);
  } else {
   return test_arm_instr_529(instr);
  }
 } else {
  return test_arm_instr_532(instr);
 }
}

static int test_arm_instr_596(uint32_t instr) {
 if((instr & (1u << 15))) {
  if((instr & (1u << 14))) {
   if((instr & (1u << 13))) {
    if((instr & 0x1080u) == 0x80u) {
     return test_arm_instr_238(instr);
    } else {
     return 0;
    }
   } else if((instr & (1u << 12))) {
    return test_arm_instr_588(instr);
   } else {
    return test_arm_instr_587(instr);
   }
  } else if((instr & 0x3000u) == 0x1000u) {
   return test_arm_instr_587(instr);
  } else {
   return test_arm_instr_588(instr);
  }
 } else if((instr & (1u << 14))) {
  return test_arm_instr_588(instr);
 } else {
  return test_arm_instr_587(instr);
 }
}

static int test_arm_instr_8(uint32_t instr) {
 if((instr & 0x70u) == 0x10u) {
  return 0;
 } else {
  return !((instr & 0xfu) == 0xfu);
 }
}

static int test_arm_instr_9(uint32_t instr) {
 if((instr & (1u << 7))) {
  return test_arm_instr_8(instr);
 } else {
  return test_arm_instr_6(instr);
 }
}

static int test_arm_instr_217(uint32_t instr) {
 if((instr & 0xf0u) == 0x90u) {
  return 0;
 } else {
  return !((instr & 0xfu) == 0xfu);
 }
}

static int test_arm_instr_221(uint32_t instr) {
 if((instr & 0xf00u) == 0xf00u) {
  return test_arm_instr_9(instr);
 } else {
  return test_arm_instr_217(instr);
 }
}

static int test_arm_instr_490(uint32_t instr) {
 if((instr & 0xf00u) == 0xf00u) {
  if((instr & (1u << 7))) {
   if((instr & 0x70u) == 0x10u) {
    return !!((instr & 0xfu) == 0xfu);
   } else {
    return !((instr & 0xfu) == 0xfu);
   }
  } else {
   return test_arm_instr_6(instr);
  }
 } else {
  return test_arm_instr_217(instr);
 }
}

static int test_arm_instr_533(uint32_t instr) {
 if((instr & (1u << 4))) {
  return !((instr & 0xeu) == 0xeu);
 } else {
  return 0;
 }
}

static int test_arm_instr_536(uint32_t instr) {
 if((instr & (1u << 7))) {
  if((instr & 0x60u) == 0x0u) {
   return test_arm_instr_533(instr);
  } else {
   return test_arm_instr_22(instr);
  }
 } else {
  return 0;
 }
}

static int test_arm_instr_541(uint32_t instr) {
 if((instr & 0xf00u) == 0xf00u) {
  return test_arm_instr_536(instr);
 } else {
  return test_arm_instr_529(instr);
 }
}

static int test_arm_instr_540(uint32_t instr) {
 if((instr & (1u << 11))) {
  if((instr & 0x700u) == 0x700u) {
   return test_arm_instr_536(instr);
  } else {
   return test_arm_instr_529(instr);
  }
 } else {
  return test_arm_instr_532(instr);
 }
}

static int test_arm_instr_556(uint32_t instr) {
 if((instr & (1u << 15))) {
  if((instr & (1u << 14))) {
   if((instr & (1u << 13))) {
    if((instr & (1u << 12))) {
     return 0;
    } else if((instr & 0xf00u) == 0xf00u) {
     if((instr & 0xc0u) == 0x80u) {
      if((instr & (1u << 5))) {
       return test_arm_instr_22(instr);
      } else {
       return test_arm_instr_533(instr);
      }
     } else {
      return 0;
     }
    } else if((instr & 0xf0u) == 0xb0u) {
     return !((instr & 0xfu) == 0xfu);
    } else {
     return 0;
    }
   } else if((instr & (1u << 12))) {
    return test_arm_instr_541(instr);
   } else {
    return test_arm_instr_540(instr);
   }
  } else if((instr & 0x3000u) == 0x1000u) {
   return test_arm_instr_540(instr);
  } else {
   return test_arm_instr_541(instr);
  }
 } else if((instr & (1u << 14))) {
  return test_arm_instr_541(instr);
 } else {
  return test_arm_instr_540(instr);
 }
}

static int test_arm_instr_16(uint32_t instr) {
 if((instr & 0x60u) == 0x20u) {
  return !((instr & 0xfu) == 0xfu);
 } else {
  return test_arm_instr_6(instr);
 }
}

static int test_arm_instr_465(uint32_t instr) {
 if((instr & (1u << 7))) {
  return test_arm_instr_16(instr);
 } else {
  return !((instr & 0xfu) == 0xfu);
 }
}

static int test_arm_instr_684(uint32_t instr) {
 if((instr & (1u << 22))) {
  if((instr & (1u << 21))) {
   if((instr & (1u << 20))) {
    if((instr & (1u << 19))) {
     if((instr & 0x70000u) == 0x70000u) {
      return 0;
     } else {
      return test_arm_instr_674(instr);
     }
    } else if((instr & 0x70000u) == 0x0u) {
     return test_arm_instr_635(instr);
    } else {
     return test_arm_instr_674(instr);
    }
   } else if((instr & (1u << 19))) {
    if((instr & 0x70000u) == 0x70000u) {
     return 0;
    } else {
     return test_arm_instr_656(instr);
    }
   } else if((instr & 0x70000u) == 0x0u) {
    return test_arm_instr_620(instr);
   } else {
    return test_arm_instr_656(instr);
   }
  } else if((instr & (1u << 20))) {
   if((instr & 0xf0000u) == 0xf0000u) {
    return 0;
   } else {
    return test_arm_instr_635(instr);
   }
  } else if((instr & 0xf0000u) == 0xf0000u) {
   return 0;
  } else {
   return test_arm_instr_620(instr);
  }
 } else if((instr & (1u << 21))) {
  if((instr & (1u << 20))) {
   if((instr & (1u << 19))) {
    if((instr & 0x70000u) == 0x70000u) {
     return test_arm_instr_571(instr);
    } else {
     return test_arm_instr_596(instr);
    }
   } else if((instr & 0x70000u) == 0x0u) {
    if((instr & 0xe000u) == 0xe000u) {
     if((instr & (1u << 12))) {
      return 0;
     } else {
      return test_arm_instr_221(instr);
     }
    } else {
     return test_arm_instr_490(instr);
    }
   } else {
    return test_arm_instr_596(instr);
   }
  } else if((instr & (1u << 19))) {
   if((instr & 0x70000u) == 0x70000u) {
    return test_arm_instr_571(instr);
   } else {
    return test_arm_instr_556(instr);
   }
  } else if((instr & 0x70000u) == 0x0u) {
   if((instr & 0xe000u) == 0xe000u) {
    if((instr & (1u << 12))) {
     return 0;
    } else if((instr & 0xf00u) == 0xf00u) {
     if((instr & 0xc0u) == 0x80u) {
      if((instr & 0x30u) == 0x10u) {
       return !((instr & 0xeu) == 0xeu);
      } else {
       return !((instr & 0xfu) == 0xfu);
      }
     } else {
      return test_arm_instr_6(instr);
     }
    } else {
     return test_arm_instr_465(instr);
    }
   } else if((instr & 0xf00u) == 0xf00u) {
    if((instr & (1u << 7))) {
     if((instr & 0x70u) == 0x10u) {
      return !((instr & 0xeu) == 0xeu);
     } else {
      return !((instr & 0xfu) == 0xfu);
     }
    } else {
     return test_arm_instr_6(instr);
    }
   } else {
    return test_arm_instr_217(instr);
   }
  } else {
   return test_arm_instr_556(instr);
  }
 } else if((instr & (1u << 20))) {
  if((instr & 0xf0000u) == 0xf0000u) {
   return 0;
  } else if((instr & 0xf000u) == 0xf000u) {
   return 0;
  } else {
   return test_arm_instr_490(instr);
  }
 } else if((instr & 0xf0000u) == 0xf0000u) {
  return 0;
 } else if((instr & 0xe000u) == 0xe000u) {
  if((instr & (1u << 12))) {
   return 0;
  } else if((instr & 0xf00u) == 0xf00u) {
   if((instr & 0xc0u) == 0x80u) {
    return !((instr & 0xfu) == 0xfu);
   } else {
    return test_arm_instr_6(instr);
   }
  } else {
   return test_arm_instr_465(instr);
  }
 } else if((instr & 0xf00u) == 0xf00u) {
  if((instr & 0x90u) == 0x10u) {
   return 0;
  } else {
   return !((instr & 0xfu) == 0xfu);
  }
 } else {
  return test_arm_instr_217(instr);
 }
}

static int test_arm_instr_81(uint32_t instr) {
 if((instr & (1u << 7))) {
  return test_arm_instr_80(instr);
 } else {
  return test_arm_instr_6(instr);
 }
}

static int test_arm_instr_106(uint32_t instr) {
 if((instr & 0xf00u) == 0xf00u) {
  return test_arm_instr_81(instr);
 } else {
  return test_arm_instr_102(instr);
 }
}

static int test_arm_instr_377(uint32_t instr) {
 if((instr & 0xf0000u) == 0xf0000u) {
  return 0;
 } else if((instr & 0xf000u) == 0xf000u) {
  return test_arm_instr_226(instr);
 } else {
  return test_arm_instr_106(instr);
 }
}

static int test_arm_instr_297(uint32_t instr) {
 if((instr & 0x1f0u) == 0x0u) {
  return !((instr & 0xfu) == 0xfu);
 } else {
  return 0;
 }
}

static int test_arm_instr_408(uint32_t instr) {
 if((instr & 0x90u) == 0x80u) {
  return !((instr & 0xfu) == 0xfu);
 } else {
  return 0;
 }
}

static int test_arm_instr_416(uint32_t instr) {
 if((instr & 0x700u) == 0x700u) {
  return 0;
 } else {
  return test_arm_instr_408(instr);
 }
}

static int test_arm_instr_254(uint32_t instr) {
 if((instr & 0x70u) == 0x0u) {
  return !((instr & 0xfu) == 0xfu);
 } else {
  return 0;
 }
}

static int test_arm_instr_407(uint32_t instr) {
 if((instr & (1u << 7))) {
  return test_arm_instr_6(instr);
 } else {
  return test_arm_instr_254(instr);
 }
}

static int test_arm_instr_406(uint32_t instr) {
 if((instr & 0xf00u) == 0xf00u) {
  if((instr & (1u << 7))) {
   return !!((instr & 0x70u) == 0x30u);
  } else {
   return test_arm_instr_149(instr);
  }
 } else if((instr & (1u << 7))) {
  return test_arm_instr_88(instr);
 } else {
  return test_arm_instr_149(instr);
 }
}

static int test_arm_instr_391(uint32_t instr) {
 if((instr & (1u << 7))) {
  return test_arm_instr_390(instr);
 } else {
  return test_arm_instr_149(instr);
 }
}

static int test_arm_instr_386(uint32_t instr) {
 if((instr & (1u << 7))) {
  return test_arm_instr_80(instr);
 } else {
  return test_arm_instr_149(instr);
 }
}

static int test_arm_instr_396(uint32_t instr) {
 if((instr & 0xf00u) == 0xf00u) {
  return test_arm_instr_391(instr);
 } else {
  return test_arm_instr_386(instr);
 }
}

static int test_arm_instr_410(uint32_t instr) {
 if((instr & (1u << 7))) {
  return test_arm_instr_6(instr);
 } else {
  return !!((instr & 0x70u) == 0x0u);
 }
}

static int test_arm_instr_409(uint32_t instr) {
 if((instr & (1u << 8))) {
  return test_arm_instr_408(instr);
 } else {
  return test_arm_instr_407(instr);
 }
}

static int test_arm_instr_428(uint32_t instr) {
 if((instr & 0xe000u) == 0xe000u) {
  if((instr & (1u << 12))) {
   if((instr & (1u << 11))) {
    return test_arm_instr_416(instr);
   } else if((instr & (1u << 10))) {
    return test_arm_instr_408(instr);
   } else if((instr & (1u << 9))) {
    return test_arm_instr_410(instr);
   } else {
    return test_arm_instr_409(instr);
   }
  } else {
   return test_arm_instr_406(instr);
  }
 } else {
  return test_arm_instr_396(instr);
 }
}

static int test_arm_instr_418(uint32_t instr) {
 if((instr & (1u << 12))) {
  if((instr & (1u << 11))) {
   return test_arm_instr_416(instr);
  } else if((instr & (1u << 10))) {
   return test_arm_instr_408(instr);
  } else if((instr & (1u << 9))) {
   if((instr & (1u << 8))) {
    return test_arm_instr_407(instr);
   } else {
    return test_arm_instr_410(instr);
   }
  } else {
   return test_arm_instr_409(instr);
  }
 } else {
  return test_arm_instr_406(instr);
 }
}

static int test_arm_instr_431(uint32_t instr) {
 if((instr & (1u << 16))) {
  return test_arm_instr_428(instr);
 } else if((instr & 0xe000u) == 0xe000u) {
  return test_arm_instr_418(instr);
 } else {
  return test_arm_instr_396(instr);
 }
}

static int test_arm_instr_354(uint32_t instr) {
 if((instr & (1u << 6))) {
  return !!((instr & 0x30u) == 0x30u);
 } else {
  return !!((instr & 0x30u) == 0x0u);
 }
}

static int test_arm_instr_355(uint32_t instr) {
 if((instr & (1u << 7))) {
  return 0;
 } else {
  return test_arm_instr_354(instr);
 }
}

static int test_arm_instr_86(uint32_t instr) {
 if((instr & (1u << 6))) {
  return test_arm_instr_6(instr);
 } else {
  return test_arm_instr_76(instr);
 }
}

static int test_arm_instr_338(uint32_t instr) {
 if((instr & (1u << 7))) {
  return test_arm_instr_86(instr);
 } else {
  return test_arm_instr_325(instr);
 }
}

static int test_arm_instr_320(uint32_t instr) {
 if((instr & (1u << 6))) {
  return test_arm_instr_319(instr);
 } else {
  return !!((instr & 0x30u) == 0x0u);
 }
}

static int test_arm_instr_335(uint32_t instr) {
 if((instr & (1u << 7))) {
  return test_arm_instr_86(instr);
 } else {
  return test_arm_instr_320(instr);
 }
}

static int test_arm_instr_339(uint32_t instr) {
 if((instr & (1u << 9))) {
  return test_arm_instr_338(instr);
 } else {
  return test_arm_instr_335(instr);
 }
}

static int test_arm_instr_322(uint32_t instr) {
 if((instr & (1u << 6))) {
  return test_arm_instr_319(instr);
 } else {
  return !!((instr & 0x3fu) == 0x0u);
 }
}

static int test_arm_instr_326(uint32_t instr) {
 if((instr & (1u << 7))) {
  return test_arm_instr_77(instr);
 } else {
  return test_arm_instr_325(instr);
 }
}

static int test_arm_instr_321(uint32_t instr) {
 if((instr & (1u << 7))) {
  return test_arm_instr_77(instr);
 } else {
  return test_arm_instr_320(instr);
 }
}

static int test_arm_instr_327(uint32_t instr) {
 if((instr & (1u << 9))) {
  return test_arm_instr_326(instr);
 } else {
  return test_arm_instr_321(instr);
 }
}

static int test_arm_instr_458(uint32_t instr) {
 if((instr & (1u << 21))) {
  if((instr & (1u << 20))) {
   return test_arm_instr_377(instr);
  } else if((instr & (1u << 19))) {
   if((instr & (1u << 18))) {
    if((instr & (1u << 17))) {
     if((instr & (1u << 16))) {
      if((instr & 0xf000u) == 0xf000u) {
       if((instr & 0xc00u) == 0x0u) {
        if((instr & (1u << 9))) {
         return !!((instr & 0xf0u) == 0x0u);
        } else {
         return test_arm_instr_297(instr);
        }
       } else {
        return 0;
       }
      } else if((instr & 0xff0u) == 0xf10u) {
       return !((instr & 0xfu) == 0xfu);
      } else {
       return 0;
      }
     } else if((instr & 0xe000u) == 0xe000u) {
      if((instr & (1u << 12))) {
       if((instr & (1u << 11))) {
        return test_arm_instr_416(instr);
       } else if((instr & (1u << 10))) {
        return test_arm_instr_408(instr);
       } else if((instr & 0x300u) == 0x100u) {
        return test_arm_instr_408(instr);
       } else {
        return test_arm_instr_407(instr);
       }
      } else {
       return test_arm_instr_406(instr);
      }
     } else {
      return test_arm_instr_396(instr);
     }
    } else {
     return test_arm_instr_431(instr);
    }
   } else {
    return test_arm_instr_428(instr);
   }
  } else if((instr & 0x60000u) == 0x0u) {
   if((instr & (1u << 16))) {
    return test_arm_instr_428(instr);
   } else if((instr & (1u << 15))) {
    if((instr & 0x6000u) == 0x6000u) {
     return test_arm_instr_418(instr);
    } else {
     return test_arm_instr_396(instr);
    }
   } else if((instr & 0x7000u) == 0x0u) {
    if((instr & (1u << 11))) {
     if((instr & 0x700u) == 0x700u) {
      return test_arm_instr_391(instr);
     } else {
      return test_arm_instr_386(instr);
     }
    } else if((instr & 0x700u) == 0x0u) {
     if((instr & (1u << 7))) {
      return test_arm_instr_80(instr);
     } else if((instr & (1u << 6))) {
      if((instr & (1u << 5))) {
       if((instr & (1u << 4))) {
        return 1;
       } else {
        return !!((instr & 0xfu) == 0xeu);
       }
      } else {
       return test_arm_instr_22(instr);
      }
     } else {
      return 0;
     }
    } else {
     return test_arm_instr_386(instr);
    }
   } else {
    return test_arm_instr_396(instr);
   }
  } else {
   return test_arm_instr_431(instr);
  }
 } else if((instr & (1u << 20))) {
  return test_arm_instr_377(instr);
 } else if((instr & 0xf0000u) == 0xf0000u) {
  if((instr & 0xf000u) == 0xf000u) {
   return !!((instr & 0xf0u) == 0x70u);
  } else if((instr & 0xc00u) == 0x0u) {
   if((instr & (1u << 9))) {
    if((instr & (1u << 7))) {
     return 0;
    } else if((instr & (1u << 6))) {
     return !!((instr & 0x30u) == 0x30u);
    } else {
     return !!((instr & 0x3fu) == 0x0u);
    }
   } else {
    return test_arm_instr_355(instr);
   }
  } else if((instr & (1u << 9))) {
   return !!((instr & 0xf0u) == 0x70u);
  } else {
   return test_arm_instr_355(instr);
  }
 } else if((instr & 0xe000u) == 0xe000u) {
  if((instr & (1u << 12))) {
   return !!((instr & 0xf0u) == 0x70u);
  } else if((instr & (1u << 11))) {
   if((instr & (1u << 10))) {
    if((instr & (1u << 9))) {
     if((instr & (1u << 8))) {
      if((instr & (1u << 7))) {
       if((instr & (1u << 6))) {
        return 0;
       } else {
        return test_arm_instr_319(instr);
       }
      } else {
       return test_arm_instr_325(instr);
      }
     } else {
      return test_arm_instr_338(instr);
     }
    } else {
     return test_arm_instr_335(instr);
    }
   } else {
    return test_arm_instr_339(instr);
   }
  } else if((instr & (1u << 10))) {
   return test_arm_instr_339(instr);
  } else if((instr & (1u << 9))) {
   if((instr & (1u << 7))) {
    return test_arm_instr_86(instr);
   } else {
    return test_arm_instr_322(instr);
   }
  } else {
   return test_arm_instr_335(instr);
  }
 } else if((instr & (1u << 11))) {
  if((instr & (1u << 10))) {
   if((instr & (1u << 9))) {
    if((instr & (1u << 8))) {
     if((instr & (1u << 7))) {
      return test_arm_instr_329(instr);
     } else {
      return test_arm_instr_325(instr);
     }
    } else {
     return test_arm_instr_326(instr);
    }
   } else {
    return test_arm_instr_321(instr);
   }
  } else {
   return test_arm_instr_327(instr);
  }
 } else if((instr & (1u << 10))) {
  return test_arm_instr_327(instr);
 } else if((instr & (1u << 9))) {
  if((instr & (1u << 7))) {
   return test_arm_instr_77(instr);
  } else {
   return test_arm_instr_322(instr);
  }
 } else {
  return test_arm_instr_321(instr);
 }
}

static int test_arm_instr_234(uint32_t instr) {
 if((instr & 0xf0000u) == 0xf0000u) {
  return 0;
 } else if((instr & 0xf000u) == 0xf000u) {
  return test_arm_instr_226(instr);
 } else {
  return test_arm_instr_221(instr);
 }
}

static int test_arm_instr_301(uint32_t instr) {
 if((instr & 0x30u) == 0x0u) {
  return 0;
 } else {
  return !((instr & 0xfu) == 0xfu);
 }
}

static int test_arm_instr_1271(uint32_t instr) {
 if((instr & (1u << 6))) {
  return !!((instr & 0x30u) == 0x30u);
 } else if((instr & 0x30u) == 0x0u) {
  return !((instr & 0xfu) == 0xfu);
 } else {
  return 0;
 }
}

static int test_arm_instr_1313(uint32_t instr) {
 if((instr & (1u << 7))) {
  return 0;
 } else {
  return test_arm_instr_1271(instr);
 }
}

static int test_arm_instr_251(uint32_t instr) {
 if((instr & 0x30u) == 0x20u) {
  return !((instr & 0xfu) == 0xfu);
 } else {
  return 0;
 }
}

static int test_arm_instr_1270(uint32_t instr) {
 if((instr & (1u << 7))) {
  return test_arm_instr_251(instr);
 } else {
  return !!((instr & 0x70u) == 0x70u);
 }
}

static int test_arm_instr_1277(uint32_t instr) {
 if((instr & 0x700u) == 0x700u) {
  return !!((instr & 0xf0u) == 0x70u);
 } else {
  return test_arm_instr_1270(instr);
 }
}

static int test_arm_instr_1272(uint32_t instr) {
 if((instr & (1u << 7))) {
  return test_arm_instr_251(instr);
 } else {
  return test_arm_instr_1271(instr);
 }
}

static int test_arm_instr_245(uint32_t instr) {
 if((instr & 0x70u) == 0x30u) {
  return !((instr & 0xfu) == 0xfu);
 } else {
  return 0;
 }
}

static int test_arm_instr_1269(uint32_t instr) {
 if((instr & 0xf00u) == 0xf00u) {
  if((instr & (1u << 7))) {
   return test_arm_instr_245(instr);
  } else {
   return test_arm_instr_325(instr);
  }
 } else if((instr & (1u << 7))) {
  return test_arm_instr_16(instr);
 } else {
  return test_arm_instr_325(instr);
 }
}

static int test_arm_instr_1263(uint32_t instr) {
 if((instr & 0xf00u) == 0xf00u) {
  if((instr & (1u << 7))) {
   return test_arm_instr_238(instr);
  } else {
   return test_arm_instr_325(instr);
  }
 } else if((instr & (1u << 7))) {
  return test_arm_instr_8(instr);
 } else {
  return test_arm_instr_325(instr);
 }
}

static int test_arm_instr_1290(uint32_t instr) {
 if((instr & 0xe000u) == 0xe000u) {
  if((instr & (1u << 12))) {
   if((instr & (1u << 11))) {
    return test_arm_instr_1277(instr);
   } else if((instr & (1u << 10))) {
    return test_arm_instr_1270(instr);
   } else if((instr & 0x300u) == 0x100u) {
    return test_arm_instr_1270(instr);
   } else {
    return test_arm_instr_1272(instr);
   }
  } else {
   return test_arm_instr_1269(instr);
  }
 } else {
  return test_arm_instr_1263(instr);
 }
}

static int test_arm_instr_1293(uint32_t instr) {
 if((instr & (1u << 7))) {
  return test_arm_instr_251(instr);
 } else {
  return test_arm_instr_354(instr);
 }
}

static int test_arm_instr_1283(uint32_t instr) {
 if((instr & (1u << 8))) {
  return test_arm_instr_1270(instr);
 } else {
  return test_arm_instr_1272(instr);
 }
}

static int test_arm_instr_170(uint32_t instr) {
 if((instr & (1u << 7))) {
  if((instr & 0x50u) == 0x10u) {
   return !((instr & 0xfu) == 0xfu);
  } else {
   return 0;
  }
 } else {
  return test_arm_instr_149(instr);
 }
}

static int test_arm_instr_165(uint32_t instr) {
 if((instr & (1u << 7))) {
  return test_arm_instr_14(instr);
 } else {
  return test_arm_instr_149(instr);
 }
}

static int test_arm_instr_156(uint32_t instr) {
 if((instr & (1u << 6))) {
  return test_arm_instr_23(instr);
 } else {
  return !!((instr & 0x3fu) == 0x0u);
 }
}

static int test_arm_instr_166(uint32_t instr) {
 if((instr & (1u << 7))) {
  return test_arm_instr_14(instr);
 } else {
  return test_arm_instr_156(instr);
 }
}

static int test_arm_instr_160(uint32_t instr) {
 if((instr & (1u << 7))) {
  return test_arm_instr_22(instr);
 } else {
  return test_arm_instr_149(instr);
 }
}

static int test_arm_instr_150(uint32_t instr) {
 if((instr & (1u << 7))) {
  return !((instr & 0xfu) == 0xfu);
 } else {
  return test_arm_instr_149(instr);
 }
}

static int test_arm_instr_157(uint32_t instr) {
 if((instr & (1u << 7))) {
  return !((instr & 0xfu) == 0xfu);
 } else {
  return test_arm_instr_156(instr);
 }
}

static int test_arm_instr_178(uint32_t instr) {
 if((instr & 0xe000u) == 0xe000u) {
  if((instr & (1u << 12))) {
   return 0;
  } else if((instr & (1u << 11))) {
   if((instr & 0x700u) == 0x700u) {
    return test_arm_instr_170(instr);
   } else {
    return test_arm_instr_165(instr);
   }
  } else if((instr & 0x600u) == 0x200u) {
   return test_arm_instr_166(instr);
  } else {
   return test_arm_instr_165(instr);
  }
 } else if((instr & (1u << 11))) {
  if((instr & 0x700u) == 0x700u) {
   return test_arm_instr_160(instr);
  } else {
   return test_arm_instr_150(instr);
  }
 } else if((instr & 0x600u) == 0x200u) {
  return test_arm_instr_157(instr);
 } else {
  return test_arm_instr_150(instr);
 }
}

static int test_arm_instr_181(uint32_t instr) {
 if((instr & (1u << 6))) {
  return test_arm_instr_23(instr);
 } else {
  return !!((instr & 0x30u) == 0x0u);
 }
}

static int test_arm_instr_189(uint32_t instr) {
 if((instr & (1u << 7))) {
  return test_arm_instr_14(instr);
 } else {
  return test_arm_instr_181(instr);
 }
}

static int test_arm_instr_191(uint32_t instr) {
 if((instr & (1u << 9))) {
  return test_arm_instr_165(instr);
 } else {
  return test_arm_instr_189(instr);
 }
}

static int test_arm_instr_182(uint32_t instr) {
 if((instr & (1u << 7))) {
  return !((instr & 0xfu) == 0xfu);
 } else {
  return test_arm_instr_181(instr);
 }
}

static int test_arm_instr_184(uint32_t instr) {
 if((instr & (1u << 9))) {
  return test_arm_instr_150(instr);
 } else {
  return test_arm_instr_182(instr);
 }
}

static int test_arm_instr_235(uint32_t instr) {
 if((instr & (1u << 20))) {
  return test_arm_instr_234(instr);
 } else if((instr & 0xe0000u) == 0xe0000u) {
  if((instr & (1u << 16))) {
   if((instr & 0xf000u) == 0xf000u) {
    return 0;
   } else if((instr & 0xc00u) == 0x0u) {
    if((instr & (1u << 9))) {
     return !!((instr & 0xffu) == 0x0u);
    } else {
     return !!((instr & 0xf0u) == 0x0u);
    }
   } else {
    return !!((instr & 0x2f0u) == 0x0u);
   }
  } else {
   return test_arm_instr_178(instr);
  }
 } else if((instr & (1u << 16))) {
  if((instr & 0xe000u) == 0xe000u) {
   if((instr & (1u << 12))) {
    return 0;
   } else if((instr & (1u << 11))) {
    if((instr & (1u << 10))) {
     if((instr & (1u << 9))) {
      if((instr & (1u << 8))) {
       return test_arm_instr_170(instr);
      } else {
       return test_arm_instr_165(instr);
      }
     } else {
      return test_arm_instr_189(instr);
     }
    } else {
     return test_arm_instr_191(instr);
    }
   } else if((instr & (1u << 10))) {
    return test_arm_instr_191(instr);
   } else if((instr & (1u << 9))) {
    return test_arm_instr_166(instr);
   } else {
    return test_arm_instr_189(instr);
   }
  } else if((instr & (1u << 11))) {
   if((instr & (1u << 10))) {
    if((instr & (1u << 9))) {
     if((instr & (1u << 8))) {
      return test_arm_instr_160(instr);
     } else {
      return test_arm_instr_150(instr);
     }
    } else {
     return test_arm_instr_182(instr);
    }
   } else {
    return test_arm_instr_184(instr);
   }
  } else if((instr & (1u << 10))) {
   return test_arm_instr_184(instr);
  } else if((instr & (1u << 9))) {
   return test_arm_instr_157(instr);
  } else {
   return test_arm_instr_182(instr);
  }
 } else {
  return test_arm_instr_178(instr);
 }
}

static int test_arm_instr_85(uint32_t instr) {
 if((instr & 0xf00u) == 0xf00u) {
  return test_arm_instr_81(instr);
 } else if((instr & (1u << 7))) {
  return test_arm_instr_77(instr);
 } else {
  return !((instr & 0xfu) == 0xfu);
 }
}

static int test_arm_instr_143(uint32_t instr) {
 if((instr & 0xf0000u) == 0xf0000u) {
  return 0;
 } else if((instr & 0xf000u) == 0xf000u) {
  return 0;
 } else {
  return test_arm_instr_85(instr);
 }
}

static int test_arm_instr_93(uint32_t instr) {
 if((instr & 0xf00u) == 0xf00u) {
  if((instr & 0xe0u) == 0xa0u) {
   return !((instr & 0x1fu) == 0xfu);
  } else {
   return test_arm_instr_6(instr);
  }
 } else if((instr & (1u << 7))) {
  return test_arm_instr_86(instr);
 } else {
  return !((instr & 0xfu) == 0xfu);
 }
}

static int test_arm_instr_122(uint32_t instr) {
 if((instr & 0xf0000u) == 0xf0000u) {
  return 0;
 } else if((instr & 0xf000u) == 0xf000u) {
  return 0;
 } else {
  return test_arm_instr_93(instr);
 }
}

static int test_arm_instr_101(uint32_t instr) {
 if((instr & 0xf0000u) == 0xf0000u) {
  return 0;
 } else if((instr & 0xe000u) == 0xe000u) {
  if((instr & (1u << 12))) {
   return 0;
  } else {
   return test_arm_instr_93(instr);
  }
 } else {
  return test_arm_instr_85(instr);
 }
}

static int test_arm_instr_13(uint32_t instr) {
 if((instr & 0xf00u) == 0xf00u) {
  return test_arm_instr_9(instr);
 } else {
  return !((instr & 0xfu) == 0xfu);
 }
}

static int test_arm_instr_72(uint32_t instr) {
 if((instr & 0xf0000u) == 0xf0000u) {
  return 0;
 } else if((instr & 0xf000u) == 0xf000u) {
  return 0;
 } else {
  return test_arm_instr_13(instr);
 }
}

static int test_arm_instr_49(uint32_t instr) {
 if((instr & (1u << 7))) {
  if((instr & 0x60u) == 0x0u) {
   return !((instr & 0xfu) == 0xfu);
  } else {
   return test_arm_instr_6(instr);
  }
 } else {
  return !((instr & 0xfu) == 0xfu);
 }
}

static int test_arm_instr_15(uint32_t instr) {
 if((instr & 0xd0u) == 0xd0u) {
  return 0;
 } else {
  return !((instr & 0xfu) == 0xfu);
 }
}

static int test_arm_instr_73(uint32_t instr) {
 if((instr & (1u << 20))) {
  return test_arm_instr_72(instr);
 } else if((instr & 0xf0000u) == 0xf0000u) {
  return 0;
 } else if((instr & 0xf000u) == 0xf000u) {
  return 0;
 } else if((instr & (1u << 11))) {
  if((instr & 0x700u) == 0x700u) {
   return test_arm_instr_6(instr);
  } else {
   return test_arm_instr_49(instr);
  }
 } else if((instr & 0x700u) == 0x0u) {
  return test_arm_instr_15(instr);
 } else {
  return test_arm_instr_49(instr);
 }
}

static int test_arm_instr_21(uint32_t instr) {
 if((instr & 0xf00u) == 0xf00u) {
  if((instr & 0xe0u) == 0xa0u) {
   return !((instr & 0xfu) == 0xfu);
  } else {
   return test_arm_instr_6(instr);
  }
 } else {
  return test_arm_instr_15(instr);
 }
}

static int test_arm_instr_114(uint32_t instr) {
 if((instr & 0xf0000u) == 0xf0000u) {
  return 0;
 } else if((instr & 0xf000u) == 0xf000u) {
  return 0;
 } else {
  return test_arm_instr_106(instr);
 }
}

static int test_arm_instr_29(uint32_t instr) {
 if((instr & 0xf00u) == 0xf00u) {
  return 0;
 } else if((instr & 0xf0u) == 0x90u) {
  return !((instr & 0xfu) == 0xfu);
 } else {
  return 0;
 }
}

static int test_arm_instr_148(uint32_t instr) {
 if((instr & (1u << 23))) {
  if((instr & (1u << 22))) {
   if((instr & (1u << 21))) {
    if((instr & (1u << 20))) {
     return test_arm_instr_143(instr);
    } else {
     return test_arm_instr_122(instr);
    }
   } else if((instr & (1u << 20))) {
    return test_arm_instr_143(instr);
   } else {
    return test_arm_instr_101(instr);
   }
  } else if((instr & (1u << 21))) {
   return test_arm_instr_73(instr);
  } else if((instr & (1u << 20))) {
   return test_arm_instr_72(instr);
  } else if((instr & 0xf0000u) == 0xf0000u) {
   return 0;
  } else if((instr & 0xe000u) == 0xe000u) {
   if((instr & (1u << 12))) {
    return 0;
   } else {
    return test_arm_instr_21(instr);
   }
  } else {
   return test_arm_instr_13(instr);
  }
 } else if((instr & (1u << 22))) {
  if((instr & (1u << 21))) {
   if((instr & (1u << 20))) {
    return test_arm_instr_114(instr);
   } else {
    return test_arm_instr_122(instr);
   }
  } else if((instr & (1u << 20))) {
   return test_arm_instr_114(instr);
  } else {
   return test_arm_instr_101(instr);
  }
 } else if((instr & (1u << 21))) {
  return test_arm_instr_73(instr);
 } else if((instr & (1u << 20))) {
  if((instr & 0xf0000u) == 0xf0000u) {
   return 0;
  } else if((instr & 0xf000u) == 0xf000u) {
   return test_arm_instr_29(instr);
  } else {
   return test_arm_instr_13(instr);
  }
 } else if((instr & 0xf0000u) == 0xf0000u) {
  return 0;
 } else if((instr & 0xe000u) == 0xe000u) {
  if((instr & (1u << 12))) {
   return test_arm_instr_29(instr);
  } else {
   return test_arm_instr_21(instr);
  }
 } else {
  return test_arm_instr_13(instr);
 }
}

static int test_arm_instr_252(uint32_t instr) {
 if((instr & 0xb0u) == 0xa0u) {
  return !((instr & 0xfu) == 0xfu);
 } else {
  return 0;
 }
}

static int test_arm_instr_260(uint32_t instr) {
 if((instr & 0x700u) == 0x700u) {
  return 0;
 } else {
  return test_arm_instr_252(instr);
 }
}

static int test_arm_instr_255(uint32_t instr) {
 if((instr & (1u << 7))) {
  return test_arm_instr_251(instr);
 } else {
  return test_arm_instr_254(instr);
 }
}

static int test_arm_instr_250(uint32_t instr) {
 if((instr & 0xf00u) == 0xf00u) {
  if((instr & (1u << 7))) {
   return test_arm_instr_245(instr);
  } else {
   return test_arm_instr_149(instr);
  }
 } else if((instr & (1u << 7))) {
  return test_arm_instr_16(instr);
 } else {
  return test_arm_instr_149(instr);
 }
}

static int test_arm_instr_243(uint32_t instr) {
 if((instr & 0xf00u) == 0xf00u) {
  if((instr & (1u << 7))) {
   return test_arm_instr_238(instr);
  } else {
   return test_arm_instr_149(instr);
  }
 } else if((instr & (1u << 7))) {
  return test_arm_instr_8(instr);
 } else {
  return test_arm_instr_149(instr);
 }
}

static int test_arm_instr_273(uint32_t instr) {
 if((instr & 0xe000u) == 0xe000u) {
  if((instr & (1u << 12))) {
   if((instr & (1u << 11))) {
    return test_arm_instr_260(instr);
   } else if((instr & (1u << 10))) {
    return test_arm_instr_252(instr);
   } else if((instr & 0x300u) == 0x100u) {
    return test_arm_instr_252(instr);
   } else {
    return test_arm_instr_255(instr);
   }
  } else {
   return test_arm_instr_250(instr);
  }
 } else {
  return test_arm_instr_243(instr);
 }
}

static int test_arm_instr_276(uint32_t instr) {
 if((instr & (1u << 7))) {
  return test_arm_instr_251(instr);
 } else {
  return !!((instr & 0x70u) == 0x0u);
 }
}

static int test_arm_instr_266(uint32_t instr) {
 if((instr & (1u << 8))) {
  return test_arm_instr_252(instr);
 } else {
  return test_arm_instr_255(instr);
 }
}

static int test_arm_instr(uint32_t instr) {
 if((instr & 0xe0000000u) == 0xe0000000u) {
  if((instr & (1u << 28))) {
   if((instr & (1u << 27))) {
    if((instr & (1u << 26))) {
     if((instr & (1u << 25))) {
      if((instr & (1u << 24))) {
       return 0;
      } else if((instr & (1u << 23))) {
       if((instr & (1u << 21))) {
        if((instr & (1u << 20))) {
         if((instr & (1u << 19))) {
          if((instr & (1u << 18))) {
           if((instr & 0xe00u) == 0xa00u) {
            return !!((instr & 0x50u) == 0x40u);
           } else {
            return 1;
           }
          } else if((instr & 0xe00u) == 0xa00u) {
           return !!((instr & 0xd0u) == 0x40u);
          } else {
           return 1;
          }
         } else {
          return !((instr & 0xe00u) == 0xa00u);
         }
        } else if((instr & 0xf000u) == 0xf000u) {
         if((instr & 0xe00u) == 0xa00u) {
          return 0;
         } else {
          return !((instr & (1u << 4)));
         }
        } else {
         return !((instr & 0xe00u) == 0xa00u);
        }
       } else if((instr & (1u << 20))) {
        return !((instr & 0xe00u) == 0xa00u);
       } else {
        return test_arm_instr_1078(instr);
       }
      } else if((instr & 0x10f000u) == 0xf000u) {
       return test_arm_instr_1052(instr);
      } else if((instr & 0xe00u) == 0xa00u) {
       return !!((instr & 0x50u) == 0x0u);
      } else {
       return 1;
      }
     } else if((instr & 0x1800000u) == 0x0u) {
      if((instr & (1u << 22))) {
       if((instr & (1u << 21))) {
        return !((instr & 0xf0000u) == 0xf0000u);
       } else if((instr & 0xf0000u) == 0xf0000u) {
        return 0;
       } else if((instr & 0xf000u) == 0xf000u) {
        return 0;
       } else {
        return !((instr & 0xe00u) == 0xa00u);
       }
      } else if((instr & (1u << 21))) {
       return !((instr & 0xf0000u) == 0xf0000u);
      } else {
       return 0;
      }
     } else {
      return !((instr & 0xf0000u) == 0xf0000u);
     }
    } else if((instr & (1u << 25))) {
     return 0;
    } else if((instr & (1u << 22))) {
     return !!((instr & 0x1fffe0u) == 0xd0500u);
    } else if((instr & (1u << 21))) {
     if((instr & (1u << 20))) {
      return !((instr & 0xfffffu) == 0xf0a00u);
     } else {
      return 0;
     }
    } else if((instr & (1u << 20))) {
     if((instr & 0xf0000u) == 0xf0000u) {
      return 0;
     } else {
      return !!((instr & 0xffffu) == 0xa00u);
     }
    } else {
     return 0;
    }
   } else if((instr & (1u << 26))) {
    if((instr & (1u << 25))) {
     if((instr & 0x1400000u) == 0x0u) {
      return 0;
     } else if((instr & 0x300000u) == 0x100000u) {
      if((instr & 0xf0000u) == 0xf0000u) {
       return 0;
      } else if((instr & 0xf010u) == 0xf000u) {
       return !((instr & 0xfu) == 0xfu);
      } else {
       return 0;
      }
     } else {
      return 0;
     }
    } else if((instr & (1u << 24))) {
     if((instr & 0xc00000u) == 0x400000u) {
      if((instr & (1u << 21))) {
       if((instr & 0x1fff80u) == 0x1ff000u) {
        if((instr & (1u << 6))) {
         return !((instr & 0x30u) == 0x30u);
        } else {
         return !!((instr & 0x3fu) == 0x1fu);
        }
       } else {
        return 0;
       }
      } else if((instr & (1u << 20))) {
       return test_arm_instr_1893(instr);
      } else {
       return 0;
      }
     } else if((instr & 0x300000u) == 0x100000u) {
      return test_arm_instr_1893(instr);
     } else {
      return 0;
     }
    } else if((instr & (1u << 23))) {
     if((instr & (1u << 22))) {
      if((instr & (1u << 21))) {
       if((instr & (1u << 20))) {
        return 0;
       } else if((instr & 0xf0000u) == 0xf0000u) {
        return 0;
       } else if((instr & (1u << 15))) {
        if((instr & (1u << 14))) {
         if((instr & (1u << 13))) {
          if((instr & (1u << 12))) {
           if((instr & (1u << 11))) {
            if((instr & (1u << 10))) {
             if((instr & (1u << 9))) {
              return test_arm_instr_1919(instr);
             } else if((instr & (1u << 8))) {
              return 0;
             } else if((instr & (1u << 7))) {
              return !!((instr & 0x60u) == 0x0u);
             } else if((instr & (1u << 6))) {
              return !((instr & (1u << 5)));
             } else {
              return !!((instr & 0x30u) == 0x0u);
             }
            } else if((instr & 0x340u) == 0x0u) {
             return test_arm_instr_1902(instr);
            } else {
             return 0;
            }
           } else {
            return test_arm_instr_1964(instr);
           }
          } else if((instr & (1u << 11))) {
           if((instr & (1u << 10))) {
            if((instr & (1u << 9))) {
             return test_arm_instr_1919(instr);
            } else if((instr & (1u << 8))) {
             if((instr & 0xc0u) == 0xc0u) {
              return 0;
             } else {
              return !((instr & (1u << 5)));
             }
            } else {
             return test_arm_instr_1915(instr);
            }
           } else if((instr & (1u << 9))) {
            return 0;
           } else {
            return test_arm_instr_1957(instr);
           }
          } else {
           return test_arm_instr_1956(instr);
          }
         } else if((instr & (1u << 12))) {
          if((instr & (1u << 11))) {
           if((instr & (1u << 10))) {
            return test_arm_instr_1920(instr);
           } else {
            return test_arm_instr_1950(instr);
           }
          } else {
           return test_arm_instr_1948(instr);
          }
         } else if((instr & (1u << 11))) {
          if((instr & (1u << 10))) {
           return test_arm_instr_1920(instr);
          } else {
           return test_arm_instr_1941(instr);
          }
         } else {
          return test_arm_instr_1939(instr);
         }
        } else if((instr & (1u << 13))) {
         if((instr & (1u << 11))) {
          if((instr & (1u << 10))) {
           return test_arm_instr_1920(instr);
          } else {
           return test_arm_instr_1933(instr);
          }
         } else {
          return test_arm_instr_1931(instr);
         }
        } else {
         return test_arm_instr_1922(instr);
        }
       } else {
        return test_arm_instr_1922(instr);
       }
      } else if((instr & (1u << 20))) {
       return test_arm_instr_1893(instr);
      } else if((instr & 0xf0000u) == 0xf0000u) {
       return 0;
      } else if((instr & (1u << 15))) {
       if((instr & (1u << 14))) {
        if((instr & (1u << 13))) {
         if((instr & (1u << 12))) {
          if((instr & (1u << 11))) {
           if((instr & 0x740u) == 0x0u) {
            return test_arm_instr_1902(instr);
           } else {
            return 0;
           }
          } else {
           return test_arm_instr_1964(instr);
          }
         } else if((instr & (1u << 11))) {
          if((instr & 0x600u) == 0x0u) {
           return test_arm_instr_1957(instr);
          } else {
           return 0;
          }
         } else {
          return test_arm_instr_1956(instr);
         }
        } else if((instr & (1u << 12))) {
         if((instr & (1u << 11))) {
          if((instr & (1u << 10))) {
           return 0;
          } else {
           return test_arm_instr_1950(instr);
          }
         } else {
          return test_arm_instr_1948(instr);
         }
        } else if((instr & (1u << 11))) {
         if((instr & (1u << 10))) {
          return 0;
         } else {
          return test_arm_instr_1941(instr);
         }
        } else {
         return test_arm_instr_1939(instr);
        }
       } else if((instr & (1u << 13))) {
        if((instr & (1u << 11))) {
         if((instr & (1u << 10))) {
          return 0;
         } else {
          return test_arm_instr_1933(instr);
         }
        } else {
         return test_arm_instr_1931(instr);
        }
       } else {
        return test_arm_instr_1908(instr);
       }
      } else {
       return test_arm_instr_1908(instr);
      }
     } else if((instr & (1u << 21))) {
      if((instr & (1u << 20))) {
       return 0;
      } else if((instr & 0xf0000u) == 0xf0000u) {
       return 0;
      } else {
       return test_arm_instr_1922(instr);
      }
     } else if((instr & (1u << 20))) {
      return 0;
     } else if((instr & 0xf0000u) == 0xf0000u) {
      return 0;
     } else {
      return test_arm_instr_1908(instr);
     }
    } else if((instr & (1u << 22))) {
     if((instr & (1u << 21))) {
      if((instr & (1u << 20))) {
       return 0;
      } else {
       return test_arm_instr_1889(instr);
      }
     } else if((instr & (1u << 20))) {
      return test_arm_instr_1893(instr);
     } else {
      return test_arm_instr_1889(instr);
     }
    } else if((instr & (1u << 20))) {
     return 0;
    } else if((instr & 0xf0000u) == 0xf0000u) {
     return 0;
    } else {
     return test_arm_instr_1871(instr);
    }
   } else if((instr & (1u << 25))) {
    if((instr & (1u << 24))) {
     if((instr & (1u << 23))) {
      if((instr & (1u << 21))) {
       if((instr & (1u << 20))) {
        if((instr & (1u << 19))) {
         if((instr & (1u << 18))) {
          if((instr & 0x30000u) == 0x30000u) {
           if((instr & (1u << 12))) {
            if((instr & (1u << 11))) {
             if((instr & (1u << 10))) {
              return test_arm_instr_1761(instr);
             } else if((instr & (1u << 9))) {
              return !((instr & (1u << 4)));
             } else {
              return test_arm_instr_1841(instr);
             }
            } else {
             return !!((instr & 0x50u) == 0x10u);
            }
           } else if((instr & (1u << 11))) {
            if((instr & (1u << 10))) {
             return test_arm_instr_1753(instr);
            } else if((instr & (1u << 9))) {
             return test_arm_instr_1541(instr);
            } else {
             return test_arm_instr_1841(instr);
            }
           } else {
            return test_arm_instr_1408(instr);
           }
          } else {
           return test_arm_instr_1780(instr);
          }
         } else if((instr & (1u << 17))) {
          if((instr & (1u << 16))) {
           if((instr & (1u << 12))) {
            if((instr & (1u << 11))) {
             return test_arm_instr_1762(instr);
            } else if((instr & (1u << 10))) {
             return !((instr & (1u << 6)));
            } else {
             return !!((instr & 0x50u) == 0x10u);
            }
           } else if((instr & (1u << 11))) {
            return test_arm_instr_1754(instr);
           } else if((instr & (1u << 10))) {
            return !((instr & 0x41u) == 0x41u);
           } else {
            return test_arm_instr_1408(instr);
           }
          } else if((instr & (1u << 12))) {
           if((instr & (1u << 11))) {
            return test_arm_instr_1762(instr);
           } else if((instr & (1u << 10))) {
            return !!((instr & 0x50u) == 0x10u);
           } else if((instr & (1u << 9))) {
            return test_arm_instr_1773(instr);
           } else if((instr & 0x180u) == 0x80u) {
            return !((instr & (1u << 6)));
           } else {
            return !!((instr & 0x50u) == 0x10u);
           }
          } else if((instr & (1u << 11))) {
           return test_arm_instr_1754(instr);
          } else if((instr & (1u << 10))) {
           return test_arm_instr_1408(instr);
          } else if((instr & (1u << 9))) {
           return test_arm_instr_1768(instr);
          } else if((instr & (1u << 8))) {
           return test_arm_instr_1607(instr);
          } else if((instr & (1u << 7))) {
           return !((instr & 0x41u) == 0x41u);
          } else {
           return test_arm_instr_1408(instr);
          }
         } else if((instr & (1u << 16))) {
          if((instr & (1u << 12))) {
           if((instr & (1u << 11))) {
            return test_arm_instr_1762(instr);
           } else {
            return test_arm_instr_1757(instr);
           }
          } else if((instr & (1u << 11))) {
           return test_arm_instr_1754(instr);
          } else {
           return test_arm_instr_1749(instr);
          }
         } else if((instr & (1u << 12))) {
          if((instr & (1u << 11))) {
           return test_arm_instr_1745(instr);
          } else if((instr & (1u << 10))) {
           if((instr & 0x300u) == 0x100u) {
            return !!((instr & 0x50u) == 0x10u);
           } else {
            return !((instr & (1u << 6)));
           }
          } else if((instr & (1u << 9))) {
           return test_arm_instr_1405(instr);
          } else if((instr & 0x180u) == 0x0u) {
           return !((instr & (1u << 6)));
          } else {
           return !!((instr & 0x50u) == 0x10u);
          }
         } else if((instr & (1u << 11))) {
          return test_arm_instr_1738(instr);
         } else if((instr & (1u << 10))) {
          if((instr & 0x300u) == 0x100u) {
           return test_arm_instr_1408(instr);
          } else {
           return !((instr & 0x41u) == 0x41u);
          }
         } else if((instr & (1u << 9))) {
          return test_arm_instr_1735(instr);
         } else if((instr & 0x180u) == 0x0u) {
          return !((instr & 0x41u) == 0x41u);
         } else {
          return test_arm_instr_1408(instr);
         }
        } else if((instr & (1u << 18))) {
         if((instr & (1u << 17))) {
          if((instr & (1u << 16))) {
           return test_arm_instr_1780(instr);
          } else if((instr & (1u << 12))) {
           if((instr & (1u << 11))) {
            return test_arm_instr_1762(instr);
           } else if((instr & (1u << 10))) {
            if((instr & 0x380u) == 0x200u) {
             return test_arm_instr_1481(instr);
            } else {
             return !!((instr & 0x50u) == 0x10u);
            }
           } else if((instr & (1u << 9))) {
            return test_arm_instr_1773(instr);
           } else if((instr & 0x180u) == 0x0u) {
            return !!((instr & 0x50u) == 0x10u);
           } else {
            return !((instr & (1u << 6)));
           }
          } else if((instr & (1u << 11))) {
           return test_arm_instr_1754(instr);
          } else if((instr & (1u << 10))) {
           if((instr & (1u << 9))) {
            if((instr & (1u << 8))) {
             return test_arm_instr_1767(instr);
            } else if((instr & (1u << 7))) {
             return test_arm_instr_1408(instr);
            } else {
             return test_arm_instr_1664(instr);
            }
           } else {
            return test_arm_instr_1408(instr);
           }
          } else if((instr & (1u << 9))) {
           return test_arm_instr_1768(instr);
          } else if((instr & 0x180u) == 0x0u) {
           return test_arm_instr_1408(instr);
          } else {
           return !((instr & 0x41u) == 0x41u);
          }
         } else if((instr & (1u << 16))) {
          return test_arm_instr_1764(instr);
         } else if((instr & (1u << 12))) {
          if((instr & (1u << 11))) {
           return test_arm_instr_1762(instr);
          } else if((instr & 0x600u) == 0x600u) {
           return !((instr & (1u << 6)));
          } else {
           return test_arm_instr_1405(instr);
          }
         } else if((instr & (1u << 11))) {
          return test_arm_instr_1754(instr);
         } else if((instr & 0x600u) == 0x600u) {
          return !((instr & 0x41u) == 0x41u);
         } else {
          return test_arm_instr_1735(instr);
         }
        } else if((instr & (1u << 17))) {
         if((instr & (1u << 16))) {
          return test_arm_instr_1780(instr);
         } else if((instr & (1u << 12))) {
          if((instr & (1u << 11))) {
           return test_arm_instr_1762(instr);
          } else if((instr & (1u << 10))) {
           return !!((instr & 0x50u) == 0x10u);
          } else if((instr & (1u << 9))) {
           return test_arm_instr_1773(instr);
          } else {
           return !((instr & (1u << 6)));
          }
         } else if((instr & (1u << 11))) {
          return test_arm_instr_1754(instr);
         } else if((instr & (1u << 10))) {
          return test_arm_instr_1408(instr);
         } else if((instr & (1u << 9))) {
          return test_arm_instr_1768(instr);
         } else {
          return !((instr & 0x41u) == 0x41u);
         }
        } else if((instr & (1u << 16))) {
         return test_arm_instr_1764(instr);
        } else if((instr & (1u << 12))) {
         if((instr & (1u << 11))) {
          return test_arm_instr_1745(instr);
         } else if((instr & (1u << 10))) {
          return !((instr & (1u << 6)));
         } else if((instr & (1u << 9))) {
          return test_arm_instr_1405(instr);
         } else if((instr & 0x180u) == 0x180u) {
          return !!((instr & 0x50u) == 0x10u);
         } else {
          return !((instr & (1u << 6)));
         }
        } else if((instr & (1u << 11))) {
         return test_arm_instr_1738(instr);
        } else if((instr & (1u << 10))) {
         return !((instr & 0x41u) == 0x41u);
        } else if((instr & (1u << 9))) {
         return test_arm_instr_1735(instr);
        } else if((instr & 0x180u) == 0x180u) {
         return test_arm_instr_1408(instr);
        } else {
         return !((instr & 0x41u) == 0x41u);
        }
       } else if((instr & (1u << 16))) {
        if((instr & (1u << 12))) {
         if((instr & (1u << 11))) {
          return test_arm_instr_1614(instr);
         } else {
          return !!((instr & 0x50u) == 0x10u);
         }
        } else if((instr & (1u << 11))) {
         if((instr & (1u << 10))) {
          if((instr & (1u << 9))) {
           return test_arm_instr_1578(instr);
          } else {
           return !!((instr & 0x150u) == 0x0u);
          }
         } else {
          return test_arm_instr_1710(instr);
         }
        } else {
         return test_arm_instr_1709(instr);
        }
       } else if((instr & (1u << 12))) {
        if((instr & (1u << 11))) {
         return test_arm_instr_1614(instr);
        } else {
         return test_arm_instr_1673(instr);
        }
       } else if((instr & (1u << 11))) {
        if((instr & (1u << 10))) {
         if((instr & (1u << 9))) {
          return test_arm_instr_1578(instr);
         } else {
          return test_arm_instr_1701(instr);
         }
        } else if((instr & (1u << 9))) {
         return test_arm_instr_1699(instr);
        } else if((instr & (1u << 8))) {
         return test_arm_instr_1553(instr);
        } else {
         return test_arm_instr_1537(instr);
        }
       } else if((instr & (1u << 10))) {
        if((instr & (1u << 9))) {
         return test_arm_instr_1696(instr);
        } else if((instr & (1u << 8))) {
         return !((instr & 0x51u) == 0x51u);
        } else {
         return test_arm_instr_1695(instr);
        }
       } else if((instr & 0x300u) == 0x300u) {
        return test_arm_instr_1465(instr);
       } else {
        return !((instr & 0x51u) == 0x51u);
       }
      } else if((instr & (1u << 20))) {
       if((instr & (1u << 16))) {
        if((instr & (1u << 12))) {
         return test_arm_instr_1691(instr);
        } else if((instr & (1u << 11))) {
         if((instr & (1u << 10))) {
          return !!((instr & 0x350u) == 0x0u);
         } else {
          return test_arm_instr_1710(instr);
         }
        } else {
         return test_arm_instr_1709(instr);
        }
       } else if((instr & (1u << 12))) {
        return test_arm_instr_1686(instr);
       } else if((instr & (1u << 11))) {
        if((instr & (1u << 10))) {
         if((instr & (1u << 9))) {
          return 0;
         } else {
          return test_arm_instr_1701(instr);
         }
        } else if((instr & (1u << 9))) {
         return test_arm_instr_1699(instr);
        } else if((instr & (1u << 8))) {
         return !!((instr & 0x91u) == 0x10u);
        } else {
         return test_arm_instr_1537(instr);
        }
       } else if((instr & (1u << 10))) {
        return test_arm_instr_1696(instr);
       } else if((instr & (1u << 8))) {
        return test_arm_instr_1465(instr);
       } else {
        return !((instr & 0x51u) == 0x51u);
       }
      } else if((instr & (1u << 19))) {
       if((instr & (1u << 16))) {
        if((instr & (1u << 12))) {
         return test_arm_instr_1691(instr);
        } else if((instr & (1u << 11))) {
         return test_arm_instr_1684(instr);
        } else if((instr & (1u << 10))) {
         return test_arm_instr_1688(instr);
        } else {
         return test_arm_instr_1519(instr);
        }
       } else if((instr & (1u << 12))) {
        return test_arm_instr_1686(instr);
       } else if((instr & (1u << 11))) {
        return test_arm_instr_1684(instr);
       } else if((instr & 0x500u) == 0x400u) {
        return test_arm_instr_1664(instr);
       } else {
        return test_arm_instr_1465(instr);
       }
      } else if((instr & (1u << 16))) {
       if((instr & (1u << 12))) {
        if((instr & (1u << 11))) {
         return test_arm_instr_1488(instr);
        } else {
         return !!((instr & 0x50u) == 0x10u);
        }
       } else if((instr & (1u << 11))) {
        return test_arm_instr_1670(instr);
       } else if((instr & (1u << 10))) {
        if((instr & (1u << 8))) {
         return test_arm_instr_1466(instr);
        } else {
         return test_arm_instr_1491(instr);
        }
       } else {
        return test_arm_instr_1492(instr);
       }
      } else if((instr & (1u << 12))) {
       if((instr & (1u << 11))) {
        return test_arm_instr_1488(instr);
       } else {
        return test_arm_instr_1673(instr);
       }
      } else if((instr & (1u << 11))) {
       return test_arm_instr_1670(instr);
      } else if((instr & 0x500u) == 0x400u) {
       if((instr & (1u << 7))) {
        return test_arm_instr_1664(instr);
       } else {
        return test_arm_instr_1468(instr);
       }
      } else {
       return test_arm_instr_1466(instr);
      }
     } else if((instr & (1u << 21))) {
      if((instr & (1u << 20))) {
       return test_arm_instr_1461(instr);
      } else if((instr & 0x11000u) == 0x0u) {
       if((instr & (1u << 11))) {
        if((instr & (1u << 10))) {
         if((instr & (1u << 9))) {
          return test_arm_instr_1631(instr);
         } else if((instr & (1u << 8))) {
          return test_arm_instr_1411(instr);
         } else {
          return 0;
         }
        } else {
         return test_arm_instr_1644(instr);
        }
       } else {
        return !((instr & 0x41u) == 0x41u);
       }
      } else if((instr & (1u << 11))) {
       if((instr & 0x600u) == 0x400u) {
        return !!((instr & 0x150u) == 0x100u);
       } else {
        return test_arm_instr_1636(instr);
       }
      } else {
       return !((instr & (1u << 6)));
      }
     } else if((instr & (1u << 20))) {
      if((instr & 0x11000u) == 0x0u) {
       if((instr & (1u << 11))) {
        if((instr & (1u << 10))) {
         return 0;
        } else {
         return test_arm_instr_1644(instr);
        }
       } else {
        return !((instr & 0x41u) == 0x41u);
       }
      } else if((instr & (1u << 11))) {
       if((instr & (1u << 10))) {
        return 0;
       } else {
        return test_arm_instr_1636(instr);
       }
      } else {
       return !((instr & (1u << 6)));
      }
     } else if((instr & 0x11000u) == 0x0u) {
      if((instr & (1u << 11))) {
       if((instr & (1u << 10))) {
        if((instr & (1u << 9))) {
         return test_arm_instr_1631(instr);
        } else {
         return test_arm_instr_1520(instr);
        }
       } else if((instr & (1u << 9))) {
        return !!((instr & 0x140u) == 0x0u);
       } else {
        return !((instr & 0x41u) == 0x41u);
       }
      } else {
       return !((instr & 0x41u) == 0x41u);
      }
     } else if((instr & (1u << 11))) {
      if((instr & (1u << 10))) {
       if((instr & (1u << 9))) {
        return test_arm_instr_1636(instr);
       } else {
        return !!((instr & 0x140u) == 0x100u);
       }
      } else if((instr & 0x300u) == 0x300u) {
       return 0;
      } else {
       return !((instr & (1u << 6)));
      }
     } else {
      return !((instr & (1u << 6)));
     }
    } else if((instr & (1u << 23))) {
     if((instr & (1u << 21))) {
      if((instr & (1u << 20))) {
       if((instr & (1u << 16))) {
        if((instr & (1u << 12))) {
         return test_arm_instr_1615(instr);
        } else if((instr & (1u << 11))) {
         if((instr & (1u << 10))) {
          return test_arm_instr_1621(instr);
         } else if((instr & (1u << 9))) {
          return !!((instr & 0x1d0u) == 0x10u);
         } else {
          return !!((instr & 0x91u) == 0x10u);
         }
        } else if((instr & 0x500u) == 0x400u) {
         return !!((instr & 0x50u) == 0x0u);
        } else {
         return test_arm_instr_1465(instr);
        }
       } else if((instr & (1u << 12))) {
        return test_arm_instr_1615(instr);
       } else if((instr & (1u << 11))) {
        if((instr & (1u << 10))) {
         if((instr & 0x280u) == 0x200u) {
          return test_arm_instr_1607(instr);
         } else {
          return !!((instr & 0x51u) == 0x40u);
         }
        } else if((instr & (1u << 9))) {
         if((instr & 0x180u) == 0x0u) {
          if((instr & (1u << 6))) {
           return !!((instr & 0x11u) == 0x0u);
          } else {
           return !!((instr & (1u << 4)));
          }
         } else {
          return !!((instr & 0x51u) == 0x40u);
         }
        } else if((instr & (1u << 7))) {
         return !!((instr & 0x51u) == 0x40u);
        } else if((instr & 0x50u) == 0x0u) {
         return 0;
        } else {
         return !((instr & (1u << 0)));
        }
       } else if((instr & 0x500u) == 0x400u) {
        return test_arm_instr_1411(instr);
       } else {
        return !((instr & 0x41u) == 0x41u);
       }
      } else if((instr & (1u << 16))) {
       if((instr & (1u << 12))) {
        if((instr & (1u << 11))) {
         return test_arm_instr_1588(instr);
        } else if((instr & (1u << 10))) {
         if((instr & (1u << 9))) {
          return !!((instr & 0x150u) == 0x110u);
         } else if((instr & (1u << 8))) {
          return test_arm_instr_1546(instr);
         } else {
          return !!((instr & 0x50u) == 0x40u);
         }
        } else {
         return test_arm_instr_1582(instr);
        }
       } else if((instr & (1u << 11))) {
        return test_arm_instr_1580(instr);
       } else if((instr & (1u << 10))) {
        return test_arm_instr_1565(instr);
       } else {
        return test_arm_instr_1562(instr);
       }
      } else if((instr & (1u << 12))) {
       if((instr & (1u << 11))) {
        return test_arm_instr_1588(instr);
       } else if((instr & (1u << 10))) {
        if((instr & (1u << 9))) {
         return test_arm_instr_1513(instr);
        } else if((instr & (1u << 8))) {
         return test_arm_instr_1546(instr);
        } else {
         return test_arm_instr_1532(instr);
        }
       } else {
        return test_arm_instr_1582(instr);
       }
      } else if((instr & (1u << 11))) {
       return test_arm_instr_1580(instr);
      } else if((instr & 0x500u) == 0x400u) {
       return test_arm_instr_1532(instr);
      } else {
       return !((instr & 0x51u) == 0x51u);
      }
     } else if((instr & (1u << 20))) {
      if((instr & (1u << 16))) {
       if((instr & (1u << 12))) {
        if((instr & (1u << 11))) {
         return test_arm_instr_1557(instr);
        } else if((instr & (1u << 10))) {
         if((instr & (1u << 9))) {
          return !!((instr & 0x150u) == 0x110u);
         } else if((instr & (1u << 8))) {
          return !!((instr & 0x50u) == 0x10u);
         } else {
          return !!((instr & 0x50u) == 0x40u);
         }
        } else {
         return test_arm_instr_1548(instr);
        }
       } else if((instr & (1u << 11))) {
        return test_arm_instr_1544(instr);
       } else if((instr & (1u << 10))) {
        if((instr & (1u << 9))) {
         return test_arm_instr_1565(instr);
        } else if((instr & (1u << 8))) {
         return test_arm_instr_1465(instr);
        } else {
         return !!((instr & 0x50u) == 0x40u);
        }
       } else if((instr & (1u << 9))) {
        return test_arm_instr_1562(instr);
       } else {
        return test_arm_instr_1560(instr);
       }
      } else if((instr & (1u << 12))) {
       if((instr & (1u << 11))) {
        return test_arm_instr_1557(instr);
       } else if((instr & (1u << 10))) {
        if((instr & (1u << 9))) {
         return test_arm_instr_1513(instr);
        } else if((instr & (1u << 8))) {
         return !!((instr & 0x50u) == 0x10u);
        } else {
         return test_arm_instr_1532(instr);
        }
       } else {
        return test_arm_instr_1548(instr);
       }
      } else if((instr & (1u << 11))) {
       return test_arm_instr_1544(instr);
      } else if((instr & (1u << 10))) {
       if((instr & (1u << 9))) {
        if((instr & (1u << 8))) {
         return !((instr & 0x51u) == 0x51u);
        } else {
         return test_arm_instr_1532(instr);
        }
       } else if((instr & (1u << 8))) {
        return test_arm_instr_1465(instr);
       } else {
        return test_arm_instr_1532(instr);
       }
      } else if((instr & 0x300u) == 0x100u) {
       return test_arm_instr_1465(instr);
      } else {
       return !((instr & 0x51u) == 0x51u);
      }
     } else if((instr & (1u << 19))) {
      if((instr & (1u << 16))) {
       if((instr & (1u << 12))) {
        if((instr & (1u << 11))) {
         return !!((instr & 0x691u) == 0x10u);
        } else if((instr & 0x500u) == 0x400u) {
         return 0;
        } else {
         return !!((instr & 0x50u) == 0x10u);
        }
       } else if((instr & (1u << 11))) {
        return test_arm_instr_1511(instr);
       } else if((instr & (1u << 10))) {
        return test_arm_instr_1520(instr);
       } else {
        return test_arm_instr_1519(instr);
       }
      } else if((instr & (1u << 12))) {
       if((instr & (1u << 11))) {
        return !!((instr & 0x691u) == 0x10u);
       } else if((instr & 0x500u) == 0x400u) {
        return !!((instr & 0x51u) == 0x0u);
       } else {
        return !!((instr & 0x50u) == 0x10u);
       }
      } else if((instr & (1u << 11))) {
       return test_arm_instr_1511(instr);
      } else if((instr & 0x500u) == 0x400u) {
       return !!((instr & 0x51u) == 0x0u);
      } else {
       return test_arm_instr_1465(instr);
      }
     } else if((instr & (1u << 16))) {
      if((instr & (1u << 12))) {
       if((instr & (1u << 11))) {
        return test_arm_instr_1488(instr);
       } else if((instr & 0x580u) == 0x480u) {
        return 0;
       } else {
        return !!((instr & 0x50u) == 0x10u);
       }
      } else if((instr & (1u << 11))) {
       return test_arm_instr_1479(instr);
      } else if((instr & (1u << 10))) {
       if((instr & (1u << 8))) {
        return test_arm_instr_1466(instr);
       } else {
        return !!((instr & 0x90u) == 0x10u);
       }
      } else {
       return test_arm_instr_1492(instr);
      }
     } else if((instr & (1u << 12))) {
      if((instr & (1u << 11))) {
       return test_arm_instr_1488(instr);
      } else if((instr & 0x500u) == 0x400u) {
       if((instr & (1u << 7))) {
        return !!((instr & 0x51u) == 0x0u);
       } else {
        return test_arm_instr_1481(instr);
       }
      } else {
       return !!((instr & 0x50u) == 0x10u);
      }
     } else if((instr & (1u << 11))) {
      return test_arm_instr_1479(instr);
     } else if((instr & 0x500u) == 0x400u) {
      if((instr & (1u << 7))) {
       return !!((instr & 0x51u) == 0x0u);
      } else {
       return test_arm_instr_1468(instr);
      }
     } else {
      return test_arm_instr_1466(instr);
     }
    } else if((instr & (1u << 21))) {
     if((instr & (1u << 20))) {
      return test_arm_instr_1461(instr);
     } else if((instr & 0x11000u) == 0x0u) {
      if((instr & (1u << 11))) {
       if((instr & (1u << 10))) {
        if((instr & (1u << 9))) {
         if((instr & (1u << 8))) {
          return !((instr & 0x41u) == 0x41u);
         } else {
          return 0;
         }
        } else {
         return test_arm_instr_1409(instr);
        }
       } else {
        return test_arm_instr_1426(instr);
       }
      } else {
       return !((instr & 0x41u) == 0x41u);
      }
     } else if((instr & 0xc00u) == 0xc00u) {
      if((instr & (1u << 9))) {
       return !!((instr & 0x140u) == 0x100u);
      } else {
       return test_arm_instr_1417(instr);
      }
     } else {
      return !((instr & (1u << 6)));
     }
    } else if((instr & (1u << 20))) {
     if((instr & 0x11000u) == 0x0u) {
      if((instr & (1u << 11))) {
       if((instr & (1u << 10))) {
        return 0;
       } else {
        return test_arm_instr_1426(instr);
       }
      } else {
       return !((instr & 0x41u) == 0x41u);
      }
     } else if((instr & 0xc00u) == 0xc00u) {
      return 0;
     } else {
      return !((instr & (1u << 6)));
     }
    } else if((instr & 0x11000u) == 0x0u) {
     if((instr & (1u << 11))) {
      if((instr & (1u << 10))) {
       if((instr & (1u << 9))) {
        if((instr & (1u << 8))) {
         return !((instr & 0x41u) == 0x41u);
        } else {
         return test_arm_instr_1411(instr);
        }
       } else {
        return test_arm_instr_1409(instr);
       }
      } else if((instr & (1u << 9))) {
       return test_arm_instr_1405(instr);
      } else {
       return !((instr & 0x41u) == 0x41u);
      }
     } else {
      return !((instr & 0x41u) == 0x41u);
     }
    } else if((instr & (1u << 11))) {
     if((instr & (1u << 10))) {
      if((instr & (1u << 9))) {
       if((instr & (1u << 8))) {
        return !((instr & (1u << 6)));
       } else {
        return !!((instr & 0x50u) == 0x0u);
       }
      } else {
       return test_arm_instr_1417(instr);
      }
     } else if((instr & 0x300u) == 0x300u) {
      return !!((instr & 0x50u) == 0x10u);
     } else {
      return !((instr & (1u << 6)));
     }
    } else {
     return !((instr & (1u << 6)));
    }
   } else if((instr & 0x1f00000u) == 0x1000000u) {
    if((instr & (1u << 19))) {
     if((instr & (1u << 17))) {
      return test_arm_instr_1392(instr);
     } else if((instr & (1u << 16))) {
      return 0;
     } else {
      return test_arm_instr_1376(instr);
     }
    } else if((instr & (1u << 18))) {
     return 0;
    } else if((instr & (1u << 17))) {
     return test_arm_instr_1392(instr);
    } else if((instr & (1u << 16))) {
     return !!((instr & 0xfdffu) == 0x0u);
    } else {
     return test_arm_instr_1376(instr);
    }
   } else {
    return 0;
   }
  } else if((instr & (1u << 27))) {
   return test_arm_instr_1256(instr);
  } else if((instr & (1u << 26))) {
   if((instr & (1u << 25))) {
    if((instr & (1u << 24))) {
     if((instr & (1u << 23))) {
      if((instr & (1u << 22))) {
       if((instr & (1u << 21))) {
        if((instr & (1u << 20))) {
         if((instr & 0xf0000u) == 0xf0000u) {
          if((instr & 0xf000u) == 0xf000u) {
           return !!((instr & 0xf0u) == 0xf0u);
          } else if((instr & (1u << 7))) {
           return test_arm_instr_325(instr);
          } else {
           return test_arm_instr_149(instr);
          }
         } else if((instr & 0xf000u) == 0xf000u) {
          return !!((instr & 0xf0u) == 0xf0u);
         } else if((instr & (1u << 7))) {
          if((instr & (1u << 6))) {
           return test_arm_instr_76(instr);
          } else {
           return test_arm_instr_6(instr);
          }
         } else {
          return test_arm_instr_897(instr);
         }
        } else {
         return test_arm_instr_909(instr);
        }
       } else {
        return test_arm_instr_926(instr);
       }
      } else {
       return test_arm_instr_910(instr);
      }
     } else {
      return test_arm_instr_882(instr);
     }
    } else {
     return test_arm_instr_824(instr);
    }
   } else {
    return test_arm_instr_694(instr);
   }
  } else if((instr & (1u << 25))) {
   return test_arm_instr_726(instr);
  } else if((instr & (1u << 24))) {
   if((instr & (1u << 23))) {
    return test_arm_instr_684(instr);
   } else if((instr & (1u << 22))) {
    return test_arm_instr_458(instr);
   } else if((instr & (1u << 21))) {
    if((instr & (1u << 20))) {
     return test_arm_instr_234(instr);
    } else if((instr & (1u << 19))) {
     if((instr & (1u << 18))) {
      if((instr & 0x30000u) == 0x30000u) {
       if((instr & 0xf000u) == 0xf000u) {
        if((instr & (1u << 11))) {
         if((instr & 0x700u) == 0x700u) {
          if((instr & (1u << 7))) {
           return 0;
          } else if((instr & (1u << 6))) {
           return !!((instr & 0x30u) == 0x30u);
          } else {
           return test_arm_instr_301(instr);
          }
         } else {
          return !!((instr & 0xf0u) == 0x70u);
         }
        } else if((instr & (1u << 10))) {
         return !!((instr & 0xf0u) == 0x70u);
        } else if((instr & (1u << 9))) {
         if((instr & (1u << 8))) {
          return test_arm_instr_1313(instr);
         } else {
          return test_arm_instr_355(instr);
         }
        } else if((instr & (1u << 8))) {
         return !!((instr & 0xf0u) == 0x70u);
        } else {
         return test_arm_instr_1313(instr);
        }
       } else {
        return !!((instr & 0xf0u) == 0x70u);
       }
      } else {
       return test_arm_instr_1290(instr);
      }
     } else if((instr & 0xe000u) == 0xe000u) {
      if((instr & (1u << 12))) {
       if((instr & (1u << 11))) {
        return test_arm_instr_1277(instr);
       } else if((instr & (1u << 10))) {
        return test_arm_instr_1270(instr);
       } else if((instr & (1u << 9))) {
        if((instr & (1u << 8))) {
         return test_arm_instr_1293(instr);
        } else {
         return test_arm_instr_1272(instr);
        }
       } else {
        return test_arm_instr_1283(instr);
       }
      } else {
       return test_arm_instr_1269(instr);
      }
     } else {
      return test_arm_instr_1263(instr);
     }
    } else if((instr & (1u << 18))) {
     if((instr & 0x30000u) == 0x30000u) {
      if((instr & 0xe000u) == 0xe000u) {
       if((instr & (1u << 12))) {
        if((instr & (1u << 11))) {
         return test_arm_instr_1277(instr);
        } else if((instr & (1u << 10))) {
         return test_arm_instr_1270(instr);
        } else if((instr & (1u << 9))) {
         if((instr & (1u << 8))) {
          return test_arm_instr_1272(instr);
         } else {
          return test_arm_instr_1293(instr);
         }
        } else {
         return test_arm_instr_1283(instr);
        }
       } else {
        return test_arm_instr_1269(instr);
       }
      } else {
       return test_arm_instr_1263(instr);
      }
     } else {
      return test_arm_instr_1290(instr);
     }
    } else if((instr & 0x30000u) == 0x0u) {
     if((instr & 0xe000u) == 0xe000u) {
      if((instr & (1u << 12))) {
       if((instr & (1u << 11))) {
        return test_arm_instr_1277(instr);
       } else if((instr & 0x600u) == 0x200u) {
        return test_arm_instr_1272(instr);
       } else {
        return test_arm_instr_1270(instr);
       }
      } else {
       return test_arm_instr_1269(instr);
      }
     } else {
      return test_arm_instr_1263(instr);
     }
    } else {
     return test_arm_instr_1290(instr);
    }
   } else {
    return test_arm_instr_235(instr);
   }
  } else {
   return test_arm_instr_148(instr);
  }
 } else if((instr & (1u << 27))) {
  return test_arm_instr_1256(instr);
 } else if((instr & (1u << 26))) {
  if((instr & (1u << 25))) {
   if((instr & (1u << 24))) {
    if((instr & (1u << 23))) {
     if((instr & (1u << 22))) {
      if((instr & (1u << 21))) {
       return test_arm_instr_909(instr);
      } else {
       return test_arm_instr_926(instr);
      }
     } else {
      return test_arm_instr_910(instr);
     }
    } else {
     return test_arm_instr_882(instr);
    }
   } else {
    return test_arm_instr_824(instr);
   }
  } else {
   return test_arm_instr_694(instr);
  }
 } else if((instr & (1u << 25))) {
  return test_arm_instr_726(instr);
 } else if((instr & (1u << 24))) {
  if((instr & (1u << 23))) {
   return test_arm_instr_684(instr);
  } else if((instr & (1u << 22))) {
   return test_arm_instr_458(instr);
  } else if((instr & (1u << 21))) {
   if((instr & (1u << 20))) {
    return test_arm_instr_234(instr);
   } else if((instr & (1u << 19))) {
    if((instr & (1u << 18))) {
     if((instr & 0x30000u) == 0x30000u) {
      if((instr & 0xf000u) == 0xf000u) {
       if((instr & (1u << 11))) {
        if((instr & 0x7c0u) == 0x700u) {
         return test_arm_instr_301(instr);
        } else {
         return 0;
        }
       } else if((instr & (1u << 10))) {
        return 0;
       } else if((instr & (1u << 9))) {
        if((instr & (1u << 8))) {
         if((instr & 0xf0u) == 0x0u) {
          return !((instr & 0xfu) == 0xfu);
         } else {
          return 0;
         }
        } else {
         return !!((instr & 0xf0u) == 0x0u);
        }
       } else {
        return test_arm_instr_297(instr);
       }
      } else {
       return 0;
      }
     } else {
      return test_arm_instr_273(instr);
     }
    } else if((instr & 0xe000u) == 0xe000u) {
     if((instr & (1u << 12))) {
      if((instr & (1u << 11))) {
       return test_arm_instr_260(instr);
      } else if((instr & (1u << 10))) {
       return test_arm_instr_252(instr);
      } else if((instr & (1u << 9))) {
       if((instr & (1u << 8))) {
        return test_arm_instr_276(instr);
       } else {
        return test_arm_instr_255(instr);
       }
      } else {
       return test_arm_instr_266(instr);
      }
     } else {
      return test_arm_instr_250(instr);
     }
    } else {
     return test_arm_instr_243(instr);
    }
   } else if((instr & (1u << 18))) {
    if((instr & 0x30000u) == 0x30000u) {
     if((instr & 0xe000u) == 0xe000u) {
      if((instr & (1u << 12))) {
       if((instr & (1u << 11))) {
        return test_arm_instr_260(instr);
       } else if((instr & (1u << 10))) {
        return test_arm_instr_252(instr);
       } else if((instr & (1u << 9))) {
        if((instr & (1u << 8))) {
         return test_arm_instr_255(instr);
        } else {
         return test_arm_instr_276(instr);
        }
       } else {
        return test_arm_instr_266(instr);
       }
      } else {
       return test_arm_instr_250(instr);
      }
     } else {
      return test_arm_instr_243(instr);
     }
    } else {
     return test_arm_instr_273(instr);
    }
   } else if((instr & 0x30000u) == 0x0u) {
    if((instr & 0xe000u) == 0xe000u) {
     if((instr & (1u << 12))) {
      if((instr & (1u << 11))) {
       return test_arm_instr_260(instr);
      } else if((instr & 0x600u) == 0x200u) {
       return test_arm_instr_255(instr);
      } else {
       return test_arm_instr_252(instr);
      }
     } else {
      return test_arm_instr_250(instr);
     }
    } else {
     return test_arm_instr_243(instr);
    }
   } else {
    return test_arm_instr_273(instr);
   }
  } else {
   return test_arm_instr_235(instr);
  }
 } else {
  return test_arm_instr_148(instr);
 }
}
