.global emu_set_tid_address
.global emu_mmap2
.global raw_syscall
.global asm_ensure_emutls_exists
.global call_on_stack
.hidden emu_set_tid_address
.hidden emu_mmap2
.hidden raw_syscall
.hidden asm_ensure_emutls_exists
.hidden call_on_stack
.arm

emu_set_tid_address:
push {r1}
mrc 15, #0, r1, cr13, cr0, #3
ldr r1, [r1, #-4]
ldr r1, [r1, #0]
str r0, [r1, #0]
pop {r1}
mov r0, #0

emu_mmap2:
push {r1, r2, r3, lr}
push {r4, r5}
add lr, pc, #4
ldr pc, [pc, #-4]
.long 0
pop {r4, r5}
pop {r1, r2, r3, lr}

raw_syscall:
mov ip, sp
push {r4, r5, r7, lr}
mov r7, r0
mov r0, r1
mov r1, r2
mov r2, r3
ldm ip, {r3, r4, r5}
svc #0
pop {r4, r5, r7, pc}

asm_ensure_emutls_exists:
sub sp, sp, #128
push {r0, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, lr}
mov fp, sp
and sp, sp, #-16
vmov r0, r1, d0
vmov r2, r3, d1
vmov r4, r5, d2
vmov r6, r7, d3
push {r0, r1, r2, r3, r4, r5, r6, r7}
vmov r0, r1, d4
vmov r2, r3, d5
vmov r4, r5, d6
vmov r6, r7, d7
push {r0, r1, r2, r3, r4, r5, r6, r7}
vmov r0, r1, d8
vmov r2, r3, d9
vmov r4, r5, d10
vmov r6, r7, d11
push {r0, r1, r2, r3, r4, r5, r6, r7}
vmov r0, r1, d12
vmov r2, r3, d13
vmov r4, r5, d14
vmov r6, r7, d15
push {r0, r1, r2, r3, r4, r5, r6, r7}
bl ensure_emutls_exists
pop {r0, r1, r2, r3, r4, r5, r6, r7}
vmov d15, r6, r7
vmov d14, r4, r5
vmov d13, r2, r3
vmov d12, r0, r1
pop {r0, r1, r2, r3, r4, r5, r6, r7}
vmov d11, r6, r7
vmov d10, r4, r5
vmov d9, r2, r3
vmov d8, r0, r1
pop {r0, r1, r2, r3, r4, r5, r6, r7}
vmov d7, r6, r7
vmov d6, r4, r5
vmov d5, r2, r3
vmov d4, r0, r1
pop {r0, r1, r2, r3, r4, r5, r6, r7}
vmov d3, r6, r7
vmov d2, r4, r5
vmov d1, r2, r3
vmov d0, r0, r1
mov sp, fp
pop {r0, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, lr}
add sp, sp, #128
bx lr

call_on_stack:
push {fp, lr}
mov fp, sp
mov sp, r0
mov r0, r2
bx r1
mov sp, fp
pop {fp, lr}
