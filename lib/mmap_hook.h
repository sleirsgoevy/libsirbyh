#pragma once
#include <stddef.h>
#include <stdint.h>

uintptr_t modified_mmap(uintptr_t addr, size_t size, int prot, int flags, int fd, uintptr_t offset);
