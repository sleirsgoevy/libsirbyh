#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <setjmp.h>
#include <inttypes.h>
#include <dlfcn.h>
#include <sched.h>
#include <sys/syscall.h>
#include <unistd.h>
#include <pthread.h>
#include <linux/futex.h>
#include "elfldr.h"
#include "patches.h"
#include "tls_emu.h"
#include "thread_atexit.h"

extern char stub_data[];
extern size_t stub_size;

static __thread jmp_buf jb;
static void*(*ptr_android_dlopen)(const char* path, int flags);
static void*(*ptr_android_dlsym)(void* handle, const char* name);
static void* android_clone;

static void stub_callback(uintptr_t p_android_dlopen, uintptr_t p_android_dlsym, uintptr_t p_android_clone)
{
    ptr_android_dlopen = (void*)p_android_dlopen;
    ptr_android_dlsym = (void*)p_android_dlsym;
    android_clone = (void*)p_android_clone;
    longjmp(jb, 1);
}

static int(*original_android_clone)(int(*fn)(void*), void* stack, int flags, void* arg, uintptr_t vararg1, uintptr_t vararg2, uintptr_t vararg3, ...);
static const uint64_t android_pthread_default_clone_flags = CLONE_VM|CLONE_FS|CLONE_FILES|CLONE_SIGHAND|CLONE_THREAD|CLONE_SYSVSEM|CLONE_SETTLS|CLONE_PARENT_SETTID|CLONE_CHILD_CLEARTID;

static __thread int pthread_state;
static __thread int pthread_new_tid;
static __thread int(*thread_start_fn)(void*);
static __thread void* thread_opaque;
static __thread uintptr_t thread_tls;

struct pthread_create_opaque
{
    uintptr_t tls_addr;
    uintptr_t tid_addr;
    int(*fn)(void*);
    void* arg;
    void* stack;
    int tid;
};

static void thread_exit_handler(int exit_code);
static void destroy_android_emutls(void* unused);

int call_on_stack(void* stack, int(*fn)(void*), void* arg);

static void* new_thread_for_android(void* op)
{
    struct pthread_create_opaque* opaque = op;
    uintptr_t tls_addr = opaque->tls_addr;
    uintptr_t tid_addr = opaque->tid_addr;
    int(*fn)(void*) = opaque->fn;
    void* arg = opaque->arg;
    void* stack = opaque->stack;
    __atomic_store_n(&opaque->tid, syscall(SYS_gettid), __ATOMIC_SEQ_CST);
    set_emutls(tls_addr);
    set_emu_tid_address(tid_addr);
    set_thread_exit_handler(thread_exit_handler);
    set_thread_atexit(destroy_android_emutls, NULL);
    return (void*)(intptr_t)call_on_stack(stack, fn, arg);
}

static int hooked_android_clone(int(*fn)(void*), void* stack, int flags, void* arg, uintptr_t vararg1, uintptr_t vararg2, uintptr_t vararg3, ...)
{
    if(flags == android_pthread_default_clone_flags)
    {
        int* parent_tid = (int*)vararg1;
        void* new_tls = (void*)vararg2;
        int* child_tid = (int*)vararg3;
        if(parent_tid != child_tid)
            __builtin_trap();
        if(pthread_state == 1)
        {
            thread_start_fn = fn;
            thread_opaque = arg;
            thread_tls = (uintptr_t)new_tls;
            return *parent_tid = pthread_new_tid;
        }
        else
        {
            pthread_t ans;
            struct pthread_create_opaque op;
            op.tls_addr = (uintptr_t)new_tls;
            op.tid_addr = (uintptr_t)child_tid;
            op.fn = fn;
            op.arg = arg;
            op.stack = stack;
            op.tid = 0;
            if(pthread_create(&ans, NULL, new_thread_for_android, &op))
                return -1;
            pthread_detach(ans);
            int tid;
            while((tid = __atomic_load_n(&op.tid, __ATOMIC_SEQ_CST)) == 0);
            return *parent_tid = tid;
        }
    }
    return original_android_clone(fn, stack, flags, arg, vararg1, vararg2, vararg3);
}

static void* tls_creation_callback(void* opaque)
{
    longjmp(jb, 1);
}

static int(*android_pthread_create)(void*, void*, void*, void*);
static int(*android_pthread_detach)(void*);
static void(*android_pthread_exit)(void*);

static uintptr_t create_android_emutls(uintptr_t parent_tls, int tid)
{
    uintptr_t bak = get_emutls();
    set_emutls(parent_tls);
    void* pthread;
    pthread_state = 1;
    pthread_new_tid = tid;
    if(android_pthread_create(&pthread, NULL, tls_creation_callback, NULL))
    {
        set_emutls(bak);
        return 0;
    }
    set_emutls(thread_tls);
    if(!setjmp(jb))
        thread_start_fn(thread_opaque);
    set_emutls(parent_tls);
    android_pthread_detach(pthread);
    set_emutls(bak);
    pthread_state = 0;
    return thread_tls;
}

static uintptr_t pthread_farm_tls;

static void thread_exit_handler(int exit_code)
{
    uintptr_t addr = get_emu_tid_address();
    if(addr)
    {
        *(int*)addr = 0;
        syscall(SYS_futex, (void*)addr, FUTEX_WAKE, 1, NULL, NULL, 0);
    }
    set_emutls(0);
    if(pthread_state == 2)
        longjmp(jb, 1);
    else
        pthread_exit((void*)(intptr_t)exit_code);
}

static void destroy_android_emutls(void* unused)
{
    if(get_emutls())
    {
        pthread_state = 2;
        if(!setjmp(jb))
            android_pthread_exit(NULL);
        pthread_state = 0;
    }
}

void ensure_emutls_exists(void)
{
    if(!get_emutls())
    {
        set_emutls(create_android_emutls(pthread_farm_tls, syscall(SYS_gettid)));
        set_thread_exit_handler(thread_exit_handler);
        set_thread_atexit(destroy_android_emutls, NULL);
    }
}

__attribute__((visibility("default"))) void* android_dlopen(const char* path, int flags)
{
    if(!ptr_android_dlopen)
        return NULL;
    return ptr_android_dlopen(path, flags);
}

__attribute__((visibility("default"))) void* android_dlsym(void* handle, const char* name)
{
    if(!ptr_android_dlsym)
        return NULL;
    return ptr_android_dlsym(handle, name);
}

__attribute__((constructor)) static void init_bionic(void)
{
    void* entry;
    void* linker_base;
    void* linker_entry;
    void* phdr;
    size_t phdr_size;
    if(load_elf(stub_data, stub_size, NULL, &entry, &linker_base, &linker_entry, &phdr, &phdr_size, patch_dyld))
    {
        perror("load_elf");
        return;
    }
    stack_t stk = {
        .ss_sp = malloc(65536),
        .ss_size = 65536,
    };
    if(!setjmp(jb))
    {
        char buf[32] = {0};
        sprintf(buf, "%" PRIuPTR, (uintptr_t)stub_callback);
        const char* argv[4] = {"android-stub", "--yes-i-am-really-libsirbyh", buf, 0};
        const char* envp[2] = {};
        const char* libsirbyh_preload = getenv("LIBSIRBYH_LD_PRELOAD");
        if(libsirbyh_preload)
            envp[0] = libsirbyh_preload - 11;
        run_elf(3, argv, envp, entry, linker_base, linker_entry, phdr, phdr_size, &stk);
    }
    void* libc_so = android_dlopen("libc.so", RTLD_NOW);
    android_pthread_create = android_dlsym(libc_so, "pthread_create");
    android_pthread_detach = android_dlsym(libc_so, "pthread_detach");
    android_pthread_exit = android_dlsym(libc_so, "pthread_exit");
    original_android_clone = hook_function(android_clone, hooked_android_clone);
    pthread_farm_tls = create_android_emutls(get_emutls(), 0);
    set_emutls(0);
}
