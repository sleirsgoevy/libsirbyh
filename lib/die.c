#include <stdio.h>
#include <stdarg.h>

void die(const char* fmt, ...)
{
    va_list l;
    va_start(l, fmt);
    vfprintf(stderr, fmt, l);
    va_end(l);
    fflush(stderr);
    __builtin_trap();
}
