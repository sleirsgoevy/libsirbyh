#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <alloca.h>
#include <sys/mman.h>
#include <sys/auxv.h>
#include <errno.h>
#include <fcntl.h>
#include <elf.h>
#include "elfldr.h"
#include "archdetect.h"

static int sort_by_virtual_address(const void* a, const void* b)
{
    const Elf_Phdr* phdr1 = a;
    const Elf_Phdr* phdr2 = b;
    if(phdr1->p_vaddr < phdr2->p_vaddr)
        return -1;
    else if(phdr1->p_vaddr > phdr2->p_vaddr)
        return 1;
    else
        return 0;
}

int load_elf(const void* elf, size_t elf_size, void** base, void** entry, void** linker_base, void** linker_entry, void** phdr, size_t* phdr_size, void(*patch_code)(uintptr_t start, uintptr_t end))
{
    const char* p_elf = elf;
    if(elf_size < sizeof(Elf_Ehdr))
        return ERROR(ERANGE);
    const Elf_Ehdr* ehdr = elf;
    if(memcmp(ehdr->e_ident, e_ident, sizeof(e_ident))
    || ehdr->e_phentsize != sizeof(Elf_Phdr)
    || ehdr->e_type != ET_DYN
    || ehdr->e_machine != CURRENT_MACHINE)
        return ERROR(ENOEXEC);
    if(ehdr->e_phoff + (size_t)ehdr->e_phnum * sizeof(Elf_Phdr) > elf_size)
        return ERROR(ERANGE);
    uintptr_t min_addr = -1;
    uintptr_t max_addr = 0;
    const Elf_Phdr* phdrs = (Elf_Phdr*)(p_elf + ehdr->e_phoff);
    for(size_t i = 0; i < ehdr->e_phnum; i++)
    {
        const Elf_Phdr* phdr = &phdrs[i];
        if(phdr->p_type == PT_LOAD)
        {
            uintptr_t start = phdr->p_vaddr;
            uintptr_t end = phdr->p_vaddr + phdr->p_memsz;
            if(start < min_addr)
                min_addr = start;
            if(end > max_addr)
                max_addr = end;
            if(phdr->p_offset + phdr->p_filesz > elf_size)
                return ERROR(ERANGE);
        }
    }
    uintptr_t page_size = sysconf(_SC_PAGE_SIZE);
    min_addr &= -page_size;
    char* mapping = mmap(NULL, max_addr - min_addr, PROT_NONE, MAP_PRIVATE | MAP_ANON, -1, 0);
    if(mapping == MAP_FAILED)
        return -1;
    char* limit = mapping;
    size_t shift = 0;
    int prot = 0;
    Elf_Phdr* phdr_copy = malloc(sizeof(*phdrs) * ehdr->e_phnum);
    if(!phdr_copy)
    {
        munmap(mapping, max_addr - min_addr);
        return -1;
    }
    memcpy(phdr_copy, phdrs, sizeof(*phdrs) * ehdr->e_phnum);
    qsort(phdr_copy, ehdr->e_phnum, sizeof(*phdrs), sort_by_virtual_address);
    void* p_phdr = NULL;
    size_t p_phdr_size = 0;
    const char* p_interp = NULL;
    for(size_t i = 0; i < ehdr->e_phnum; i++)
    {
        Elf_Phdr* phdr = &phdr_copy[i];
        if(phdr->p_type == PT_LOAD)
        {
            uintptr_t start_offset = phdr->p_vaddr & -page_size;
            uintptr_t end_offset = (phdr->p_vaddr + phdr->p_memsz + page_size - 1) & -page_size;
            uintptr_t offset = phdr->p_vaddr - phdr->p_offset;
            int new_prot = 0;
            if((phdr->p_flags & PF_R))
                new_prot |= PROT_READ;
            if((phdr->p_flags & PF_W))
                new_prot |= PROT_WRITE;
            if((phdr->p_flags & PF_X))
                new_prot |= PROT_EXEC;
            char* start = mapping + start_offset - min_addr;
            char* end = mapping + end_offset - min_addr;
            if(offset & (page_size - 1)
            || (start < limit && (prot != new_prot || shift != offset)))
            {
                free(phdr_copy);
                munmap(mapping, max_addr-min_addr);
                return ERROR(EINVAL);
            }
            mprotect(start, end-start, PROT_READ|PROT_WRITE);
            char* data_start = mapping + (phdr->p_vaddr - min_addr);
            char* data_end = mapping + (phdr->p_vaddr + phdr->p_filesz - min_addr);
            memcpy(data_start, elf+phdr->p_offset, phdr->p_filesz);
            memset(data_end, 0, phdr->p_memsz - phdr->p_filesz);
            if((phdr->p_flags & PF_X))
            {
                if(patch_code)
                    patch_code((uintptr_t)data_start, (uintptr_t)data_end);
            }
            mprotect(start, end-start, new_prot);
            __builtin___clear_cache(data_start, data_end);
            limit = end;
            shift = offset;
            prot = new_prot;
        }
        else if(phdr->p_type == PT_INTERP)
            p_interp = mapping + (phdr->p_vaddr - min_addr);
        else if(phdr->p_type == PT_PHDR)
        {
            p_phdr = mapping + (phdr->p_vaddr - min_addr);
            p_phdr_size = phdr->p_memsz;
        }
    }
    if(p_interp)
    {
        int interp_fd = open(p_interp, O_RDONLY);
        if(interp_fd < 0)
        {
            munmap(mapping, max_addr - min_addr);
            return -1;
        }
        off_t size;
        char* interpreter;
        if((size = lseek(interp_fd, 0, SEEK_END)) < 0
        || (size != (size_t)size && ERROR(ENOMEM))
        || (interpreter = mmap(NULL, size, PROT_READ, MAP_PRIVATE, interp_fd, 0)) == MAP_FAILED)
        {
            close(interp_fd);
            munmap(mapping, max_addr - min_addr);
            return -1;
        }
        close(interp_fd);
        int status = load_elf(interpreter, size, linker_base, linker_entry, NULL, NULL, NULL, NULL, patch_code);
        munmap(interpreter, size);
        if(status)
        {
            munmap(mapping, max_addr - min_addr);
            return -1;
        }
    }
    else if(linker_entry)
        *linker_entry = NULL;
    if(base)
        *base = (void*)((uintptr_t)mapping - min_addr);
    if(entry)
        *entry = mapping + (ehdr->e_entry - min_addr);
    if(phdr)
        *phdr = p_phdr;
    if(phdr_size)
        *phdr_size = p_phdr_size;
    return 0;
}

void run_elf(int argc, const char** argv, const char** envp, void* entry, void* linker_base, void* linker_entry, void* phdr, size_t phdr_size, const stack_t* stack)
{
    int envc = 0;
    while(envp[envc])
        envc++;
    size_t n_words = 1 + argc+1 + envc+1 + 2*7;
    size_t n_chars = 0;
    for(int i = 0; i < argc; i++)
        n_chars += strlen(argv[i]) + 1;
    for(int i = 0; i < envc; i++)
        n_chars += strlen(envp[i]) + 1;
    size_t frame_size = (n_words + (n_chars + sizeof(uintptr_t) - 1) / sizeof(uintptr_t)) * sizeof(uintptr_t);
    uintptr_t sp = (uintptr_t)((char*)stack->ss_sp + stack->ss_size - frame_size);
    sp &= -16;
    uintptr_t* sp_for_elf = (void*)sp;
    char* frame_strings = (void*)(sp_for_elf + n_words);
    // argv
    *sp_for_elf++ = argc;
    for(int i = 0; i < argc; i++)
    {
        *sp_for_elf++ = (uintptr_t)frame_strings;
        size_t l = strlen(argv[i]);
        memcpy(frame_strings, argv[i], l+1);
        frame_strings += l+1;
    }
    *sp_for_elf++ = 0;
    // envp
    for(int i = 0; i < envc; i++)
    {
        *sp_for_elf++ = (uintptr_t)frame_strings;
        size_t l = strlen(envp[i]);
        memcpy(frame_strings, envp[i], l+1);
        frame_strings += l+1;
    }
    *sp_for_elf++ = 0;
    // auxv
    *sp_for_elf++ = AT_PHDR;
    *sp_for_elf++ = (uintptr_t)phdr;
    *sp_for_elf++ = AT_PHENT;
    *sp_for_elf++ = sizeof(Elf_Phdr);
    *sp_for_elf++ = AT_PHNUM;
    *sp_for_elf++ = phdr_size / sizeof(Elf_Phdr);
    *sp_for_elf++ = AT_BASE;
    *sp_for_elf++ = (uintptr_t)linker_base;
    *sp_for_elf++ = AT_SECURE;
    *sp_for_elf++ = 0;
    *sp_for_elf++ = AT_ENTRY;
    *sp_for_elf++ = (uintptr_t)entry;
    *sp_for_elf++ = 0;
    *sp_for_elf++ = 0;
    if(linker_entry)
        entry = linker_entry;
    write(-179, "starting elf\n", 13);
#if CURRENT_MACHINE == EM_ARM
    mprotect(linker_base+0x9d250, 1, 7);
    *(uint32_t*)(linker_base+0x9d250) = 0x47702000;
    asm volatile("mov sp, %0\nbx %1"::"r"(sp),"r"(entry));
#endif
    __builtin_unreachable();
}
