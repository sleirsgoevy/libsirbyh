#include <sys/mman.h>
#include <unistd.h>
#include "jit.h"
#include "die.h"

static __thread char* base;
static __thread char* start;
static __thread char* end;
static __thread size_t page_size;

void lock_jit(void)
{
    mprotect(base, end - base, PROT_READ | PROT_EXEC);
    __builtin___clear_cache(base, end);
    base = start = end = NULL;
}

void* alloc_jit(size_t sz)
{
    sz = ((sz - 1) | 3) + 1;
    if(!page_size)
        page_size = sysconf(_SC_PAGE_SIZE);
    while(end - start < sz)
    {
        size_t sz1 = page_size;
        while(sz1 && sz1 < sz)
            sz1 *= 2;
        if(!sz1)
            return NULL;
        char* mapping = mmap(0, sz1, PROT_READ|PROT_WRITE|PROT_EXEC, MAP_PRIVATE|MAP_ANON, -1, 0);
        if(mapping == MAP_FAILED)
            return NULL;
        start = mapping;
        end = mapping + sz1;
    }
    char* ans = start;
    start += sz;
    for(char* i = ans; i < start; i++)
        if(*i)
            die("alloc_jit: memory not zeroed (buffer overrun???"")\n");
    return ans;
}
