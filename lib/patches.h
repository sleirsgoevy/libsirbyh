#pragma once
#include <stdint.h>

void patch_dyld(uintptr_t start, uintptr_t end);
void* hook_function(void* source, void* target);
