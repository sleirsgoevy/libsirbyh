.section .data
.global stub_data
.global stub_size
.hidden stub_data
.hidden stub_size

stub_data:
.incbin "android-stub/main"
stub_end:

.p2align 3
stub_size:
.long stub_end - stub_data
