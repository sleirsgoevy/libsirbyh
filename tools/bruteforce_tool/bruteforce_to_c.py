import json, functools, collections, sys

if '--dumb' in sys.argv:
    del sys.argv[sys.argv.index('--dumb')]
    dumb = True
else:
    dumb = False

prefix = sys.argv[1]

data = json.loads(input())
mapping = {int(k): v for k, v in data['mapping'].items()}

def indent(s):
    return ' '+s.replace('\n', '\n ')

def get_match_mask(idx, idx0):
    bad = mapping[idx][idx0]
    mask = 0
    good = 0
    bits = 0
    while not isinstance(mapping[idx], int) and bad in mapping[idx][1:]:
        mask |= 1 << mapping[idx][0]
        if bad == mapping[idx][1]:
            good |= 1 << mapping[idx][0]
            idx = mapping[idx][2]
        else:
            idx = mapping[idx][1]
        bits += 1
    return bits, mask, good, idx, bad

refcounts = collections.defaultdict(int)

@functools.lru_cache(None)
def prepopulate_refcounts(idx):
    if isinstance(mapping[idx], int): return
    for q in (1, 2):
        bits, mask, value, good, bad = get_match_mask(idx, q)
        if bits > 1:
            refcounts[good] += 1
            refcounts[bad] += 1
            prepopulate_refcounts(good)
            prepopulate_refcounts(bad)
            return
    pos, a, b = mapping[idx]
    refcounts[a] += 1
    refcounts[b] += 1
    prepopulate_refcounts(a)
    prepopulate_refcounts(b)

@functools.lru_cache(None)
def get_code(idx):
    if isinstance(mapping[idx], int):
        return 'return '+str(idx)+';'
    for q in (1, 2):
        bits, mask, value, good, bad = get_match_mask(idx, q)
        if bits > 1:
            return ('(instr & '+hex(mask)+'u) == '+hex(value)+'u', good, bad)
    pos, a, b = mapping[idx]
    return ('(instr & (1u << %d))'%pos, b, a)

@functools.lru_cache(None)
def get_func(idx):
    q = get_code(idx)
    if isinstance(q, str):
        return q
    cond, then, orelse = q
    if {then, orelse} == {0, 1}:
        if then:
            return 'return !!('+cond+');'
        else:
            return 'return !('+cond+');'
    then = get_func(then)
    orelse = get_func(orelse)
    ans = 'if('+cond+') {\n'
    ans += indent(then)
    ans += '\n} else '
    if orelse.startswith('if('):
        ans += orelse
    else:
        ans += '{\n' + indent(orelse) + '\n}'
    if not isinstance(mapping[idx], int) and refcounts[idx] > 1:
        print('static int %s_%d(uint32_t instr) {'%(prefix, idx))
        print(indent(ans))
        print('}\n')
        return 'return %s_%d(instr);'%(prefix, idx)
    else:
        return ans

if '--dumb' in sys.argv:
    for i in sorted(mapping):
        if isinstance(mapping[i], int):
            print('static int %s_%d(uint32_t instr) { return %d; }'%(prefix, i, mapping[i]))
        else:
            bit, then, orelse = mapping[i]
            print('static int %s_%d(uint32_t instr) { return (instr & (1u << %d)) ? %s_%d(instr) : %s_%d(instr); }'%(prefix, i, bit, prefix, then, prefix, orelse))
    print('static int %s(uint32_t instr) { return %s_%d(instr); }'%(prefix, prefix, data['root']))
else:
    prepopulate_refcounts(data['root'])
    print('static int '+prefix+'(uint32_t instr) {\n'+indent(get_func(data['root']))+'\n}')
