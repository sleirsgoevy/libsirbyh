import capstone, os, time, sys, json


if sys.argv[1] == 'arm32':
    cs = capstone.Cs(capstone.CS_ARCH_ARM, capstone.CS_MODE_ARM)
    def test_one(idx):
        q = idx.to_bytes(4, 'little')
        try:
            a = next(cs.disasm(q, 0))
            b = next(cs.disasm(q, 4096))
        except StopIteration: return 0
        if a.mnemonic != b.mnemonic or a.op_str != b.op_str or 'pc' in a.op_str:
            return 0
        return 1
    mapping = {0: 0, 1: 1}
    worker_start = 0
    worker_limit = 2**32
elif sys.argv[1] == 'thumb':
    cs = capstone.Cs(capstone.CS_ARCH_ARM, capstone.CS_MODE_THUMB)
    twobyte_instrs = {}
    def test_one(idx):
        i, j = divmod(idx, 65536)
        if i in twobyte_instrs:
            return twobyte_instrs[i]
        q = i.to_bytes(2, 'little') + j.to_bytes(2, 'little')
        try:
            a = next(cs.disasm(q, 0))
            b = next(cs.disasm(q, 4096))
        except StopIteration: return 0
        assert len(a.bytes) == len(b.bytes)
        assert a.mnemonic == b.mnemonic
        if a.op_str != b.op_str or 'pc' in a.op_str:
            ans = 0
        elif len(a.bytes) == 4:
            ans = 2
        elif a.mnemonic == 'it':
            ans = 3
        else:
            ans = 1
        if len(a.bytes) == 2:
            twobyte_instrs[i] = ans
        return ans
    mapping = {0: 0, 1: 1, 2: 2, 3: 3}
    worker_start = 0
    worker_limit = 0
else:
    assert False, sys.argv[1]

n_workers = 8

def spawn_worker(rng):
    r, w = os.pipe()
    if os.fork():
        os.close(w)
        return open(r, 'rb', closefd=True)
    else:
        os.close(r)
        sys.excepthook = lambda *args: None
        with open(w, 'wb', closefd=True) as f:
            for i in rng:
                f.write(bytes((test_one(i),)))
        exit(0)

def spawn_workers():
    global workers
    workers = [spawn_worker(range(worker_start+i, worker_limit, n_workers)) for i in range(n_workers)]

spawn_workers()

current_test = 0

rev_mapping = {}

start_time = time.time()
def log():
    if current_test % 10000 == 0:
        eta = int((2**32 - current_test) / current_test * (time.time() - start_time))
        print('count =', current_test, 'len(mapping) =', len(mapping), 'ETA', eta, 'seconds', file=sys.stderr)

def get(idx, order):
    global current_test
    if order == 32:
        assert idx == current_test
        current_test += 1
        log()
        return workers[idx % n_workers].read(1)[0]
    else:
        if order == 16 and sys.argv[1] == 'thumb':
            assert current_test == (idx << 16)
            ans = test_one(idx << 16)
            if idx in twobyte_instrs:
                current_test += 65536
                return ans
            else:
                global worker_start, worker_limit
                worker_start = current_test
                worker_limit = current_test + 65536
                spawn_workers()
        a = get(2*idx, order+1)
        b = get(2*idx+1, order+1)
        if a == b:
            return a
        else:
            key = (31-order, a, b)
            q = rev_mapping.get(key, None)
            if q == None:
                q = len(mapping)
                mapping[q] = key
                rev_mapping[key] = q
            return q

main_idx = get(0, 0)

print(json.dumps({'mapping': mapping, 'root': main_idx}))
