import capstone

cs = capstone.Cs(capstone.CS_ARCH_ARM, capstone.CS_MODE_THUMB)

def get_disp_capstone(q):
    q = q.to_bytes(4, 'little')
    q = next(cs.disasm(q[2:]+q[:2], 0xfffffffc))
    assert q.mnemonic == 'bl' and q.op_str.startswith('#')
    return int(q.op_str[1:], 0)

def get_disp_manual(q):
    q ^= 0xf000b800
    if (q & 0x4000000):
        q ^= 0x2800
    q = (q & 0x7ff) | ((q & 0x800) << 10) | ((q & 0x2000) << 9) | ((q & 0x3ff0000) >> 5) | ((q & 0x4000000) >> 3)
    if q >= 0x800000:
        q -= 0x1000000
    return (q * 2) % 2**32

for q in range(2**24):
    q = (q & 0xfff) | ((q & 0x1000) << 1) | ((q & 0xffe000) << 3) | 0xf0009000
    disp1 = get_disp_capstone(q)
    disp2 = get_disp_manual(q)
    assert disp1 == disp2, (hex(q), hex(disp1), hex(disp2))
