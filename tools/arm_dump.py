import subprocess, tempfile, sys

thumb = False

if '--thumb' in sys.argv:
    del sys.argv[sys.argv.index('--thumb')]
    thumb = True

instr = ' '.join(sys.argv[1:])

with tempfile.TemporaryDirectory() as path:
    with open(path+'/source.s', 'w') as file:
        file.write('.syntax unified\n')
        if thumb: file.write('.thumb\n')
        file.write('start:\n')
        file.write(instr+'\n')
    assert not subprocess.call(('gcc', '-march=native', path+'/source.s', '-c', '-o', path+'/compiled.o'))
    assert not subprocess.call(('objdump', '-d', path+'/compiled.o'))
